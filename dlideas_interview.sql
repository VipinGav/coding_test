-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2021 at 11:19 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dlideas_interview`
--

-- --------------------------------------------------------

--
-- Table structure for table `di_users_list`
--

CREATE TABLE `di_users_list` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `di_users_list`
--

INSERT INTO `di_users_list` (`user_id`, `user_name`, `user_status`) VALUES
(1, 'Leanne Graham', 0),
(2, 'Ervin Howell', 0),
(3, 'Clementine Bauch', 0),
(4, 'Patricia Lebsack', 0),
(5, 'Chelsey Dietrich', 0),
(6, 'Mrs. Dennis Schulist', 0),
(7, 'Kurtis Weissnat', 0),
(8, 'Nicholas Runolfsdottir V', 0),
(9, 'Glenna Reichert', 0),
(10, 'Clementina DuBuque', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dl_assigned_user_task`
--

CREATE TABLE `dl_assigned_user_task` (
  `assigned_task_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `assigned_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dl_assigned_user_task`
--

INSERT INTO `dl_assigned_user_task` (`assigned_task_id`, `category_id`, `user_id`, `task_id`, `assigned_status`) VALUES
(46, 1, 8, 7, 0),
(47, 1, 8, 8, 0),
(48, 1, 8, 9, 0),
(49, 1, 1, 7, 0),
(50, 1, 1, 8, 0),
(51, 1, 1, 9, 0),
(53, 4, 5, 28, 0),
(54, 4, 5, 32, 0),
(55, 4, 5, 29, 0),
(56, 4, 5, 31, 0),
(67, 2, 10, 23, 0),
(68, 2, 10, 25, 0),
(69, 2, 10, 26, 0),
(70, 2, 10, 10, 0),
(71, 2, 10, 11, 0),
(73, 2, 5, 22, 0),
(74, 2, 5, 24, 0),
(75, 2, 5, 21, 0),
(76, 2, 5, 27, 0),
(77, 2, 5, 17, 0),
(78, 2, 5, 23, 0),
(79, 2, 5, 25, 0),
(80, 2, 5, 26, 0),
(81, 2, 5, 10, 0),
(82, 2, 5, 11, 0),
(83, 2, 5, 12, 0),
(94, 2, 2, 22, 0),
(95, 2, 2, 24, 0),
(96, 2, 2, 21, 0),
(97, 2, 2, 27, 0),
(98, 2, 2, 17, 0),
(99, 2, 2, 23, 0),
(100, 2, 2, 26, 0),
(101, 2, 2, 10, 0),
(102, 2, 2, 11, 0),
(103, 2, 2, 12, 0),
(106, 1, 5, 7, 0),
(107, 1, 5, 8, 0),
(108, 1, 5, 9, 0),
(109, 2, 1, 22, 0),
(110, 2, 1, 24, 0),
(111, 2, 1, 10, 0),
(112, 2, 1, 11, 0),
(113, 2, 1, 12, 0),
(114, 2, 3, 22, 0),
(115, 2, 3, 24, 0),
(116, 2, 3, 21, 0),
(117, 2, 3, 27, 0),
(118, 2, 3, 17, 0),
(119, 2, 3, 23, 0),
(120, 2, 3, 25, 0),
(121, 2, 3, 26, 0),
(122, 2, 3, 10, 0),
(123, 2, 3, 11, 0),
(124, 2, 3, 12, 0),
(125, 1, 2, 7, 0),
(126, 1, 2, 8, 0),
(127, 1, 2, 9, 0),
(128, 4, 2, 37, 0),
(129, 4, 2, 28, 0),
(130, 4, 2, 32, 0),
(131, 4, 2, 29, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dl_category_master`
--

CREATE TABLE `dl_category_master` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(300) NOT NULL,
  `category_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dl_category_master`
--

INSERT INTO `dl_category_master` (`category_id`, `category_name`, `category_status`) VALUES
(1, 'Hobby', 0),
(2, 'Important', 0),
(4, 'Remind Me', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dl_task_master`
--

CREATE TABLE `dl_task_master` (
  `task_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `task_name` varchar(300) NOT NULL,
  `task_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dl_task_master`
--

INSERT INTO `dl_task_master` (`task_id`, `category_id`, `task_name`, `task_status`) VALUES
(7, 1, 'Task 1', 0),
(8, 1, 'Task 2', 0),
(9, 1, 'Task 3', 0),
(10, 2, 'Task 1', 0),
(11, 2, 'Task 3', 0),
(12, 2, 'Task 4', 0),
(17, 2, 'LAMP', 0),
(21, 2, 'CSS ', 0),
(22, 2, 'Bootstrap ', 0),
(23, 2, 'Log in, Log out and Register users', 0),
(24, 2, 'Create a todo entry under different labels', 0),
(25, 2, 'Save a due date and calculate the remaining time', 0),
(26, 2, 'Show the progress of the work', 0),
(27, 2, 'Edit and delete any entry', 0),
(28, 4, 'CSS', 0),
(29, 4, 'JQuery', 0),
(31, 4, 'Json', 0),
(32, 4, 'Javascript', 0),
(37, 4, 'Ajax', 0),
(38, 4, 'Codeigniter', 0),
(39, 4, 'Laravel', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `di_users_list`
--
ALTER TABLE `di_users_list`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `dl_assigned_user_task`
--
ALTER TABLE `dl_assigned_user_task`
  ADD PRIMARY KEY (`assigned_task_id`);

--
-- Indexes for table `dl_category_master`
--
ALTER TABLE `dl_category_master`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `dl_task_master`
--
ALTER TABLE `dl_task_master`
  ADD PRIMARY KEY (`task_id`),
  ADD KEY `category` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `di_users_list`
--
ALTER TABLE `di_users_list`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `dl_assigned_user_task`
--
ALTER TABLE `dl_assigned_user_task`
  MODIFY `assigned_task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `dl_category_master`
--
ALTER TABLE `dl_category_master`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dl_task_master`
--
ALTER TABLE `dl_task_master`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dl_task_master`
--
ALTER TABLE `dl_task_master`
  ADD CONSTRAINT `category` FOREIGN KEY (`category_id`) REFERENCES `dl_category_master` (`category_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
