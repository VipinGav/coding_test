var appRoot = "dlIdeas";
$(document).ready(function(){
   itemList();
});


//----Load user list for initial
function itemList(){
    var item = "initial";
    $("#itemsListTable").html("Loading..<span class='fa fa-spinner fa-spin animated'></span>");
    $("#itemsListCount").html("<span class='fa fa-spinner fa-spin animated'></span>");    

    $.ajax({
        type:'get',
        url : "Users_Master/itemList",
        data: {item:item},
        
        success: function(returnedData){
            $("#itemsListTable").html(returnedData.itemsListTable);
            $("#itemsListCount").html(returnedData.count);
            
        },
        
        error: function(){
            
        }
    });
    
    return false;
}


