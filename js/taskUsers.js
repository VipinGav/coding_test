var appRoot = "dlIdeas";
$(document).ready(function(){
   //usersList();
   $("#reset").hide(); 
   $("#refresh").show();
});



//-------Filtering Users 

$("#userFilter").on('click', '#filter', function(e){
        e.preventDefault();
     
     var userId = $("#itemsListUserId").val();
     var title  = $("#itemsListTitle").val();
     var item   = "filtering";


   if(!userId && !title){  

              swal({
                    title : "<span class='col-orange'>Warning</span>",
                    text  : "<span class='col-red'>Please choose any keywords for filtering </span>",
                    type  : "warning",
                    html  :'You can use <b>bold text</b>, and other HTML tags',
                    timer : 2500,
                  });
           return
        }

			if(userId && !title){
				var item =1;
			}
			else if(!userId && title){
				var item =2;
			}
			else if(userId && title){
				var item =3;
			}


$("#itemsListTable").css('opacity',0.2);
$("#filter").html("Filtering..<span class='fa fa-spinner fa-spin animated'></span>");
$("#itemsListCount").html("<span class='fa fa-spinner fa-spin animated'></span>");     

$.ajax({
        type:'get',
        url : "Api/usersFiltering",
        data: $('#userFilter').serialize()+"&item="+item,
        
        success: function(returnedData){
        	$("#filter").html("Filter"); 
        	$("#reset").show();  
        	$("#refresh").hide();
            $("#itemsListTable").html(returnedData.itemsListTable).css('opacity', 4);
            $("#itemsListUserId").html(returnedData.user_id);
            $("#itemsListId").html(returnedData.users);
            $("#itemsListCount").html(returnedData.count);
        },
        
        error: function(){
            
        }
    });


      
});

//---Reset Form and list

$("#userFilter").on('click', '#reset', function(e){
        e.preventDefault();
         usersList();
         $("#reset").hide();
         $("#refresh").show();
         $('#userFilter').find('input').val('');

});

//---Refresh Form and list

$("#userFilter").on('click', '#refresh', function(e){
        e.preventDefault();
         $("#refresh").html("<i class='fa fa-refresh fa-spin animated'></i>");
         $("#itemsListTable").css("opacity", "0.4"); 
         usersList(); 
              setTimeout(function(){
                     $("#refresh").html("<i class='fa fa-refresh'></i>");
                     $("#itemsListTable").css("opacity", "2"); 
                     
                }, 1000);

});


//----Load user list for initial
/*function usersList(){
    var item = "initial";
    $("#itemsListTable").html("Loading..<span class='fa fa-spinner fa-spin animated'></span>");
    $("#itemsListCount").html("<span class='fa fa-spinner fa-spin animated'></span>");    

    $.ajax({
        type:'get',
        url : "Api/userslist",
        data: {item:item},
        
        success: function(returnedData){
            $("#itemsListTable").html(returnedData.itemsListTable);
            $("#itemsListUserId").html(returnedData.user_id);
            $("#itemsListId").html(returnedData.users);
            $("#itemsListCount").html(returnedData.count);
            $("#reset").hide(); 
        },
        
        error: function(){
            
        }
    });
    
    return false;
}*/


