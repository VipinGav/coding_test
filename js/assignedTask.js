var appRoot = setAppRoot("dlIdeas", "dlIdeas");
$(document).ready(function(){
   itemTab();
});


                $("#assignTask").on('click', '#add-model', function(e){
                        e.preventDefault(); 
                        var type = $(this).attr("data-type");   
                        var id   = $(this).attr("data-id");          
                        var user = $(this).attr("data-user");

                         $('#addEditItemModal').modal({
                             backdrop: 'static', 
                             keyboard: false,
                         });
                         $("#itemsForm").html("Loading..<span class='fa fa-spinner fa-spin animated'></span>");
                                if(type=='add'){
                                   var link ="addAssignTaskForm";
                                   $(".saveButton").attr('id','saveButton');
                                   $("#saveButton").html('<i class="fa fa-floppy-o"></i> Save');
                                   $("#modal-title").html('Assign New Task');
                                }else if(type=='edit'){
                                   var link ="editAssignTaskForm";
                                   $(".saveButton").attr('id','updateButton');
                                   $("#updateButton").html('<i class="fa fa-pencil-square-o"></i> Save Changes');
                                   $("#modal-title").html('Edit Assigned Task');
                                   
                                }else{

                                }

                        $.ajax({
                                type:'get',
                                url : "Assigned_Task_Master/"+link+"",
                                data: {id:id, user:user},
                                
                                success: function(returnedData){
                                    $("#itemsForm").html(returnedData.itemsListTable);
                                },
                                
                                error: function(){
                                    
                                }
                            });
                });
//---------Get Task list depending on Category
              $("#assignTask").on('change', '#category_id', function(e){
                                    e.preventDefault(); 
                                    var catgory =$(this).val();
                                    $("#categoryErr_1").text("");     
                          $("#getTask").html("Loading..<span class='fa fa-spinner fa-spin animated'></span>");
                          $.ajax({
                                type:'get',
                                url : "Assigned_Task_Master/getTask",
                                data: {catgory:catgory},
                                
                                success: function(returnedData){
                                    $("#getTask").html(returnedData.itemsListTable);
                                },
                                
                                error: function(){
                                    
                                }
                            });

              });
//---------Check all checkbox
             $("#itemsForm").on('click', '#selectAll', function(){  
               $(':checkbox.selectEach').prop('checked', this.checked);    
             });

                  
//-------------Save Assigned Task
          $("#assignTask").on('click', '#saveButton', function(e){
                      e.preventDefault();
                       var category_id = $("#category_id").val();
                       var user_id     = $("#user_id").val();
                       var selected    = $('input:checkbox:checked').length; 

                       $("#categoryErr_1,#userErr_1").text("");
                       $("#selectAny").html("");

             if(!user_id || !category_id){   
                   !user_id ? $("#userErr_1").text("Please Select User") : "";
                   !category_id ? $("#categoryErr_1").text("Please Select Category") : "";
                    category_id.focus();
                    user_id.focus();
               return
            }
            if(parseInt(selected) <=0 ){
               $("#selectAny").html("<span class='col-red'>Please Choose any Task for User</span>");
              return
            }
            $(".errMsg").text("");
                   
                            swal({
                                  title: "<span class='col-green'>Processing...</span>",
                                  text: "<i class='fa fa-spinner fa-spin animated'></i>",
                                  showConfirmButton: false,
                                  html:'You can use <b>bold text</b>, and other HTML tags',
                                });
                      
                      $.ajax({
                          type : "post",
                          url  : "Assigned_Task_Master/addAssignedTask",
                          data : $('#assignedTaskForm').serialize(),
                          
                          success: function(returnedData){
                              
                                  if(returnedData.status === 1){
                                      document.getElementById("assignedTaskForm").reset();
                                      $('#addEditItemModal').modal('hide');
                                      swal({
                                          title: "<span class='col-green'>Success</span>",
                                          text: "<spanclass='col-green'>"+returnedData.msg+"</span>",
                                          type: "success",
                                          html:'You can use <b>bold text</b>, and other HTML tags',
                                          timer: 2000
                                        });
                                      itemTab(); 
                                  }
                                  
                                  else{
                                      
                                      swal({
                                          title: "<span class='col-orange'>Warning</span>",
                                          text: "<span class='col-red'>"+returnedData.msg+"</span>",
                                          type: "warning",
                                          html:'You can use <b>bold text</b>, and other HTML tags',
                                        });
                                  }
                          },

                          error: function(){

                                      swal({
                                          title: "<span class='col-orange'>Warning</span>",
                                          text: "<spanclass='col-red'>Unable to process your request at this time. Pls try again later!</span>",
                                          type: "warning",
                                          html:'You can use <b>bold text</b>, and other HTML tags',
                                        });
                          }
                      });
                  });

//---Get Each users task list
           $("#itemsListTable").on('click', '.getDetails', function(e){
                             e.preventDefault();
                 var userId = $(this).attr("data-index");
                 var status = $(this).attr("aria-expanded");

                 if(status=='false'){
                  $.ajax({
                                type:'get',
                                url : "Assigned_Task_Master/getEach_user_task",
                                data: {userId:userId},
                                
                                success: function(returnedData){
                                    $("#itemDetailsTable"+userId).html(returnedData.itemsListTable);
                                },
                                
                                error: function(){
                                    
                                }
                            });
                 }
           });


//----Update Task
                $("#assignTask").on('click', '#updateButton', function(e){
                             e.preventDefault();
                       var category_id = $("#category_id").val();
                       var user_id     = $("#user_id").val();
                       var selected    = $('input:checkbox:checked').length; 

                       $("#categoryErr_1,#userErr_1").text("");
                       $("#selectAny").html("");

                       if(!user_id || !category_id){   
                             !user_id ? $("#userErr_1").text("Please Select User") : "";
                             !category_id ? $("#categoryErr_1").text("Please Select Category") : "";
                              category_id.focus();
                              user_id.focus();
                         return
                      }
                      if(parseInt(selected) <=0 ){
                         $("#selectAny").html("<span class='col-red'>Please Choose any Task for User</span>");
                        return
                      }
                      $(".errMsg").text("");


                          swal({
                                    title: "Are you sure?",
                                    text: "<span class='col-black'>Do you want to Save this chages?</span>",
                                    type: "warning",
                                    showCancelButton: true,
                                    cancelButtonText: "No",
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Yes",
                                    closeOnConfirm: false,
                                    html: true,
                                },
                            function(){ 
                         
                                  swal({
                                        title: "<span class='col-green'>Processing...</span>",
                                        text: "<i class='fa fa-spinner fa-spin animated'></i>",
                                        showConfirmButton: false,
                                        html:'You can use <b>bold text</b>, and other HTML tags',
                                      });
                            
                            $.ajax({
                                type : "post",
                                url  : "Assigned_Task_Master/editAssignedTask",
                                data : $('#assignedTaskForm').serialize(),
                                
                                success: function(returnedData){
                                    
                                        if(returnedData.status === 1){
                                            document.getElementById("assignedTaskForm").reset();
                                            $('#addEditItemModal').modal('hide');
                                            swal({
                                                title: "<span class='col-green'>Success</span>",
                                                text: "<spanclass='col-green'>"+returnedData.msg+"</span>",
                                                type: "success",
                                                html:'You can use <b>bold text</b>, and other HTML tags',
                                                timer: 2000
                                              });
                                            itemEachDetails(user_id); 
                                        }
                                        
                                        else{
                                            
                                            swal({
                                                title: "<span class='col-orange'>Warning</span>",
                                                text: "<span class='col-red'>"+returnedData.msg+"</span>",
                                                type: "warning",
                                                html:'You can use <b>bold text</b>, and other HTML tags',
                                              });
                                        }
                                },

                                error: function(){

                                            swal({
                                                title: "<span class='col-orange'>Warning</span>",
                                                text: "<spanclass='col-red'>Unable to process your request at this time. Pls try again later!</span>",
                                                type: "warning",
                                                html:'You can use <b>bold text</b>, and other HTML tags',
                                              });
                                }
                             });
                            });
                        });
//---Refresh Form and list
                    $("#assignTask").on('click', '#refresh', function(e){
                            e.preventDefault();
                             $("#refresh").html("<i class='fa fa-refresh fa-spin animated'></i>");
                             $("#itemsListTable").css("opacity", "0.4"); 
                             itemTab(); 
                                  setTimeout(function(){
                                         $("#refresh").html("<i class='fa fa-refresh'></i>");
                                         $("#itemsListTable").css("opacity", "2"); 
                                    }, 500);

                    });
//---Delete Task
                   $("#itemsListTable").on('click', '.delete-confirm', function(e){
                      e.preventDefault();
                      var itemId  = $(this).attr("id");
                      var itemRow = $(this).closest('tr');
                      var user_id = $(this).attr('data-user');

                      if(itemId){

                             swal({
                                    title: "Are you sure?",
                                    text: "<span class='col-red'>Do you want to Delete this Task?</span>",
                                    type: "warning",
                                    showCancelButton: true,
                                    cancelButtonText: "No",
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Yes",
                                    closeOnConfirm: false,
                                    html: true,
                                },
                            function(){    
                                 
                                   swal({
                                        title : "<span class='col-green'>Processing...</span>",
                                        text  : "<i class='fa fa-spinner fa-spin animated'></i>",
                                        showConfirmButton: false,
                                        html  : true,
                                      });
                                    
                                $.ajax({
                                    url    : "Assigned_Task_Master/deleteAssignedTask",
                                    method : "get",
                                    data   : {id:itemId}
                                }).done(function(rd){
                                    if(rd.status === 1){
                                        $(itemRow).remove();
                                        swal({
                                            title : "<span class='col-green'>Success</span>",
                                            text  : "<span class='col-green'>"+rd.msg+" </span>",
                                            type  : "success",
                                            html  : true,
                                            timer : 2000
                                            
                                          });
                                        resetItemSN();
                                        itemEachDetails(user_id);
                                    }else if(rd.status === 0){
                                        swal({
                                            title : "<span class='col-orange'>Warning</span>",
                                            text  : "<span class='col-red'>"+rd.msg+" </span>",
                                            type  : "warning",
                                            html  : true,
                                            
                                          });
                                    }
                                    
                                }).fail(function(){
                                    console.log('Req Failed');
                                });

                                });
                            
                        }

                    });
//---------Get each details
            function itemEachDetails(userId){
              
                  $.ajax({
                                type:'get',
                                url : "Assigned_Task_Master/getEach_user_task",
                                data: {userId:userId},
                                
                                success: function(returnedData){
                                    $("#itemDetailsTable"+userId).html(returnedData.itemsListTable);
                                },
                                
                                error: function(){
                                    
                                }
                            });
                    };  


//----Load list for initial
                  function itemTab(url){
                      var item        = "initial";
                      var orderBy     = "um.user_name";
                      var orderFormat = "ASC";
                      var limit       = 10;

                      $("#itemsListTable").css("opacity", 0.2);
                      $("#itemsListCount").html("<span class='fa fa-spinner fa-spin animated'></span>");    
                       
                      $.ajax({
                          type:'get',
                          url : url ? url : appRoot+"Assigned_Task_Master/itemTab",
                          data: {item:item,orderBy:orderBy, orderFormat:orderFormat, limit:limit},
                          
                          success: function(returnedData){
                              $("#itemsListTable").css("opacity", 4);
                              $("#itemsListTable").html(returnedData.itemsListTable);
                              $("#itemsListCount").html(returnedData.count);
                              
                          },
                          
                          error: function(){
                              
                          }
                      });
                      
                      return false;
                  }

function resetItemSN(){
    $(".itemSN").each(function(i){
        $(this).html(parseInt(i)+1);
    });
}


function loadPage(urlToLoad){
    $.ajax({
        type: "GET",
        url: appRoot+urlToLoad,
        success: function(returnedData){
            document.getElementById('pageContent').innerHTML = returnedData.pageContent;
            document.getElementById('pageTitle').innerHTML = returnedData.pageTitle;
            //window.history.pushState("", "", "");
        }
    });
}

function setAppRoot(devFolderName, prodFolderName){
    var hostname = window.location.hostname;

    /*
     * set the appRoot
     * This will work for both http, https with or without www
     * @type String
     */
    
    //attach trailing slash to both foldernames
    var devFolder = devFolderName ? devFolderName+"/" : "";
    var prodFolder = prodFolderName ? prodFolderName+"/" : "";
    
    var baseURL = "";
    
    if(hostname.search("localhost") !== -1 || (hostname.search("192.168.") !== -1)  || (hostname.search("127.0.0.") !== -1)){
        baseURL = window.location.origin+"/"+devFolder;
    }
    
    else{
        baseURL = window.location.origin+"/"+prodFolder;
    }
    
    return baseURL;
}
