var appRoot = "dlIdeas";
$(document).ready(function(){
   itemList();
   $("#reset").hide(); 
   $("#refresh").show();
});


                $("#category").on('click', '#add-model', function(e){
                        e.preventDefault(); 
                        var type = $(this).attr("data-type");   
                        var id   = $(this).attr("data-id");          
                        

                         $('#addEditItemModal').modal({
                             backdrop: 'static', 
                             keyboard: false,
                         });
                         $("#itemsForm").html("Loading..<span class='fa fa-spinner fa-spin animated'></span>");
                                if(type=='add'){
                                   var link ="addCategoryForm";
                                   $(".saveButton").attr('id','saveButton');
                                   $("#saveButton").html('<i class="fa fa-floppy-o"></i> Save');
                                   $("#modal-title").html('Add New Category');
                                }else if(type=='edit'){
                                   var link ="editCategoryForm";
                                   $(".saveButton").attr('id','updateButton');
                                   $("#updateButton").html('<i class="fa fa-pencil-square-o"></i> Save Changes');
                                   $("#modal-title").html('Edit Category');
                                   
                                }else{

                                }

                        $.ajax({
                                type:'get',
                                url : "Category_Master/"+link+"",
                                data: {id:id},
                                
                                success: function(returnedData){
                                    $("#itemsForm").html(returnedData.itemsListTable);
                                },
                                
                                error: function(){
                                    
                                }
                            });
                });

 //=======Add more Fields

                var maxField = 10; //Input fields increment limitation
                var x = 1; //Initial field counter is 1
                //Once add button is clicked
                $("#itemsForm").on('click', '.add_button', function(e){
                    e.preventDefault();
                    //Check maximum number of input fields
                    if(x < maxField){ 
                       x++; //Increment field counter
                     $("#field_wrapper").append('<div><div class="form-group row"><label class="col-lg-3 col-form-label" for="category_name">Category Name<span class="text-danger">*</span></label><div class="col-lg-7"><input type="text" class="form-control" id="category_name" name="category_name[]"  placeholder="Enter Category Name.." required="" ><span class="col-red font-12 help-block errMsg" id="categoryNameErr_'+x+'"></span></div><a href="javascript:void(0);" class="remove_button"><i class="fa fa-minus-circle font-28 col-grey remove_button"></i></a></div></div>'); //Add field html
                     
                    }
                });
                
                //Once remove button is clicked
                $("#itemsForm").on('click', '.remove_button', function(e){
                    e.preventDefault();
                    $(this).parent('div').remove(); //Remove field html
                    x--; //Decrement field counter
                });
                  
//-------------Save Category
            $("#category").on('click', '#saveButton', function(e){
                      e.preventDefault();
                       var categoryName = document.getElementsByName("category_name[]");

            for(var i=0; i < categoryName.length; i++) {
               var x= i+1;
               $("#categoryNameErr_"+x).text("");
                  if(categoryName[i].value=="") {
                        $("#categoryNameErr_"+x).text("Please Enter category Name");
                     return
                  }
              }  

             $(".errMsg").text("");
                   
                            swal({
                                  title: "<span class='col-green'>Processing...</span>",
                                  text: "<i class='fa fa-spinner fa-spin animated'></i>",
                                  showConfirmButton: false,
                                  html:'You can use <b>bold text</b>, and other HTML tags',
                                });
                      
                      $.ajax({
                          type : "post",
                          url  : "Category_Master/addCategory",
                          data : $('#categoryForm').serialize(),
                          
                          success: function(returnedData){
                              
                                  if(returnedData.status === 1){
                                      document.getElementById("categoryForm").reset();
                                      $('#addEditItemModal').modal('hide');
                                      swal({
                                          title: "<span class='col-green'>Success</span>",
                                          text: "<spanclass='col-green'>"+returnedData.msg+"</span>",
                                          type: "success",
                                          html:'You can use <b>bold text</b>, and other HTML tags',
                                          timer: 2000
                                        });
                                      itemList(); 
                                  }
                                  
                                  else{
                                      
                                      swal({
                                          title: "<span class='col-orange'>Warning</span>",
                                          text: "<span class='col-red'>"+returnedData.msg+"</span>",
                                          type: "warning",
                                          html:'You can use <b>bold text</b>, and other HTML tags',
                                        });
                                  }
                          },

                          error: function(){

                                      swal({
                                          title: "<span class='col-orange'>Warning</span>",
                                          text: "<spanclass='col-red'>Unable to process your request at this time. Pls try again later!</span>",
                                          type: "warning",
                                          html:'You can use <b>bold text</b>, and other HTML tags',
                                        });
                          }
                      });
                  });

//----Update Category
                $("#category").on('click', '#updateButton', function(e){
                             e.preventDefault();
                             var categoryName = document.getElementsByName("category_name[]");

                  for(var i=0; i < categoryName.length; i++) {
                     var x= i+1;
                     $("#categoryNameErr_"+x).text("");
                        if(categoryName[i].value=="") {
                              $("#categoryNameErr_"+x).text("Please Enter category Name");
                           return
                        }
                    }  

                   $(".errMsg").text("");
                         
                                  swal({
                                        title: "<span class='col-green'>Processing...</span>",
                                        text: "<i class='fa fa-spinner fa-spin animated'></i>",
                                        showConfirmButton: false,
                                        html:'You can use <b>bold text</b>, and other HTML tags',
                                      });
                            
                            $.ajax({
                                type : "post",
                                url  : "Category_Master/editCategory",
                                data : $('#categoryForm').serialize(),
                                
                                success: function(returnedData){
                                    
                                        if(returnedData.status === 1){
                                            document.getElementById("categoryForm").reset();
                                            $('#addEditItemModal').modal('hide');
                                            swal({
                                                title: "<span class='col-green'>Success</span>",
                                                text: "<spanclass='col-green'>"+returnedData.msg+"</span>",
                                                type: "success",
                                                html:'You can use <b>bold text</b>, and other HTML tags',
                                                timer: 2000
                                              });
                                            itemList(); 
                                        }
                                        
                                        else{
                                            
                                            swal({
                                                title: "<span class='col-orange'>Warning</span>",
                                                text: "<span class='col-red'>"+returnedData.msg+"</span>",
                                                type: "warning",
                                                html:'You can use <b>bold text</b>, and other HTML tags',
                                              });
                                        }
                                },

                                error: function(){

                                            swal({
                                                title: "<span class='col-orange'>Warning</span>",
                                                text: "<spanclass='col-red'>Unable to process your request at this time. Pls try again later!</span>",
                                                type: "warning",
                                                html:'You can use <b>bold text</b>, and other HTML tags',
                                              });
                                }
                            });
                        });
//---Refresh Form and list
                    $("#category").on('click', '#refresh', function(e){
                            e.preventDefault();
                             $("#refresh").html("<i class='fa fa-refresh fa-spin animated'></i>");
                             $("#itemsListTable").css("opacity", "0.4"); 
                             itemList(); 
                                  setTimeout(function(){
                                         $("#refresh").html("<i class='fa fa-refresh'></i>");
                                         $("#itemsListTable").css("opacity", "2"); 
                                    }, 500);

                    });
//---Delete category
                   $("#category").on('click', '.delete-confirm', function(e){
                      e.preventDefault();
                      var itemId  = $(this).attr("id");
                      var itemRow = $(this).closest('tr');

                      if(itemId){

                             swal({
                                    title: "Are you sure?",
                                    text: "Do you want to Delete this category?",
                                    type: "warning",
                                    showCancelButton: true,
                                    cancelButtonText: "No",
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Yes",
                                    closeOnConfirm: false
                                },
                            function(){    
                                 
                                   swal({
                                        title: "<span class='col-green'>Processing...</span>",
                                        text: "<i class='fa fa-spinner fa-spin animated'></i>",
                                        showConfirmButton: false,
                                        html:'You can use <b>bold text</b>, and other HTML tags',
                                      });
                                    
                                $.ajax({
                                    url    : "Category_Master/deleteCategory",
                                    method : "get",
                                    data   : {id:itemId}
                                }).done(function(rd){
                                    if(rd.status === 1){
                                        $(itemRow).remove();
                                        swal({
                                            title: "<span class='col-green'>Success</span>",
                                            text: "<span class='col-green'>"+rd.msg+" </span>",
                                            type: "success",
                                            html:'You can use <b>bold text</b>, and other HTML tags',
                                            
                                          });
                                        resetItemSN();
                                    }else if(rd.status === 0){
                                        swal({
                                            title: "<span class='col-orange'>Warning</span>",
                                            text: "<span class='col-red'>"+rd.msg+" </span>",
                                            type: "warning",
                                            html:'You can use <b>bold text</b>, and other HTML tags',
                                            
                                          });
                                    }
                                    
                                }).fail(function(){
                                    console.log('Req Failed');
                                });

                                });
                            
                        }

                    });

//----Load user list for initial
                  function itemList(){
                      var item = "initial";
                      $("#itemsListTable").html("Loading..<span class='fa fa-spinner fa-spin animated'></span>");
                      $("#itemsListCount").html("<span class='fa fa-spinner fa-spin animated'></span>");    

                      $.ajax({
                          type:'get',
                          url : "Category_Master/itemList",
                          data: {item:item},
                          
                          success: function(returnedData){
                              $("#itemsListTable").html(returnedData.itemsListTable);
                              $("#itemsListCount").html(returnedData.count);
                              
                          },
                          
                          error: function(){
                              
                          }
                      });
                      
                      return false;
                  }

function resetItemSN(){
    $(".itemSN").each(function(i){
        $(this).html(parseInt(i)+1);
    });
}
