<?php
defined('BASEPATH') OR exit('Access Denied');
require_once './application/controllers/functions.php';

/**
 * Description of Genlib
 * Class deals with functions needed in multiple controllers to avoid repetition in each of the controllers
 *
 * @author Amir <amirsanni@gmail.com>
 */
class Genlib {
    protected $CI;
    
    public function __construct() {
        $this->CI = &get_instance();
    }




    /**
     * Ensure request is an AJAX request
     * @return string
     */
    public function ajaxOnly(){
        //display uri error if request is not from AJAX
        if(!$this->CI->input->is_ajax_request()){
            redirect(base_url());
        }
        
        else{
            return "";
        }
    } 
    
    
    
    
    /**
     * Set and return pagination configuration
     * @param type $totalRows
     * @param type $urlToCall
     * @param type $limit
     * @param type $attributes
     * @return boolean
     */
    public function setPaginationConfig($totalRows, $urlToCall, $limit, $attributes){
        $config = ['total_rows'=>$totalRows, 'base_url'=>base_url().$urlToCall, 'per_page'=>$limit, 'uri_segment'=>3,
            'num_links'=>5, 'use_page_numbers'=>TRUE, 'first_link'=>FALSE, 'last_link'=>FALSE,
            'prev_link'=>'&lt;&lt;', 'next_link'=>'&gt;&gt;', 'full_tag_open'=>"<ul class='pagination'>", 'full_tag_close'=>'</ul>', 
            'num_tag_open'=>'<li>', 'num_tag_close'=>'</li>', 'next_tag_open'=>'<li>', 'next_tag_close'=>'</li>',
            'prev_tag_open'=>'<li>', 'prev_tag_close'=>'</li>', 'cur_tag_open'=>"<li class='active'><a><b>", 
            'cur_tag_close'=>'</b></a></li>', 'attributes'=>$attributes];
        
        
        return $config;
    }
}
