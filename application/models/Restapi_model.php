<?php
class Restapi_Model extends CI_Model
{	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	function user_login($email,$password,$data)
    {
		  $this->db->select('*');
		  $this->db->from('user');
		  $this->db->where('email', $email);
		  $this->db->where('password', $password);
		  $this->db->limit(1);
		  $query = $this->db->get();
	  
		 if(count($query->result_array()) == 1)
		 {
			$successResult = $query->result_array();
			$userid=$successResult[0]['user_id'];
			$this->db->where('user_id', $userid);
			$this->db->update('user', $data);
		    return $successResult;
		 }
		 else
		 {
			return false;
		 }
	}
	function addUserRegData($data)
	{
	   $email=$data["email"];
	   $this->db->from('user');
	   $this->db->where('email',$email);
	   $query=$this->db->get();
	  
	   if(count($query->result_array()) > 0)
	   {
		   return "exist";
	   }
	   else
	   {
		    $this->db->insert('user', $data);
		    $userid=$this->db->insert_id();
		    return $userid;
	   }
	  
	}
	function latest_job_request_notifications($userid)
		{
			$resultRow=$this->lastLoginedTime($userid);
			$lastloginedtime=@$resultRow[0]['last_logined_time'];
		    $this->db->from('orders');
			$this->db->join('order_details','orders.order_id=order_details.order_id');
		    $this->db->join('user','user.user_id=orders.user_id');
			$this->db->join('service','service.service_id=order_details.service_id');
			$this->db->where('order_status','Pending');
			$loginuserid=$userid;
			$this->db->where('service.user_id',$loginuserid);
			$this->db->where('order_date >',@$lastloginedtime);
			$query=$this->db->get();
			return $query->result();
		}
		function latest_job_accept_notifications($userid)
		{
			$resultRow=$this->lastLoginedTime($userid);
			$lastloginedtime=@$resultRow[0]['last_logined_time'];
		    $this->db->from('orders');
			$this->db->join('order_details','orders.order_id=order_details.order_id');
			$this->db->join('service','service.service_id=order_details.service_id');
			$this->db->join('user','service.user_id=user.user_id');
			$this->db->where('order_status','Started');
			$loginuserid=$userid;
			$this->db->where('orders.user_id',$loginuserid);
			$this->db->where('order_accepted_time >',@$lastloginedtime);
			$query=$this->db->get();
			return $query->result();
		}
		function latest_job_reject_notifications($userid)
		{
			$resultRow=$this->lastLoginedTime($userid);
			$lastloginedtime=@$resultRow[0]['last_logined_time'];
		    $this->db->from('orders');
			$this->db->join('order_details','orders.order_id=order_details.order_id');
			$this->db->join('service','service.service_id=order_details.service_id');
			$this->db->join('user','service.user_id=user.user_id');
			$this->db->where('order_status','Rejected');
			$loginuserid=$userid;
			$this->db->where('orders.user_id',$loginuserid);
			$this->db->where('order_rejected_time >',@$lastloginedtime);
			$query=$this->db->get();
			return $query->result();
		}
		function latest_job_completed_notifications($userid)
		{
			$resultRow=$this->lastLoginedTime($userid);
			$lastloginedtime=$resultRow[0]['last_logined_time'];
		    $this->db->from('orders');
			$this->db->join('order_details','orders.order_id=order_details.order_id');
			$this->db->join('service','service.service_id=order_details.service_id');
			$this->db->join('user','service.user_id=user.user_id');
			$this->db->where('order_status','Completed');
			$loginuserid=$userid;
			$this->db->where('orders.user_id',$loginuserid);
			$this->db->where('order_completed_time >',@$lastloginedtime);
			$query=$this->db->get();
			return $query->result();
		}
		function job_notification_count($userid)
		{
			$latestJobRequestNotific=$this->latest_job_request_notifications($userid);
			$latestJobAcceptNotific=$this->latest_job_accept_notifications($userid);
			$latestJobRejectNotific=$this->latest_job_reject_notifications($userid);
			$latestJobCompleteNotific=$this->latest_job_completed_notifications($userid);
			$notificationCount=count($latestJobRequestNotific) + count($latestJobAcceptNotific) + count($latestJobRejectNotific) + count($latestJobCompleteNotific);
			return $notificationCount;
		}
		function lastLoginedTime($userid)
		{
		   $this->db->select('last_logined_time');
		   $this->db->where('user_id',$userid);
		   $this->db->from('user');	
		   $query=$this->db->get();
		   return $query->result_array();
		}
		function my_services($userid)
		{
			$query=$this->db->get_where('service', array('user_id' => $userid,'service_publish_stat' => 'yes'));
			$row = $query->result_array();
			$myserviceCount=count($row);
			return $myserviceCount;
		}
		function myJobCount($loginuserid)
		{
			 $this->db->from('order_details');
			 $this->db->join('orders','orders.order_id=order_details.order_id');
			 $this->db->join('service','service.service_id=order_details.service_id');
			 $this->db->where('service.user_id',$loginuserid);
			 $query=$this->db->get();
			 $count=$query->num_rows();
			 return $count;
		}
		function showMyOrderCount($userid)
		{
			$this->db->where('user_id',$userid);
			$this->db->from('orders');
			$query=$this->db->get();
			$myOrderCount = $query->num_rows();
			return $myOrderCount;
		}
		function showMyEarnings($userid)
		{
			$this->db->select('user_id, SUM(grand_total) AS total');
			$this->db->where('user_id',$userid);
			$this->db->group_by("user_id");
			$query=$this->db->get('orders');
			return $query->result_array();
		}
		function accountDetails($userid)
		{
		   $this->db->from('user');
		   $this->db->where('user_id',$userid);
		   $query=$this->db->get();	
		   return $query->result_array();
		}
		function categoryList()
		{
			$this->db->from('service_category_master');
			$query = $this->db->get();
			return $query;
		}
		function selsubcategoryList($categoryid=0)
		{
		    $this->db->select('service_sub_cat_id as sub_cat_id,service_sub_cat_title as sub_cat_name');
			$this->db->from('service_sub_category_master');
			$this->db->join('service_category_master', 'service_category_master.service_cat_id = service_sub_category_master.service_cat_id');
			$this->db->where('service_category_master.service_cat_id='.$categoryid);
			$query = $this->db->get();
			return $query->result();
		}
		function catSubcatDet()
		{
			$categoryListDet=$this->categoryList();
			$catDetArray=array();
			$subCatDetArray=array();
			$i=0;
			foreach($categoryListDet->result() as $catDet)
			{
				$subCatDetails=$this->selsubcategoryList($catDet->service_cat_id);
				$catDetArray[$i]=$catDet;
				$subCatDetArray[$i]=$subCatDetails;
				$i=$i+1;
			}	
			
			$details['catDet']=$catDetArray;
			$details['subcatDet']=$subCatDetArray;
			return $details;
			
			
		}
		function updateProfileData($data)
		{
		   $email=$data["email"];
		   $this->db->from('user');
		   $this->db->where('email',$email);
		   $query=$this->db->get();
		   if(count($query->result_array()) > 0)
		   {
			   $profRow=$query->result_array();
			   $userid=$profRow[0]["user_id"];
			   $this->db->where("user_id",$userid);
			   $this->db->update("user",$data);
			   return $userid;
		   }
		   else
		   {
			    return "false";
		   }
		}
		function discoverserviceList()
		{
			$this->db->select('service_id,service_title,service_image1,service.user_id as servuserid ,profile_pic,basic_price,total_likes,total_comments,rating,user.user_id,name,post_date as service_date,is_suggested_service');
			$this->db->from('service');
			$this->db->join('user','service.user_id=user.user_id');
			$this->db->where('service_publish_stat','yes');
			$query = $this->db->get();
			return $query;
		}
		function isliked($serviceid,$userid)
		{
		    $this->db->from('likes');	
			$this->db->where('user_id',$userid);
			$this->db->where('service_id',$serviceid);
			$query=$this->db->get();
			if(count($query->result_array())>0)
			{
				return "true";
			}
			else
			{
				return "false";	
			}
		}
		
		function isfollowing($servuserid,$userid)
		{
		    $this->db->from('followers');	
		    $this->db->where('following_id',$userid);
		    $this->db->where('follower_id',$servuserid);
			$query=$this->db->get();
		    if(count($query->result_array())>0)
			{
				return "true";
			}
			else
			{
			    return "false";	
			}
		}
		
		function is_followed($userid)
		{
		    $this->db->from('followers');	
		    $this->db->where('following_id',$userid);
			$query=$this->db->get();
		    if(count($query->result_array())>0)
			{
				return "true";
			}
			else
			{
			    return "false";	
			}
		}
		
		function serviceDetail($serviceid)
		{
			$this->db->select('service_id,service_title,service_description,service.user_id as servuserid ,name,profile_pic as user_pic,basic_price,total_likes,total_comments,rating,user.user_id,post_date,is_suggested_service,service_image1,service_image1_thumb,	service_image2,service_image2_thumb,service_image3,service_image3_thumb,service_image4,service_image4_thumb');
			$this->db->from('service');
			$this->db->join('user','service.user_id=user.user_id');
			$this->db->where('service_id',$serviceid);
			$query = $this->db->get();
			return $query;
		}
		function serviceDetails($serviceid)
		{
			$this->db->select('service.user_id,service_title,service.service_cat_id as category,service_category_master.service_cat_title as categoryname,service.service_sub_cat_id as sub_category, service_sub_category_master.service_sub_cat_title as subcategoryname,service_description,location_anywhere as location_anywhere,	service_location as service_location,service_location_latitude as service_location_latitude,service_location_longitude as service_location_longitude,basic_price,service_image1,service_image1_thumb,	service_image2,service_image2_thumb,service_image3,service_image3_thumb,service_image4,service_image4_thumb,service_publish_stat');
			$this->db->from('service');
			$this->db->join('service_category_master','service_category_master.service_cat_id=service.service_cat_id');
			$this->db->join('service_sub_category_master','service_sub_category_master.service_sub_cat_id=service.service_sub_cat_id');
			$this->db->join('user','service.user_id=user.user_id');
			$this->db->where('service_id',$serviceid);
			$query = $this->db->get();
			//echo $this->db->last_query();
			return $query;
		}
		function otherServices($serviceid,$userid)
		{
			$this->db->from('service');
			$this->db->where('service_id!='.$serviceid);
			$this->db->where('user_id='.$userid);
			$this->db->where("service_publish_stat = 'yes'");
			$query = $this->db->get();
			//echo "last query = " . $this->db->last_query();
			return $query;
		}
		function my_services_act($userid)
		{
			$this->db->select('service_id,service_title,service_description,service_image1,service.user_id as servuserid ,name,profile_pic as user_pic,basic_price,total_likes,total_comments,rating,user.user_id,post_date');
			$this->db->order_by("service_id", "desc");
			$this->db->where(array('service.user_id' => $userid,'service_publish_stat' => 'yes'));
			$this->db->from('service');
			$this->db->join('user','service.user_id=user.user_id');
			$query=$this->db->get();
			return $query->result_array();
		}
		function my_services_inc($userid)
		{
			$this->db->select('service_id,service_title,service_description,service_image1,service.user_id as servuserid ,name,profile_pic as user_pic,basic_price,total_likes,total_comments,rating,user.user_id,post_date');
			$this->db->order_by("service_id", "desc");
			$this->db->where(array('service.user_id' => $userid,'service_publish_stat' => 'no'));
			$this->db->from('service');
			$this->db->join('user','service.user_id=user.user_id');
			$query=$this->db->get();
			return $query->result_array();
		}
		function PendingMyjobServDet($userid)   // pending my jobs detais, my profile details, my service details 
	    {
			 $this->db->select('order_details.job_id,order_details.service_id,service_title,service_image1 as service_pic,post_date as service_date,order_date as requested_date,user.user_id,name as user_name,profile_pic as user_pic,
			 basic_price,grand_total as ordered_total_price,mobile as contact_phone, email as contact_email,sub_total as extras_ordered,extra_id as id,extra_price as price');
			 $this->db->from('order_details');
			 $this->db->join('service','order_details.service_id=service.service_id and service.user_id='.$userid);
			 $this->db->join('orders','orders.order_id=order_details.order_id');
			 $this->db->join('user','orders.user_id=user.user_id');
			 $this->db->where('order_status','Pending');
			 $this->db->order_by('job_id','desc');
			 $query=$this->db->get();
			 return $query;
	   }
	   function jobRequestedUserDet($userid)
		{
			$this->db->from('orders');
			$this->db->join('user','orders.user_id=user.user_id');
			$this->db->where('user.user_id',$userid);
			$query=$this->db->get();
			return $query->result_array();
		}
		function extraServiceDetails($extserviceid)
		{
			    $this->db->select("extra_service_id as id,extra_serv_description as title,extra_serv_price as price");
				$this->db->from('extra_service');
				$this->db->where('extra_service.extra_service_id',$extserviceid);
				$query=$this->db->get();
				return $query->row();
		}
		
		function extraServicExist($serviceid)
		{
				$this->db->from('service');
				$this->db->join('extra_service','service.service_id=extra_service.service_id');
				$this->db->where('service.service_id',$serviceid);
				$query=$this->db->get();
				return $query;
		}
		
		function ActiveMyjobServDet($userid)   // active my jobs detais, my profile details, my service details 
	    {
			 $this->db->select('order_details.job_id,order_details.service_id,service_title,service_image1 as service_pic,post_date as service_date,order_date as requested_date,user.user_id,name as user_name,profile_pic as user_pic,
			 basic_price,grand_total as ordered_total_price,mobile as contact_phone, email as contact_email,sub_total as extras_ordered,extra_id as id,extra_price as price');
			 $this->db->from('order_details');
			 $this->db->join('service','order_details.service_id=service.service_id and service.user_id='.$userid);
			 $this->db->join('orders','orders.order_id=order_details.order_id');
			 $this->db->join('user','orders.user_id=user.user_id');
			 $this->db->where('order_status','Started');
			 $this->db->order_by('job_id','desc');
			 $query=$this->db->get();
			 return $query;
	   }
	   function CompletedMyjobServDet($userid)   // active my jobs detais, my profile details, my service details 
	    {
			 $this->db->select('order_details.job_id,order_details.service_id,service_title,service_image1 as service_pic,post_date as service_date,order_date as requested_date,user.user_id,name as user_name,profile_pic as user_pic,
			 basic_price,grand_total as ordered_total_price,mobile as contact_phone, email as contact_email,sub_total as extras_ordered,extra_id as id,extra_price as price');
			 $this->db->from('order_details');
			 $this->db->join('service','order_details.service_id=service.service_id and service.user_id='.$userid);
			 $this->db->join('orders','orders.order_id=order_details.order_id');
			 $this->db->join('user','orders.user_id=user.user_id');
			 $this->db->where('order_status','Completed');
			 $this->db->order_by('job_id','desc');
			 $query=$this->db->get();
			 return $query;
	   }
	   function extra_ServiceDetails($serviceid)  // show al extra services of specified serviceid
		{
			    $this->db->select("extra_service_id as id,extra_serv_description as title,extra_serv_price as price");
				$this->db->from('service');
				$this->db->join('extra_service','service.service_id=extra_service.service_id');
				$this->db->where('service.service_id',$serviceid);
				$query=$this->db->get();
				return $query->result_array();
		}
		
		function ext_ServDtls($serviceid)  // show al extra services of specified serviceid
		{
			    $this->db->select("extra_serv_description as desc,extra_serv_price as price");
				$this->db->from('service');
				$this->db->join('extra_service','service.service_id=extra_service.service_id');
				$this->db->where('service.service_id',$serviceid);
				$query=$this->db->get();
				return $query->result_array();
		}
		function get_profile($userid) 
		{
			$this->db->select('user_id,name as user_name,profile_pic as user_pic, about_user as about,email,mobile as phone,total_followers as followers, total_following as following');
			$this->db->from("user");
			$this->db->where("user_id",$userid);
			$query = $this->db->get();
			return $query->result_array();
		}
		function myServices($userid)
		{
			$query=$this->db->get_where('service', array('user_id' => $userid,'service_publish_stat' => 'yes'));
			return $query->result_array();
		}
		function suggestedprofile($userid)
	    {
			$this->db->select("user_id,name as user_name,profile_pic as user_pic");
			$this->db->from("user");
			$this->db->where('user_id !=',$userid);  
			$this->db->where('admin_suggested_user =','1');
			$this->db->where("total_points > ",5);
			$this->db->order_by("total_points", "desc");
			$query = $this->db->get();    
			// echo "last query = " . $this->db->last_query();
			return $query->result_array();
	    }
	  	function feedsList()
		{
			$this->db->select('is_suggested_service,service_id,service_title,service_image1 as service_pic,user.user_id,profile_pic as user_pic,basic_price,total_likes as likes,total_comments as comments,rating,name,post_date as service_date,service.user_id as servuserid');
			$this->db->from('service');
			$this->db->join('user','service.user_id=user.user_id');
			$this->db->where('service_publish_stat','yes');
		//	$this->db->where('is_suggested_service','yes');
		    $this->db->order_by("service_date", "desc");
			$query = $this->db->get();
			return $query->result_array();
		}
		function likesListDet($serviceid)
		{
			 $this->db->select('user.user_id,name as user_name,profile_pic as user_pic,date');
			 $this->db->from('likes');
			 $this->db->join('user','likes.user_id=user.user_id');
			 $this->db->where('likes.service_id',$serviceid);
			 $query=$this->db->get();
			 return $query->result_array();
		}
	
	    function commentListDet($serviceid)
		{
			 $this->db->select('user.user_id,name as user_name,profile_pic as user_pic,comments as comment,date');
			 $this->db->from('comments');
			 $this->db->join('user','comments.user_id=user.user_id');
			 $this->db->where('comments.service_id',$serviceid);
			 $query=$this->db->get();
			 return $query->result_array();
		}
		function saveFollowDetails($data)
		{
			  $this->db->insert('followers',$data);	
		}
		function getFollowerDetails($follower_id)
		{
			  $query = $this->db->get_where('user', array('user_id' => $follower_id));
		      return $query->result_array();
		}
		function updateFollowersCount($follower_id,$datacnt1)
		{
		    $this->db->where('user_id', $follower_id);
		    $this->db->update('user',$datacnt1);		
		}
		function getFollowingDetails($following_id)
		{
		   $query = $this->db->get_where('user', array('user_id' => $following_id));
		   return $query->result_array();	
		}
		function updateFollowingCount($following_id,$datacnt2)
		{  
		   $this->db->where('user_id', $following_id);
		   $this->db->update('user',$datacnt2);	
		}
		function getFollowCount($following_id,$follower_id)
		{
			  $query = $this->db->get_where('followers', array('following_id' => $following_id,'follower_id' => $follower_id));  
		      $following = $query->result_array();
			  return $following;
		}
		function rmvFollower($follow_id)
		{
			$this->db->where('follow_id', $follow_id);
			$this->db->delete('followers');
		}
		function searchDiscServBycatSubcat($categoryId,$SubCategoryId)
	    {
			$this->db->select('service_id,service_title,service_image1,service.user_id as servuserid ,profile_pic,basic_price,total_likes,total_comments,rating,user.user_id,name,post_date as service_date,is_suggested_service');
			$this->db->from('service');
			$this->db->join('user','service.user_id=user.user_id');
			if($categoryId != "" && $categoryId != 0)
			{
				$this->db->where('service.service_cat_id',$categoryId);
			}
			if($SubCategoryId != "" && $SubCategoryId != 0)
			{
			   $this->db->where('service.service_sub_cat_id',$SubCategoryId);
			}
			$this->db->where('service_publish_stat','yes');
			$this->db->order_by("service_id", "asc");
			$query = $this->db->get();
			return $query;
	   }
	   function getFollowersList($followerid)
	   {
		   $this->db->select('user_id, name as user_name,profile_pic as user_pic');
		   $this->db->from('followers');
		   $this->db->where('follower_id',$followerid);
		   $this->db->join('user','user.user_id=followers.following_id');
		   $query=$this->db->get();
		//   echo "query = " . $this->db->last_query();
		   return $query->result_array();
	   }
	    function getFollowingList($followingid)
	   {
		   $this->db->select('user_id, name as user_name,profile_pic as user_pic');
		   $this->db->from('followers');
		   $this->db->where('following_id',$followingid);
		   $this->db->join('user','user.user_id=followers.follower_id');
		   $query=$this->db->get();
		   return $query->result_array();
	   }
	   function equallyFollowed($userid,$followerid)
	   {
		   $this->db->from('followers');
		   $this->db->where('following_id',$userid);
		   $this->db->where('follower_id',$followerid);
		   $query=$this->db->get();
		    if(count($query->result_array())>0)
			{
				return "true";
			}
			else
			{
			    return "false";	
			}
	   }
	   function showServiceDetails($serviceid)
	   {
		   $this->db->from('service');
		   $this->db->where('service_id',@$serviceid);
		   $query=$this->db->get();
		   $row=$query->result_array();
		   return $row;
	   }
	   function extServiceDetails($serviceid,$extraservids)
	   {
				//$extraservids=explode(',',$extraserviceids);
				$this->db->from('extra_service');
				$this->db->where_in('extra_service.extra_service_id', $extraservids);
				$query=$this->db->get();
				return $query;
	    }
		
		function ext_Service_Details($serviceid,$extraserviceids)
	   {
		        $extraservids=explode(',',$extraserviceids);
				$this->db->from('extra_service');
				$this->db->where_in('extra_service.extra_service_id', $extraservids);
				$query=$this->db->get();
				return $query;
	    }
		
	   function serviceUserDetails($userid)
		{
			$this->db->from('service');
			$this->db->join('user','service.user_id=user.user_id');
			$this->db->where('user.user_id',$userid);
			$query=$this->db->get();
			return $query;
		}
	   public function saveCart($userid,$serviceid,$data)
	   {
			$this->db->from('cart');
			//$rowid=$data['rowid'];
			$this->db->where('userid',$userid);
			$this->db->where('serviceid',$serviceid);
		//	$this->db->where('rowid',$rowid);
			$query=$this->db->get();
			$row=$query->result_array();
	     	
			
			if(count($query->result_array()) > 0)
			{
				$cartid=$row[0]['cart_id'];
				$this->update_Cart($data,$cartid);
			}
			else
			{
			   $this->db->insert('cart',$data);
			}
	  }
	/*  public function updateCart($data,$rowid)
	  {
			$this->db->where('rowid',$rowid);
			$this->db->update('cart',$data);
	  }*/
	  public function showCartData($userid)
	  {
			$this->db->from('cart');
			$this->db->order_by('cart_id','desc');
			$this->db->where('userid',$userid);
			$query=$this->db->get();
			return $query->result_array();
	  }
	 public function deleteCartItem($userid,$serviceid)
	 {
		$this->db->where('userid',$userid);
		$this->db->where('serviceid',$serviceid);
		$this->db->delete('cart');
	 }
	 public function update_Cart($data,$cartid)
	 {
			$this->db->where('cart_id',$cartid);
			$this->db->update('cart',$data);
	 }
	 public function removeExtraServId($userid,$serviceid,$extrasid)
	 {
		$this->db->select('extservid,cart_id'); 
		$this->db->where('userid',$userid);
		$this->db->where('serviceid',$serviceid);
		$this->db->from('cart');
		$query=$this->db->get(); 
		$row=$query->result_array();
		$extservid=unserialize($row[0]['extservid']);
		$cartid=$row[0]['cart_id'];
        $unsetPos=array_search($extrasid,$extservid);
		unset($extservid[$unsetPos]);
		$data['extservid']=serialize($extservid);
		$this->update_Cart($data,$cartid);
	 }
	 public function cartCount($userid)
	 {
	 	$this->db->from('cart');
		$this->db->where('userid',$userid);
		$query=$this->db->get();
		return count($query->result_array());
	}
	function saveOrderInfo($data)
	{
		$this->db->insert('orders', $data);
		$insert_id = $this->db->insert_id();
  		return $insert_id;
	}
	function serviceidExistOrder($serviceid,$orderid)
	{
		$this->db->where('service_id',$serviceid);
		$this->db->where('order_id',$orderid);
		$this->db->from('order_details');
		$query=$this->db->get();
		if($query->result_array())
		   $stat="yes";
		else
		   $stat="no";
		return $stat;
	}
	function updateGrandTotal($data,$orderid)
	{
		$this->db->where('order_id',$orderid);
		$this->db->update('orders', $data);
	}
	function saveOrderDetailsInfo($data)
	{
		$this->db->insert('order_details', $data);
	}
	public function deleteCartData($userid)
	{
		$this->db->where('userid',$userid);
		$query=$this->db->delete('cart');
	}
	function showUserOrder($userid)
	{
		$this->db->select('order_id as order_no,no_of_services as total_services,grand_total as total_price,order_date as ordered_date');
		$this->db->where('orders.user_id',$userid);
		$this->db->order_by('orders.order_id','desc');
		$this->db->from('orders');
		$query=$this->db->get();
		return $query;
	}
	function showOrderDetails($orderid)
	{
		$this->db->where('order_details.order_id',$orderid);
		$this->db->from('order_details');
		$this->db->join('service','service.service_id=order_details.service_id');
	//	$this->db->join('orders','orders.order_id=order_details.order_id');
		$this->db->join('user','service.user_id=user.user_id');
		$query=$this->db->get();
		return $query;
	}
	function updateJobStatus($jobId,$data)
	{
		$this->db->where('job_id',$jobId);
		$this->db->update('order_details',$data);
	}
	function job_request_notifications($userid)
	{
		$this->db->from('orders');
		$this->db->join('order_details','orders.order_id=order_details.order_id');
		$this->db->join('user','user.user_id=orders.user_id');
		$this->db->join('service','service.service_id=order_details.service_id');
		$this->db->where('order_status','Pending');
		$this->db->where('service.user_id',$userid);
		$query=$this->db->get();
		return $query->result();
	}
	function job_accept_notifications($userid)
	{
		$this->db->from('orders');
		$this->db->join('order_details','orders.order_id=order_details.order_id');
		$this->db->join('service','service.service_id=order_details.service_id');
		$this->db->join('user','service.user_id=user.user_id');
		$this->db->where('order_status','Started');
		$this->db->where('orders.user_id',$userid);
		$query=$this->db->get();
		return $query->result();
	}
	function job_reject_notifications($userid)
	{
		$this->db->from('orders');
		$this->db->join('order_details','orders.order_id=order_details.order_id');
		$this->db->join('service','service.service_id=order_details.service_id');
		$this->db->join('user','service.user_id=user.user_id');
		$this->db->where('order_status','Rejected');
		$this->db->where('orders.user_id',$userid);
		$query=$this->db->get();
		return $query->result();
	}
	function job_completed_notifications($userid)
	{
		$this->db->from('orders');
		$this->db->join('order_details','orders.order_id=order_details.order_id');
		$this->db->join('service','service.service_id=order_details.service_id');
		$this->db->join('user','service.user_id=user.user_id');
		$this->db->where('order_status','Completed');
		$this->db->where('orders.user_id',$userid);
		$query=$this->db->get();
		return $query->result();
	}
	function OrderList($userid)
	{
        $this->db->select('service_image1_thumb,service_title,sub_total');
	    $this->db->select("DATE_FORMAT(order_date,'%b %D,%Y') as order_date");
		$this->db->from('orders');
		$this->db->join('order_details','orders.order_id=order_details.order_id');
		$this->db->join('service','order_details.service_id=service.service_id');
		$this->db->join('user','service.user_id=user.user_id');
		$this->db->where('orders.user_id',$userid);
		$query=$this->db->get();
		return $query->result_array();
	}
	function like($data)
	{
	    $result =  $this->db->insert('likes', $data);
	    if(!empty($result)) 
		{
			$service_id = $data['service_id'];
			$this->db->set('total_likes', 'total_likes+1', FALSE);
			$this->db->where('service_id', $service_id);
			$this->db->update('service');			
		}		
	}
	function like_delete($user_id,$service_id)
	{
		   $this->db->where('user_id', $user_id);
		   $this->db->where('service_id', $service_id);
		   $result = $this->db->delete('likes');
		   if(!empty($result))
		   {
			 $this->db->set('total_likes', 'total_likes-1', FALSE);
			 $this->db->where('service_id', $service_id);
			 $this->db->update('service');			
		   }		
	}
	function like_status($user_id,$service_id)
	{
		 $this->db->from('likes');
		 $this->db->where('user_id', $user_id);
		 $this->db->where('service_id', $service_id);
		 $query=$this->db->get();
		 return $query->result_array();
	}
	function addComment($data,$serviceid)
	{
		  $this->db->insert('comments', $data);	
		  $this->db->set('total_comments', 'total_comments+1', FALSE);
		  $this->db->where('service_id', $serviceid);
		  $this->db->update('service');
	}
	function saveServicePost($data)
	{
		$this->db->insert('service',$data);	
	    //echo $this->db->last_query();
		return $this->db->insert_id();
	}
	function updateServicePost($data,$service_id)
	{
		$this->db->where('service_id', $service_id);
		$this->db->update('service', $data);
	//	echo $this->db->last_query();
	}
	function updatePublishStatus($data,$serviceid)
	{
		$this->db->where('service_id', $serviceid);
		$this->db->update('service', $data);
	}
	function savePricingDetailsInfo($data,$serviceid)
	{
		$this->db->where('service_id',$serviceid);
		$this->db->delete('extra_service');
		$this->db->insert_batch('extra_service', $data);
	}
	function getchannelId($senderId,$receiverId)
	{
		$this->db->select('*');
		$this->db->from('chat_channel_det');
		$this->db->where('sender_id',$senderId);
		$this->db->where('receiver_id',$receiverId);
		$query=$this->db->get();
		if(count($query->result_array()) > 0)
		{
			$result=$query->result_array();
			$channelid=$result[0]['channel_id'];
			return $channelid;
		}
		else
		{
			$this->db->select('*');
			$this->db->from('chat_channel_det');
			$this->db->where('sender_id',$receiverId);
			$this->db->where('receiver_id',$senderId);
			$query=$this->db->get();
			if(count($query->result_array()) > 0)
			{
				$result=$query->result_array();
				$channelid=$result[0]['channel_id'];
				return $channelid;
			}
			else
			{
			    $data['sender_id']=$senderId;
			    $data['receiver_id']=$receiverId;
			    $this->db->insert('chat_channel_det',$data);
			//	echo "last query = " . $this->db->last_query();
				return $this->db->insert_id();   // no records found then insert
			}
		}
	}
	function getSelUserList($loginUserId)
	{
		  $this->db->select('receiver_id as user_id,channel_id,chat_stat,lastmsg_recuser,last_message');
		  $this->db->from('chat_channel_det');
		  $this->db->where('sender_id',$loginUserId);
		  $query=$this->db->get();
		  $channel_list1=$query->result_array();
		  $this->db->select('sender_id as user_id,channel_id,chat_stat,lastmsg_recuser,last_message');
		  $this->db->from('chat_channel_det');
		  $this->db->where('receiver_id',$loginUserId);
		  $query=$this->db->get();
		  $channel_list2=$query->result_array();
		  $data=array_merge($channel_list1,$channel_list2);
		  return $data;
	}
	function getSelUsersDet($userid,$channel_id,$chat_stat,$lastmsg_recuser,$last_message)
	{
		  $this->db->select('name as username,profile_pic as user_pic');
		  $this->db->from('user');
		  $this->db->where('user_id',$userid);
		  $query = $this->db->get();
	      $resArray=$query->result_array();
		  $resArray[0]['user_id']=$userid;
		  $resArray[0]['channel_id']=$channel_id;
		  $resArray[0]['chat_stat']=$chat_stat;
		  $resArray[0]['lastmsg_recuser']=$lastmsg_recuser;
		  $resArray[0]['last_message']=$last_message;
		  return $resArray[0];
	}
	function getUserdata($userid)
	{
		  $this->db->select('name as user_name,profile_pic as user_pic');
		  $this->db->from('user');	
		  $this->db->where('user_id',$userid);
		  $query=$this->db->get();
		  $userDet=$query->result_array();
		  return $userDet;
	}
	function saveReview($insertData)
	{
		$this->db->insert('star_rating', $insertData);	
		$serviceid=$insertData['service_id'];
		$rating=$insertData['rating'];
		
		$query1=$this->db->get_where('star_rating', array('service_id' => $serviceid));
		$reviews = $query1->result_array();
		    
	    $this->db->set('service_points', 'service_points+1+'.$rating, FALSE);
	    $this->db->where('service_id', $serviceid);
	    $this->db->update('service');
	   
		$queryserv = $this->db->get_where('service', array('service_id' => $serviceid));
		$rowserv = $queryserv->result_array();
		$user_id  = $rowserv[0]['user_id'];
		
		$this->db->set('total_points', 'total_points+1+'.$rating, FALSE);
		$this->db->where('user_id', $user_id);
		$this->db->update('user');	
		
		$max = 0;
		$n = count($reviews); 
		foreach($reviews as $rate => $count)
		{ 
		  $max = $max+$count['rating'];
		}
		$ratingcount_tot = $max / $n;
		$ratingcount = array('rating' => $ratingcount_tot); 
		$this->db->where('service_id',$serviceid);
		$this->db->update('service', $ratingcount);	
		
	}
	function reviewDetails($service_id)
	{
	    $this->db->select('star_rating.*,user.*');
        $this->db->from('star_rating');
        $this->db->join('user', 'star_rating.user_id = user.user_id'); 
		$this->db->where('star_rating.service_id', $service_id);
		$this->db->order_by("star_rating.date", "desc");
        $query = $this->db->get();
        return $query; 	
	}
	function ratingDet($orderno,$serviceid,$userid)
	{
		$this->db->from('star_rating');
		$this->db->where('order_id',$orderno);
		$this->db->where('service_id',$serviceid);
		$this->db->where('user_id',$userid);
		$query=$this->db->get();
		return $query->result_array();
	}
}
?>