<?php
class Api_model extends CI_Model {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }



	
//--------------Category	
		
	function categorylist()
	{
   
		$this->db->select('*');
        $this->db->where("status='on'");
		$this->db->from('category');
		$this->db->order_by("category_name","asc");
		$query = $this->db->get();
		return $query->result();
			
	}

//--------------SubCategory	
	
	
	function subcategorylist()
	{

		$this->db->select('C.category_id,C.category_name,C.category_name_ar,C.status,SC.subcategory_name,SC.subcategory_name_ar,SC.status,SC.subcategory_id');
		$this->db->from('category C, subcategory SC');
        $this->db->where("SC.category_id=C.category_id  and C.status='on'");
		$this->db->order_by("SC.subcategory_name","asc");
		$query = $this->db->get();
		return $query->result();
			
	}


function Subcategory($langauge)
	{
   
      if($langauge=='eng'){
		$this->db->select('subcategory_id,subcategory_name as title,subcategory_icon as subcat_pic');
        $this->db->where("status='on'");
		$this->db->from('subcategory');
		$this->db->order_by("subcategory_name","asc");
      }
      if($langauge=='arb'){
        $this->db->select('subcategory_id,subcategory_name_ar as title,subcategory_icon as subcat_pic');
        $this->db->where("status='on'");
		$this->db->from('subcategory');
		$this->db->order_by("subcategory_name","asc");
      } 

		$query = $this->db->get();
		return $query->result();
			
	}
	
//-------------------Brand
	
	
	function brandList()
	{
		$this->db->select('*');
		$this->db->from('brands');
		$this->db->where("trash=0 and status=0");
		$this->db->order_by("brand_name","asc");
		$query = $this->db->get();
		return $query->result();	
	}


//-------------------Currency
	

	function currencyList()
	{
		$this->db->select('*');
		$this->db->from('currency_master');
		$this->db->where("trash=0 and status=0");
		$this->db->order_by("currency_name","asc");
		$query = $this->db->get();
		return $query->result();	
	}

//--------------------Delivery Mode
	
	function deliveryModeList()
	{
		$this->db->select('payment_mode_id,payment_mode');
		$this->db->from('payment_mode_master');
		$this->db->where("trash=0");
		$this->db->order_by("payment_mode","asc");
		$query = $this->db->get();
		return $query->result();	
	}


//----------------Status Master

	
	function statusMasterList()
	{
		$this->db->select('*');
		$this->db->from('status_master');
		$this->db->where("trash=0");
		$this->db->order_by("status_name","asc");
		$query = $this->db->get();
		return $query->result();	
	}


//----------------Shipping charge Master

	
	function shippingChargeList()
	{
		$this->db->select('sc.shipping_charge_id,sc.currency_id,sc.shipping_charge,sc.status,cm.currency_code,cm.decimal_point,cm.default_currency,cm.currency_name');
		$this->db->from('shipping_charge_master sc, currency_master cm');
		$this->db->where("sc.trash=0 and sc.status=0 and cm.trash=0 and cm.status=0 and sc.currency_id=cm.currency_id");
		$this->db->order_by("sc.shipping_charge_id","desc");
		$query = $this->db->get();
		return $query->result();	
	}


//--------------------Products

function productList()
	{
		$this->db->select('*');
		$this->db->from('products');
        $this->db->where("trash=0 and status='on'");
		$this->db->order_by("product_name","asc");
		$query = $this->db->get();
		return $query->result();	
	}

function updateProduct($dataProd,$product_id)
	{
		$this->db->where('product_id', $product_id);
		$this->db->update('products', $dataProd); 		
	}


function checkPriceExist($currency_id)
	{
		$this->db->select('P.product_id,P.trash,Cm.currency_id,Pp.product_id, Pp.currency_id,Pp.price_details_id');
		$this->db->from('products P, product_price_details Pp,currency_master Cm');
        $this->db->where("Pp.currency_id='$currency_id' and Pp.product_id=P.product_id and P.trash=0");
		$this->db->group_by("Pp.currency_id");
		$query = $this->db->get();
		return $query->result();	
	}

function insertpriceDetails($dataPrice)
	{
		$this->db->insert('order_price_details', $dataPrice);
	}



function productPrice_list($product_id,$currency_id)
	{
		$this->db->select('P.product_id,P.status,P.trash,Cm.currency_id, Cm.currency_code,Cm.status, Cm.trash,Pp.product_id, Pp.currency_id, Pp.price_normal, Pp.price_offer ,Pp.price_details_id,Pp.off_percentage');
		$this->db->from('products P, product_price_details Pp,currency_master Cm');
        $this->db->where("Cm.status=0 and Cm.trash=0 and P.status='on' and P.trash=0  and Pp.product_id='$product_id' and Pp.currency_id='$currency_id' and Pp.product_id=P.product_id and Pp.currency_id=Cm.currency_id");
		//$this->db->order_by("p.product_id","desc");
		$query = $this->db->get();
		return $query->result();	
	}

function productjoinList()
	{
		$this->db->select('p.product_id,p.product_name,p.product_name_ar,p.product_type,p.product_details ,p.product_details_ar,p.product_image,p.product_icon,p.trash,p.status,p.product_sale_count,p.product_stock,c.category_name,c.category_name_ar,c.category_id,sc.subcategory_id,sc.subcategory_name,sc.subcategory_name_ar, b.brand_name,b.brand_name_ar,b.brand_image');
		$this->db->from('products p, brands b,category c, subcategory sc');
        $this->db->where("p.category_id=c.category_id and p.subcategory_id=sc.subcategory_id and p.brand_id=b.brand_id and p.trash=0");
		$this->db->order_by("p.product_id","desc");
		$query = $this->db->get();
		return $query->result();	
	}



//-------------------Product Deals

function productDealsList()
	{
   
        $this->db->select('p.product_id,p.product_name,p.product_name_ar,p.product_type,p.product_details ,p.product_details_ar,p.product_image,p.product_icon,p.product_stock,p.deal_status,p.deal_trash,p.trash, p.status,c.category_name,c.category_name_ar,c.category_id,sc.subcategory_id,sc.subcategory_name,sc.subcategory_name_ar, b.brand_name,b.brand_name_ar,b.brand_image,pr.currency_id,pr.price_normal,pr.off_percentage,pr.price_offer,cm.currency_id,cm.currency_name,cm.currency_code,cm.decimal_point,cm.status,cm.trash');
		$this->db->from('products p, brands b,category c, subcategory sc ,product_price_details pr,currency_master cm');
        $this->db->where("p.trash=0 and p.status='on' and p.deal_status=0 and p.deal_trash=0 and p.product_stock >0 and p.category_id=c.category_id and p.subcategory_id=sc.subcategory_id and p.brand_id=b.brand_id and p.product_type='deals' and pr.currency_id=cm.currency_id and cm.status=0 and cm.trash=0 and p.product_id=pr.product_id");
		$this->db->order_by("p.product_id","desc");
        $this->db->group_by("p.product_id");
		$query = $this->db->get();
		return $query->result();	
	}


//-------------------Product List

function getProductsList()
	{
   
        $this->db->select('p.product_id,p.product_name,p.product_name_ar,p.product_type,p.product_details ,p.product_details_ar,p.product_image,p.product_icon,p.product_stock,p.product_sale_count,p.status,p.deal_status,p.trash,p.deal_trash,c.category_name,c.category_name_ar,c.category_id,sc.subcategory_id,sc.subcategory_name,sc.subcategory_name_ar, b.brand_name,b.brand_name_ar,b.brand_image,pr.currency_id,pr.price_normal,pr.off_percentage,pr.price_offer,cm.currency_id,cm.currency_name,cm.currency_code,cm.decimal_point,cm.status,cm.trash');
		$this->db->from('products p, brands b,category c, subcategory sc ,product_price_details pr,currency_master cm');
        $this->db->where("p.trash=0 and p.status='on' and p.product_stock >0 and p.category_id=c.category_id and p.subcategory_id=sc.subcategory_id and p.brand_id=b.brand_id and  p.trash=0 and pr.currency_id=cm.currency_id and cm.status=0 and cm.trash=0 and p.product_id=pr.product_id");
		$this->db->order_by("p.product_id","desc");
        $this->db->group_by("p.product_id");
		$query = $this->db->get();
		return $query->result();	
	}



//-------------------GEt SUggection Product List

function getSuggectionList()
	{
   
        $this->db->select('p.product_id,p.product_name,p.product_name_ar,p.product_type,p.product_details ,p.product_sale_count,p.product_details_ar,p.product_image,p.product_icon,p.product_stock,p.status,p.deal_status,p.trash,p.deal_trash,c.category_name,c.category_name_ar,c.category_id,sc.subcategory_id,sc.subcategory_name,sc.subcategory_name_ar, b.brand_name,b.brand_name_ar,b.brand_image,pr.currency_id,pr.price_normal,pr.off_percentage,pr.price_offer,cm.currency_id,cm.currency_name,cm.currency_code,cm.decimal_point,cm.status,cm.trash');
		$this->db->from('products p, brands b,category c, subcategory sc ,product_price_details pr,currency_master cm');
        $this->db->where("p.trash=0 and p.status='on' and p.product_stock >0 and p.category_id=c.category_id and p.subcategory_id=sc.subcategory_id and p.brand_id=b.brand_id and  p.trash=0 and pr.currency_id=cm.currency_id and cm.status=0 and cm.trash=0 and p.product_id=pr.product_id");
		$this->db->order_by("p.product_sale_count","desc");
        $this->db->group_by("p.product_id");
		$query = $this->db->get();
		return $query->result();	
	}




//------------------CMS Pages

	
	
	function pagesList()
	{
		$this->db->select('*');
		$this->db->from('pages');
		$this->db->where("trash='off'");
		$this->db->order_by("page_name","ASC");
		$query = $this->db->get();
		return $query->result();	
			
	}
	


	
	
	
//-------------------CLIENTS 
	
	function clientlist()
	{
		$this->db->select('*');
		$this->db->from('clients');
		$this->db->where("trash=0");
		$this->db->order_by("sp_order","asc");
        $this->db->order_by("sponsor_category","asc");
		$query = $this->db->get();
		return $query->result();			
	}
	
//------------------customer	
	
	
	function customerList()
	{
		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where("trash=0");
		$this->db->order_by("customer_id","desc");
		$query = $this->db->get();
		return $query;		
	}
	


//------------------User Details	
	
	
	function userList($user_id)
	{
		$this->db->select('*');
		$this->db->from('tgnuser');
		$this->db->where("User_ID='$user_id'");
		$query = $this->db->get();
		return $query->result();			
	}
	
	


//------------Product Order 

function cartInsert($dataSet)
	{
	    $this->db->insert('order_temp_cart', $dataSet);  
	}

function cartTempList()
	{
		$this->db->select('*');
		$this->db->from('order_temp_cart');
		$this->db->order_by("cart_id","desc");
		$query = $this->db->get();
		return $query->result();				
	}

function cartTempUpdate($dataSet,$product_id,$user_id)
	{
		$this->db->where("product_id='$product_id' and user_id='$user_id'");
		$this->db->update('order_temp_cart', $dataSet); 		
	}

function cartTemp_delete($user_id,$product_id)
{
   $this->db->where("user_id='$user_id' and product_id='$product_id'");
   $this->db->delete('order_temp_cart'); 
}
   

function orderInsert($dataSet)
	{
	    $this->db->insert('orders', $dataSet); 
        $insert_id = $this->db->insert_id();
	}

function orderDetailsInsert($data)
	{
	    $this->db->insert('order_details', $data); 
        $insert_id = $this->db->insert_id();
	}



function orderList()
	{
		$this->db->select('*');
		$this->db->from('orders');
		$this->db->order_by("order_date","desc");
		$query = $this->db->get();
		return $query->result();				
	}

function orderDetailsList()
	{
		$this->db->select('*');
		$this->db->from('order_details');
		$this->db->order_by("order_date","desc");
		$query = $this->db->get();
		return $query->result();				
	}

function orderDetails($shopping_id)
	{
		$this->db->select('p.product_name,p.product_image,p.product_icon,od.quantity,c.category_name,b.brand_name');
		$this->db->from('order_details od, products p, brands b, category c');
        $this->db->where("od.product_id=p.product_id and od.shopping_id='$shopping_id' and p.category_id=c.category_id and p.brand_id=b.brand_id");
		$this->db->order_by("order_details_id","desc");
		$query = $this->db->get();
		return $query->result();				
	}

function orderCount($today,$prev_date)
	{
	$this->db->select('O.shopping_id,O.order_id,O.order_date,O.customer_name,O.customer_phone,O.customer_email,O.admin_notify,Od.shopping_id,Od.quantity,p.product_name,p.product_image,p.product_icon,c.category_name,b.brand_name');
		$this->db->from('orders O, order_details Od,products p, brands b, category c');
        $this->db->where("O.order_date between '$prev_date' and '$today' and  O.admin_notify=1 and O.shopping_id=Od.shopping_id and Od.product_id=p.product_id and p.category_id=c.category_id and p.brand_id=b.brand_id");
		$this->db->order_by("O.order_date","desc");
		$query = $this->db->get();
		return $query->result();				
	}

function neworderCount($shopping_id)
	{
	$this->db->select('O.shopping_id,O.order_id,O.order_date,O.customer_name,O.customer_phone,O.customer_email,O.admin_notify,Od.shopping_id,Od.quantity,p.product_name,p.product_image,p.product_icon,c.category_name,b.brand_name');
		$this->db->from('orders O, order_details Od,products p, brands b, category c');
        $this->db->where("Od.shopping_id='$shopping_id' and O.admin_notify=1 and O.shopping_id=Od.shopping_id and Od.product_id=p.product_id and p.category_id=c.category_id and p.brand_id=b.brand_id");
		$this->db->order_by("O.order_date","desc");
		$query = $this->db->get();
		return $query->result();				
	}

function updateOrder($dataSet,$shopping_id)
	{
		$this->db->where('shopping_id', $shopping_id);
		$this->db->update('orders', $dataSet); 	
	}

//-------My Order LIst

function getMyOrders($shopping_id,$language)
	{
$baseurl=base_url();
if($language=='eng'){
$this->db->select('O.order_date,p.product_id,p.product_name,p.product_image,p.product_icon,b.brand_name,Op.price,s.status_name as order_status,sum(Od.quantity) as quantity');
		$this->db->from('orders O, order_details Od,products p, brands b, currency_master cm , order_price_details Op ,status_master s');
        $this->db->where("O.shopping_id='$shopping_id' and  O.shopping_id=Od.shopping_id  and Od.product_id=p.product_id and p.brand_id=b.brand_id and Op.order_details_id=Od.order_details_id and cm.status=0 and cm.trash=0 and Op.currency_id=cm.currency_id and Od.status_id=s.status_id and s.status=0");
		$this->db->order_by("O.order_date","desc");
        $this->db->group_by("p.product_id");
}

if($language=='arb'){

$this->db->select('O.order_date,p.product_id,p.product_name_ar,p.product_image,p.product_icon,b.brand_name_ar,Op.price,s.status_name_ar as order_status,sum(Od.quantity) as quantity');
		$this->db->from('orders O, order_details Od,products p, brands b, currency_master cm , order_price_details Op ,status_master s');
        $this->db->where("O.shopping_id='$shopping_id' and  O.shopping_id=Od.shopping_id  and Od.product_id=p.product_id and p.brand_id=b.brand_id and Op.order_details_id=Od.order_details_id and cm.status=0 and cm.trash=0 and Op.currency_id=cm.currency_id and Od.status_id=s.status_id and s.status=0");
		$this->db->order_by("O.order_date","desc");
        $this->db->group_by("p.product_id");
}

		$query = $this->db->get();
		return $query->result();
      
               
    }
//------------Product Gallery
function productGalleryList()
	{
		$this->db->select('*');
		$this->db->from('product_gallery');
		$this->db->order_by("product_id","desc");
		$query = $this->db->get();
		return $query->result();			
	}


//-----------------Product Banner


function bannerList()
	{
		$this->db->select('p.product_name,p.product_id,b.product_type,b.banner_img,b.banner_id,b.product_id');
		$this->db->from('banner b, products p');
        $this->db->where("b.status='on' and b.product_id=p.product_id");
		$this->db->order_by("b.banner_id","desc");
		$query = $this->db->get();
		return $query->result();				
	}
//-------------------------ORDER REPORT MONTH AND YEAR

function yearandMonth_report($month,$year)
	{
	$this->db->select("YEAR(O.order_date) as year,MONTH(O.order_date) as month,O.shopping_id,O.order_id,O.order_date,O.customer_name,O.customer_phone,O.customer_email,O.admin_notify,Od.shopping_id,Od.quantity,p.product_name,p.product_image,p.product_icon,c.category_name,b.brand_name");
		$this->db->from('orders O, order_details Od,products p, brands b, category c');
        $this->db->where("YEAR(O.order_date)='$year' and  MONTH(O.order_date)='$month' and  O.shopping_id=Od.shopping_id and Od.product_id=p.product_id and p.category_id=c.category_id and p.brand_id=b.brand_id");
		$this->db->order_by("O.order_date","desc");
		$query = $this->db->get();
		return $query->result();				
	}

//-------------------Update Address

function updateAddress($dataSet,$user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->update('tgnuser', $dataSet); 	
	}

//---------Search keyword for web

function get_search_keyword($language,$q)
	{
   
	    $this->db->select("c.clients_id As item_id,c.clients_name_".$language." AS item_name, c.clients_cover_pic AS item_cover,c.clients_logo AS item_icon, c.clients_address_".$language." AS item_address,c.clients_status AS item_status, c.clients_about_".$language." AS item_description, c.phone AS item_phone,c.clients_time AS item_time,c.location_latitude AS item_latitude,c.location_longitude AS item_longitude,l.location_name_".$language." as item_location,c.price_note");
		$this->db->from('clients c, locations l, price p');
        $this->db->where("c.clients_name_".$language." LIKE '%$q%'"); //and c.locate_id=l.locate_id and c.price_note=p.price_id
        $this->db->where("c.locate_id=l.location_id");
		$this->db->order_by("c.clients_name_".$language."","asc");
        $this->db->group_by("c.clients_id");
		$query = $this->db->get();
		return $query->result();				
	}

//--------Get User List

function get_UserList()
	{
		$this->db->select('*');
		$this->db->from('tgnuser');
		$query = $this->db->get();
		return $query->result();			
	}




function cartProductsList()
	{
   
        $this->db->select('pc.*,p.*,b.*,pr.currency_id,pr.price_normal,pr.off_percentage,pr.price_offer,cm.currency_id,cm.currency_name,cm.currency_code,cm.decimal_point,cm.status,cm.trash');
		$this->db->from('order_temp_cart pc,products p, brands b,product_price_details pr,currency_master cm');
        $this->db->where("p.product_id=pc.product_id and p.trash=0 and p.status='on' and p.product_stock >0 and p.brand_id=b.brand_id and  p.trash=0 and  pr.currency_id=cm.currency_id and cm.status=0 and cm.trash=0 and p.product_id=pr.product_id");
		$this->db->order_by("pc.product_id","desc");
        $this->db->group_by("pc.product_id");
		$query = $this->db->get();
		return $query->result();	
	}

	
}







?>