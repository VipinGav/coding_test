<?php
class Task_model extends CI_Model {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

public function getDuplicatItem($array) {  
    $dups = $new_arr = array();
    foreach ($array as $key => $val) {
      if (!isset($new_arr[$val])) {
         $new_arr[$val] = $key;
      } else {
        if (isset($dups[$val])) {
           $dups[$val][] = $key;
        } else {
           $dups[] = $val;
           // $dups[$val] = array($new_arr[$val], $key);
        }
      }
    }
    return $dups;  
 }

public function getDuplicatItem_Table($item,$category) { 
         $this->db->select('*');
         $this->db->from('dl_task_master');
         $this->db->where("category_id", $category);
         $this->db->where_in("task_name", $item);
         $this->db->order_by("task_id", "ASC");
         $query = $this->db->get();
         return $query->result();  
} 

public function addTask($data) {  
          $this->db->insert_batch('dl_task_master',$data);    
 }

 public function updateTask($data) { 
          $this->db->trans_start();
          $this->db->update_batch('dl_task_master', $data,'task_id');
          $this->db->trans_complete();
 }
 public function taskList() { 
         $this->db->select('*');
         $this->db->from('dl_task_master');
         $this->db->order_by("task_name", "ASC");
         $query = $this->db->get();
         return $query->result();  
 } 
public function deleteTask($id) { 
         $exist = $this->check_ExistItemIn_assigedTable($id);
         if(empty($exist)){
          $this->db->where("task_id", $id);
          $this->db->delete('dl_task_master');
           return array('status'=>true,'msg'=>'This Task deleted Successfully');
         }else{
           return array('status'=>false,'msg'=>"You can't delete this Task,Because of this Task already exist in Assigned Task Table, First delete from Assigned Task List and try again..!");
         }
 }
 public function taskList_join() { 
         $this->db->select('cm.*,tm.*');
         $this->db->from('dl_task_master tm, dl_category_master cm');
         $this->db->where("cm.category_id=tm.category_id");
         $this->db->order_by("tm.task_name,cm.category_id", "ASC");
         $query = $this->db->get();
         return $query->result();  
 }

public function check_ExistItemIn_assigedTable($id) { 
         $this->db->select('*');
         $this->db->from('dl_assigned_user_task');
         $this->db->where("task_id", $id);
         $query = $this->db->get();
         return $query->result();  
 } 




}
?>