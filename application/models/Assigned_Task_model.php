<?php
class Assigned_Task_model extends CI_Model {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

public function getDuplicatItem($array) {  
    $dups = $new_arr = array();
    foreach ($array as $key => $val) {
      if (!isset($new_arr[$val])) {
         $new_arr[$val] = $key;
      } else {
        if (isset($dups[$val])) {
           $dups[$val][] = $key;
        } else {
           $dups[] = $val;
           // $dups[$val] = array($new_arr[$val], $key);
        }
      }
    }
    return $dups;  
 }

public function getDuplicatItem_Table($item,$category,$user) { 
         $this->db->select('ut.*,tm.*');
         $this->db->from('dl_task_master tm, dl_assigned_user_task ut');
         $this->db->where("ut.category_id='$category' and ut.user_id='$user' and tm.task_id=ut.task_id");
         $this->db->where_in("ut.task_id", $item);
         $this->db->order_by("ut.task_id", "ASC");
         $query = $this->db->get();
         return $query->result();  
} 

 public function addAssignedTask($data) {  
          $this->db->insert_batch('dl_assigned_user_task',$data);
 }

 public function addAssignedTaskUpdate($data) {  
          $this->db->insert('dl_assigned_user_task',$data);
 }

 

 public function addAssignedTask_list($data) {  
          $this->db->insert_batch('dl_assigned_task_list',$data);   
 }

 public function updateTask($data) { 
          $this->db->trans_start();
          $this->db->update_batch('dl_task_master', $data,'task_id');
          $this->db->trans_complete();
 }
 public function taskList() { 
         $this->db->select('*');
         $this->db->from('dl_task_master');
         $this->db->order_by("task_name", "ASC");
         $query = $this->db->get();
         return $query->result();  
 } 
public function deleteAssignedTask($category_id,$user_id) { 
          $this->db->where("category_id='$category_id' and user_id='$user_id'");
          $this->db->delete('dl_assigned_user_task');
 }

public function deleteAssignedTask_alone($assigned_task_id) { 
          $this->db->where("assigned_task_id='$assigned_task_id'");
          $this->db->delete('dl_assigned_user_task');
 }
 

public function assigned_taskList_group($orderBy, $orderFormat, $start=0, $limit='') {
         $this->db->limit($limit, $start); 
         $this->db->select('at.*,um.*');
         $this->db->from('dl_assigned_user_task at, di_users_list um');
         $this->db->where("um.user_id=at.user_id");
         $this->db->group_by("at.user_id");
         $this->db->order_by($orderBy, $orderFormat);
         $query = $this->db->get();
         return $query->result();  
 }

 public function assigned_taskList_join($id,$category) { 
         $this->db->select('tm.*,at.*');
         $this->db->from('dl_task_master tm,dl_assigned_user_task at');
         $this->db->where("at.user_id='$id' and at.category_id='$category' and  tm.task_id=at.task_id");
         $this->db->order_by("tm.task_name", "ASC");
         $query = $this->db->get();
         return $query->result();  
 }

 public function assigned_taskList_only($id,$category) { 
         $this->db->select('tm.task_id,at.task_id');
         $this->db->from('dl_task_master tm,dl_assigned_user_task at');
         $this->db->where("at.user_id='$id' and at.category_id='$category' and tm.task_id=at.task_id");
         $this->db->order_by("tm.task_name", "ASC");
         $query = $this->db->get();
         return $query->result();  
 }

 

 public function assigned_taskList_category($id) { 
         $this->db->select('cm.*,at.*,um.*');
         $this->db->from('dl_category_master cm, dl_assigned_user_task at, di_users_list um');
         $this->db->where("at.user_id='$id' and cm.category_id=at.category_id and um.user_id=at.user_id");
         $this->db->group_by("at.category_id");
         $this->db->order_by("cm.category_id", "ASC");
         $query = $this->db->get();
         return $query->result();  
 }

 

public function check_ExistItemIn_assigedTable($id) { 
         $this->db->select('*');
         $this->db->from('dl_assigned_user_task');
         $this->db->where("task_id", $id);
         $query = $this->db->get();
         return $query->result();  
 } 
 public function usersList() { 
         $this->db->select('*');
         $this->db->from('di_users_list');
         $this->db->order_by("user_name", "ASC");
         $query = $this->db->get();
         return $query->result();  
 } 

 



}
?>