<?php
class Category_model extends CI_Model {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

public function getDuplicatItem($array) {  
    $dups = $new_arr = array();
    foreach ($array as $key => $val) {
      if (!isset($new_arr[$val])) {
         $new_arr[$val] = $key;
      } else {
        if (isset($dups[$val])) {
           $dups[$val][] = $key;
        } else {
           $dups[] = $val;
           // $dups[$val] = array($new_arr[$val], $key);
        }
      }
    }
    return $dups;  
 }

public function getDuplicatItem_Table($item) { 
         $this->db->select('*');
         $this->db->from('dl_category_master');
         $this->db->where_in("category_name", $item);
         $this->db->order_by("category_id", "ASC");
         $query = $this->db->get();
         return $query->result();  
} 

public function addCategory($data) {  
          $this->db->insert_batch('dl_category_master',$data);    
 }

 public function updateCategory($data) { 
          $this->db->trans_start();
          $this->db->update_batch('dl_category_master', $data,'category_id');
          $this->db->trans_complete();
 }
 public function categoryList() { 
         $this->db->select('*');
         $this->db->from('dl_category_master');
         $this->db->order_by("category_name", "ASC");
         $query = $this->db->get();
         return $query->result();  
} 
public function deleteCategory($id) { 
         $exist = $this->checkItemExistIn_relatedTable($id);
         if(empty($exist)){
           $this->db->where("category_id", $id);
           $this->db->delete('dl_category_master');
             return array('status'=>true,'msg'=>'This Category deleted Successfully'); 
         }else{
             return array('status'=>false,'msg'=>"You can't delete this Category,Because of this category already exist in Task Table, First delete from Task List and try again..!");
         }
 }

public function checkItemExistIn_relatedTable($id) { 
         $this->db->select('*');
         $this->db->from('dl_task_master');
         $this->db->where("category_id", $id);
         $query = $this->db->get();
         return $query->result();  
}




}
?>