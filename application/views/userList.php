<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

 <div class="col-md-10 col-lg-10 inner-body">  	
    <h3 class="main-header"><?=$title?><small> (<span id="itemsListCount">0</span>) Results Found</small>
    </h3>

<!-- inner body -->
<div class="col-md-12 col-lg-12 inner-panel"> 
  
  <div class="inner-content">
  	<div class="inner-header"></div>
      <div id="itemsListTable"></div>
  </div>

</div>

</div>

<script src="<?=base_url()?>js/usersList.js"></script>