 <div class="col-md-2 col-lg-2  left-section">
 	<h4 class="left-header">Menu</h4>
 	
<div class="left-nav">
  <a href="<?=base_url()?>index" class="<?=@$usersList?>"><i class="fa fa-users"></i> Todo Users (Completed) </a>

  <a href="<?=base_url()?>home_message" class="<?=@$introMsg?>"><i class="fa fa-comments"></i> introductory message</a>

  <a href="<?=base_url()?>categoryList" class="<?=@$categoryList?>"><i class="fa fa-archive"></i> Category Master</a>
  <a href="<?=base_url()?>taskList" class="<?=@$taskList?>"><i class="fa fa-tasks"></i>  Task Master</a>
  <a href="<?=base_url()?>assignedTaskList" class="<?=@$assignedTaskList?>"><i class="fa fa-tree"></i> Assign Task </a>
</div>

 </div>