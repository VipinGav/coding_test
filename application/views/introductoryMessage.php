
<div class=" col-md-10 col-lg-10 inner-body">  	
    <h3 class="main-header">Write a simple PHP/JS class which displays an introductory message like
<i>"Hello DL Ideas. Nice to meet you",</i><small> where DL Ideas is an argument value of the method within the class.</small></h3>
    

<!-- inner body -->
<div class="col-md-12 col-lg-12 inner-panel"> 
  <div class="inner-content">
  	<div class="inner-header"></div>

<br clear="all">
<h4>Output:</h4>

<h3><?=$message?></h3>

</div>
</div>
</div>