<div class="form-group row">
           <label class="col-lg-3 col-form-label" for="user_id">Select Users<span class="text-danger">*</span></label>
           <div class="col-lg-7">
           <select class="form-control" disabled name="user_id" id="user_id" required="">
            <option value="">--Select Users--</option>
            <?php if(!empty($users)){ foreach($users as $list) {?>
             <option value="<?=$list->user_id?>" <?=$list->user_id == @$user ? "selected" : '' ;?>><?=$list->user_name?></option>
           <? } } ?>
           </select>
           <span class="col-red font-12 help-block errMsg" id="userErr_1"></span>
           <input type="hidden" name="user_id" id="user_id" value="<?=@$user?>">
           </div>               
</div>
<div class="form-group row">
           <label class="col-lg-3 col-form-label" for="category_id">Select Category<span class="text-danger">*</span></label>
           <div class="col-lg-7">
           <select class="form-control" disabled name="category_id" id="category_id" required="">
           	<option value="">--Select Category--</option>
           	<?php if(!empty($category)){ foreach($category as $list) {?>
           	 <option value="<?=$list->category_id?>" <?=$list->category_id == @$category_id ? "selected" : '' ;?>><?=$list->category_name?></option>
           <? } } ?>
           </select>
           <span class="col-red font-12 help-block errMsg" id="categoryErr_1"></span>
           <input type="hidden" name="category_id" id="category_id" value="<?=@$category_id?>">
           </div>               
</div>
<div>

<div>
  
  <?php if(!empty($task)){ ?>
<div class="border-bottom">
<label class="container" for="selectAll"> 
   <input type="checkbox" class="selectAll" name="task" id="selectAll" value="" /> &nbsp;&nbsp;Select All Task 
        <span id="selectAny" class="p-l-30"></span>
</label>
</div>
<br clear="all">
<? 
foreach($task as $key=>$list){ 
           
           if(in_array($list->task_id, $assigned)){
             $checked ="checked";
           }else{
             $checked ="unchecked";
           }   

  ?>
<style>
/* The container */
.container<?=$list->task_id?> {
  /*display: block;*/
  position: relative;
  padding-right: 10px;
  margin: 0 12px 12px 0;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container<?=$list->task_id?> input {
  position: relative;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark<?=$list->task_id?> {
  position: absolute;
  top: 4;
  left: 4;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container<?=$list->task_id?>:hover input ~ .checkmark<?=$list->task_id?> {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container<?=$list->task_id?> input:checked ~ .checkmark<?=$list->task_id?> {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark<?=$list->task_id?>:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container<?=$list->task_id?> input:checked ~ .checkmark<?=$list->task_id?>:after {
  display: block;
}

/* Style the checkmark/indicator */
.container<?=$list->task_id?> .checkmark<?=$list->task_id?>:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
</style>  
<label class="btn btn-default container<?=$list->task_id?>" for="task_<?=$list->task_id?>"> 
<input type="checkbox" class="selectEach" <?=@$checked?> name="task_id[]" id="task_<?=$list->task_id?>" value="<?=$list->task_id?>" /> &nbsp;&nbsp;<?=$list->task_name?>
<span class="checkmark<?=$list->task_id?>" ></span>
</label>

<? } } else{ ?>
<div class="col-red">Task not found..!</div>
<? } ?>


</div>
                                            

<?php //print_r($assigned)?>

<br clear="all">
<br clear="all">

