<?php defined('BASEPATH') OR exit('') ?>
<div class="table-responsive">
   
  <?php  if(!empty($allItems)) { foreach ($allItems as $list) {  
   $dataArray =  $this->Assigned_Task_model->assigned_taskList_join($list->user_id,$list->category_id); ?>
<div class="col-lg-6 col-md-6 col-sm-12 x_panel">

    <h4><?=$list->category_name?> <button type="button" id="add-model" class="editItem btn btn-default btn-flat btn-addon btn-sm m-b-10 m-l-5 float-right"  data-id="<?=$list->category_id?>" data-user="<?=$list->user_id?>" data-type="edit" ><i class="ti-pencil-alt"></i>Edit</button></h4> 
<table  class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="5%">Sn</th>
                                                    <th width="80%">Assigned Task Name</th> 
                                                    <th width="10%">Action</th>
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
             

                      
               

<?php $x=1; if(!empty($dataArray)){ foreach($dataArray as $key=>$item) { ?>
<tr>
<td class="itemSN"><?=@$x?></td>
<td><?=$item->task_name?></td>
<td> 
 <button type="button" class="btn btn-danger btn-flat  btn-xs m-b-10 m-l-5 delete-confirm" id="<?=$item->assigned_task_id?>" data-user="<?=$item->user_id?>"><i class="ti-trash"></i>Trash</button>

</td>
 </tr>
<? $x++; } } ?>
                                             
                                            </tbody>
                                        </table>
 </div>
<? $sn++; } } ?>   
                                   
                                </div>

