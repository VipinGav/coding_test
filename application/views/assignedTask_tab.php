<?php defined('BASEPATH') OR exit('') ?>
<div class="table-responsive">


     <!-- start accordion -->
<div id="accordion">
<?php if(!empty($allItems)){
   foreach($allItems as $list){
    if($sn==1){
       $active ="false";
       $show   ="";
       $btnLink ="collapsed";
    }else{
      $active ="false";
      $show   ="";
      $btnLink ="collapsed";
      
    }
    ?>

  <div class="card">
    <div class="card-header getDetails bg-light-grey" data-index="<?=$list->user_id?>" id="headingOne_<?=$sn?>" data-toggle="collapse" data-target="#collapseOne_<?=$sn?>" aria-expanded="<?=@$active?>" aria-controls="collapseOne_<?=$sn?>">
      <h5 class="mb-0">
        <button class="btn btn-link <?=@$btnLink?> col-brown" data-toggle="collapse" data-target="#collapseOne_<?=$sn?>" aria-expanded="true" aria-controls="collapseOne_<?=$sn?>" id="<?=$list->user_id?>" data-index="<?=$list->user_id?>" > 
           <?=@$sn?>.
        </button> &nbsp;&nbsp;<?=$list->user_name?></h5>
    </div>

    <div id="collapseOne_<?=$sn?>" class="collapse <?=@$show?>" aria-labelledby="headingOne_<?=$sn?>" data-parent="#accordion">
      <div class="card-body">
        <div id="itemDetailsTable<?=$list->user_id?>"></div>
      </div>
    </div>
  </div>
  
<? $sn++; } } ?>
 
</div>
                    <!-- end of accordion -->


                                       

<!---Pagination div-->
<div class="col-sm-12 text-center">
    <ul class="pagination">
        <?= isset($links) ? $links : "" ?>
    </ul>
</div>
<div><?=@$range?></div>
</div>

<script type="text/javascript">
$('#accordion .collapse').on('show.bs.collapse', function (e) {
    var actives = $('#accordion').find('.in, .collapsing');
    actives.each( function (index, element) {
        $(element).collapse('hide');
    })
})
</script>
