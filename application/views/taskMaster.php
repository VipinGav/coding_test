<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

 <div class="col-md-10 col-lg-10 inner-body" id="taskMaster">  	
    <h3 class="main-header"><?=$title?><small> (<span id="itemsListCount">0</span>) Results Found</small>
    	<button class="btn btn-success float-right" id="add-model" data-id="0" data-type="add"><i class="fa fa-plus"></i> Add New Task</button>
    </h3>

<!-- inner body -->
<div class="col-md-12 col-lg-12 inner-panel"> 
  <div class="inner-content">
  	<button class="btn btn-info float-right" id="refresh"><i class="fa fa-refresh"></i></button>
  	<div class="inner-header"></div>
      <div id="itemsListTable"></div>
  </div>

</div>

<!--model popup Task -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true"  id="addEditItemModal">
                    <div class="modal-dialog modal-lg">
         <form id="taskForm">
                <div class="modal-content">
                <div class="modal-header bg-light-grey">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><img src="<?=base_url()?>images/close-icon.png" width="30%"/></span>
                          </button>
                          <h4 class="modal-title" id="modal-title"></h4>
                          <center><div id="editItemFMsg" class="text-center"></div></center>
                          </div>
                          <div class="modal-body">
          
                              <div id="itemsForm"></div>
  
                          </div>
                          <div class="modal-footer bg-light-grey">

                          <button type="button" data-dismiss="modal" class="btn btn-default btn-flat btn-sm btn-addon btn-sm m-b-10 m-l-5"><i class="ti-close"></i> Close</button>

                          <button type="submit"  class="btn bg-success btn-flat btn-sm btn-addon btn-sm m-b-10 m-l-5 saveButton" id="saveButton"></button>
                   </div>
               </div>
        </form>
                    </div>
                  </div> 


</div>

<script src="<?=base_url()?>js/taskList.js"></script>

              
