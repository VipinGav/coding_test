<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<link rel="stylesheet" href="<?=base_url()?>styles/animate.css">
<link href="<?=base_url()?>styles/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>styles/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>styles/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?=base_url()?>plugins/sweetalert/sweetalert.css">
<script src="<?=base_url()?>ajax/jquery.min.js"></script>

