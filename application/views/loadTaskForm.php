<div class="form-group row">
           <label class="col-lg-3 col-form-label" for="category_id">Select Category<span class="text-danger">*</span></label>
           <div class="col-lg-7">
           <select class="form-control" name="category_id" id="category_id">
           	<option value="">--Select Category--</option>
           	<?php if(!empty($category)){ foreach($category as $list) {?>
           	 <option value="<?=$list->category_id?>" <?=$list->category_id == @$details[0]->category_id ? "selected" : '' ;?>><?=$list->category_name?></option>
           <? } } ?>
           </select>
           <span class="col-red font-12 help-block errMsg" id="categoryErr_1"></span>
           </div>               
</div>
<div id="field_wrapper">
<div class="form-group row">
           <label class="col-lg-3 col-form-label" for="task_name">Task Name<span class="text-danger">*</span></label>
           <div class="col-lg-7">
           <input type="text" class="form-control" id="task_name" name="task_name[]" placeholder="Enter Task Name.." required="" value="<?=@$details[0]->task_name?>">
           <span class="col-red font-12 help-block errMsg" id="taskNameErr_1"></span>
           
           </div>
       <?php if(!@$details[0]->task_id){?>    
           <a href="javascript:void(0);" class="add_button" title="Add field"><img src="<?=base_url()?>images/add-icon.png"/></a> 
       <? } ?>                
</div>
</div>
<input type="hidden" name="task_id" id="task_id" value="<?=@$details[0]->task_id?>">

<br clear="all">
<br clear="all">

