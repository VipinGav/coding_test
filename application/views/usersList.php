<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

   <div class=" col-md-10 col-lg-10 inner-body">  	
    <h3 class="main-header">Completed Todo Users  List - <small> (<span id="itemsListCount">0</span>) Results Found</small></h3>
    

<!-- inner body -->
<div class="col-md-12 col-lg-12 inner-panel"> 
  <div class="inner-content">
  	<div class="inner-header"></div>
<!-- Filtering form -->
<form name="userFilter" id="userFilter">

			<div class="col-md-3 col-lg-3">
				<select id="itemsListUserId" name="userId" class="form-control">
					<option>--User Name--</option>
				</select>
			</div> 

			<div class="col-md-5 col-lg-5">
				<input type="text" placeholder="Enter Title/ Username/ Email/ Phone/ Website" name="title" id="itemsListTitle" class="form-control" autocomplete="off">
			</div> 

			<div class="col-md-3 col-lg-3">
				<button class="btn btn-info btn-sm" id="filter">Filter</button>
				<button class="btn btn-default btn-sm" id="reset">Reset</button>
				<button class="btn btn-default btn-md" id="refresh"><i class="fa fa-refresh"></i></button>
				
			</div> 
</form>

<!-- Users listng Table -->

<br clear="all">
<br clear="all">
<div class="col-md-12 col-lg-12 table-responsive">
    <div id="itemsListTable"></div>
</div>

  </div>

</div>

</div>

</div>

<script src="<?=base_url()?>js/completedUsers.js"></script>