<?php if(!empty($task)){ ?>
<div class="border-bottom">
<label class="container" for="selectAll"> 
   <input type="checkbox" class="selectAll" name="task" id="selectAll" value="" /> &nbsp;&nbsp;Select All Task 
        <span id="selectAny" class="p-l-30"></span>
</label>
</div>
<br clear="all">
<? foreach($task as $key=>$list){ ?>
<style>
/* The container */
.container<?=$list->task_id?> {
  /*display: block;*/
  position: relative;
  padding-right: 10px;
  margin: 0 12px 12px 0;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container<?=$list->task_id?> input {
  position: relative;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark<?=$list->task_id?> {
  position: absolute;
  top: 4;
  left: 4;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container<?=$list->task_id?>:hover input ~ .checkmark<?=$list->task_id?> {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container<?=$list->task_id?> input:checked ~ .checkmark<?=$list->task_id?> {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark<?=$list->task_id?>:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container<?=$list->task_id?> input:checked ~ .checkmark<?=$list->task_id?>:after {
  display: block;
}

/* Style the checkmark/indicator */
.container<?=$list->task_id?> .checkmark<?=$list->task_id?>:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
</style>	
<label class="btn btn-default container<?=$list->task_id?>" for="task_<?=$list->task_id?>"> 
<input type="checkbox" class="selectEach" name="task_id[]" id="task_<?=$list->task_id?>" value="<?=$list->task_id?>" /> &nbsp;&nbsp;<?=$list->task_name?>
<span class="checkmark<?=$list->task_id?>"></span>
</label>

<? } } else{ ?>
<div class="col-red">Task not found..!</div>
<? } ?>