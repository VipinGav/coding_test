<?php
 $i=1; if(!empty($users)){ ?>
<table class="table table-striped table-bordered font-14" >
	<thead>

		<tr>
			<th>Sl.No</th>
			<th>UserId</th>
			<th>Todo Title</th>
			<th>Name</th>
			<th>Username</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Website</th>
			<th>Company</th>
			<th>Address</th>
			
			
			<th>Completed</th>
		</tr>
	</thead>
	<tbody>
	<?php  foreach($users as $key=>$list) { ?>
	<tr>
	    <td><?=$i?></td>	
		<td><?=$list['userId']?></td>
		<td><?=ucfirst($list['title'])?></td>
		<td><?=ucfirst($list['name'])?></td>
		<td><?=$list['username']?></td>
		<td><?=$list['email']?></td>
		<td><?=$list['phone']?></td>
		<td><?=$list['website']?></td>
		<td><b>Name:</b><?=$list['company']['name']?><br/>
		 <small><b>CatchPhrase:</b> <?=$list['company']['catchPhrase']?></small><br/>
		 <small><b>Bs:</b> <?=$list['company']['bs']?></small>
		</td>
		<td>
			<small><b>Street:</b><?=$list['address']['street']?></small><br/>
			<small><b>Suite:</b><?=$list['address']['suite']?></small><br/>
		    <small><b>City:</b><?=$list['address']['city']?></small><br/>
		    <small><b>Zipcode:</b><?=$list['address']['zipcode']?></small><br/>
		    <small><b>Geo Lat:</b><?=$list['address']['geo']['lat']?></small><br/>
		    <small><b>Geo Lng:</b><?=$list['address']['geo']['lng']?></small><br/>	

	    </td>
		
		
		<td align="center"><i class="font-14 fa fa-check-square-o col-green "></i></td>
	</tr>
	<? $i++; } ?>	
	</tbody>
</table>
<? } else {?>
<div class="col-red font-15" align="center">Data not found..!</div>
<? } ?>