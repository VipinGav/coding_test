<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category_Master extends CI_Controller {
           public function __construct()
           {
            parent::__construct();
            // Your own constructor code
            //Model
            $this->load->model('Category_model');                  
           }
            public function categoryList()
             {
             	 $data['categoryList'] ="active";
             	 $data['title']        ="Category List";
                 $this->load->view('header',$data);	
                 $this->load->view('leftNav');	
                 $this->load->view('categoryMaster');
                 $this->load->view('footer');	   
             }
            public function itemList()
             {
              $data['allItems']       = $this->Category_model->categoryList();  
              $data['sn']             = 1;
              $json['count']          = count($data['allItems']);
              $json['itemsListTable'] = $this->load->view('categoryList', $data, TRUE);//----Data Table for table
             $this->output->set_content_type('application/json')->set_output(json_encode($json));
             }
              public function addCategoryForm()
              {  
               $id = $this->input->get('id');
               $json['itemsListTable'] = $this->load->view('loadCategoryForm',$id, TRUE);
               $this->output->set_content_type('application/json')->set_output(json_encode($json));
              }

  //----Save Category
          public function addCategory()
             {
              $this->load->helper('form');  
              $this->load->library('form_validation');
              $this->form_validation->set_error_delimiters('', '');
              $this->form_validation->set_rules('category_name[]', 'Category Name', ['required', 'trim'],['required'=>"required"]);
              $array         = $this->input->post('category_name');
              $duplicateItem = $this->Category_model->getDuplicatItem($array);
              $existTable    = $this->Category_model->getDuplicatItem_Table($array);

            if($this->form_validation->run() !== FALSE && empty($duplicateItem) && empty($existTable)){
              $this->db->trans_start();
                  $dataSet=array();
                  if(count($array)){ 
                        foreach($array as $key=>$val){
                           $dataSet[$key] = array('category_name'=>$val);
                        }
                     $this->Category_model->addCategory($dataSet);
                  }

              $this->db->trans_complete();
              $json = $this->db->trans_status() !== FALSE ? 
                    ['status'=>1, 'msg'=>"Category Details Added Successfully"] 
                    : 
                    ['status'=>0, 'msg'=>"Oops! Unexpected server error! Please contact administrator for help. Sorry for the embarrassment"];
            }else if(!empty($duplicateItem)){
                $errList=null;
                foreach($duplicateItem as $dup){
                  $errList.=$dup."</br>";
                }
                $json['status'] =0;
                $json['msg'] = "Duplicate Item exist,</br>".$errList;    
            }else if(!empty($existTable)){
                $errsList=null;
                foreach($existTable as $dup){
                  $errsList.=$dup->category_name."</br>";
                }
                $json['status'] =0;
                $json['msg'] = "Below listed Item already exist in Database.</br>".$errsList;    
            }

            else{
            //return all error messages
            $json['errMsg'] = $this->form_validation->error_array();//get an array of all errors
            
            $json['msg'] = "One or more required fields are empty or not correctly filled / Entered Field Already Exist";
            $json['status'] = 0;
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($json));
             }
//---Edit Category
            public function editCategoryForm()
              {  
               $id   = $this->input->get('id');
                                         $this->db->where('category_id', $id);
               $data['details']        = $this->Category_model->categoryList();
               $json['itemsListTable'] = $this->load->view('loadCategoryForm',$data, TRUE);
               $this->output->set_content_type('application/json')->set_output(json_encode($json));
              }
 //----Edit Category
          public function editCategory()
             {
              $this->load->helper('form');  
              $this->load->library('form_validation');
              $this->form_validation->set_error_delimiters('', '');
              $this->form_validation->set_rules('category_name[]', 'Category Name', ['required', 'trim'],['required'=>"required"]);
              $array         = $this->input->post('category_name');
              $id            = $this->input->post('category_id');
                               $this->db->where("category_id<>$id");
              $existTable    = $this->Category_model->getDuplicatItem_Table($array);

            if($this->form_validation->run() !== FALSE && empty($existTable)){
              $this->db->trans_start();
                  $dataSet=array();
                  if(count($array)){ 
                        foreach($array as $key=>$val){
                           $dataSet[$key] = array('category_id'=>$id,'category_name'=>$val);
                        }
                     $this->Category_model->updateCategory($dataSet);
                  }

              $this->db->trans_complete();
              $json = $this->db->trans_status() !== FALSE ? 
                    ['status'=>1, 'msg'=>"Category Details Updated Successfully"] 
                    : 
                    ['status'=>0, 'msg'=>"Oops! Unexpected server error! Please contact administrator for help. Sorry for the embarrassment"];
            }else if(!empty($existTable)){
                $errsList=null;
                foreach($existTable as $dup){
                  $errsList.=$dup->category_name."</br>";
                }
                $json['status'] =0;
                $json['msg'] = "Below listed Item already exist in Database.</br>".$errsList;    
            }

            else{
            //return all error messages
            $json['errMsg'] = $this->form_validation->error_array();//get an array of all errors
            
            $json['msg'] = "One or more required fields are empty or not correctly filled / Entered Field Already Exist";
            $json['status'] = 0;
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($json));
             }
//---------Delete Category
             public function deleteCategory()
             {
              $id             = $this->input->get('id');
              $delete         = $this->Category_model->deleteCategory($id);
               if($delete['status']==false){
                   $json['status'] = 0;
                   $json['msg']    = $delete['msg'];
               }else{
                   $json['status'] = 1;
                   $json['msg']    = $delete['msg'];
               }
              
              
              $this->output->set_content_type('application/json')->set_output(json_encode($json));   
             }
             

}