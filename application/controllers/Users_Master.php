<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_Master extends CI_Controller {

    public function usersList()
     {
     	 $data['userList'] ="active";
     	 $data['title']    ="Users List";
         $this->load->view('header',$data);	
         $this->load->view('leftNav');	
         $this->load->view('userList');
         $this->load->view('footer');	   
     }
    public function itemList()
     {
     $users = json_decode(file_get_contents('https://jsonplaceholder.typicode.com/users'), true);

     	 function usersArrays($array){
				 $result=[];
			if(!empty($array)){	 			
				 foreach ($array as $key=>$data)
				 {
					 $result[]=$data;
				 }
			   }
				return $result;
		     }
		     
	 $json['count']    = count($users);	     
     $data['allItems'] = usersArrays($users);
     $json['itemsListTable'] = $this->load->view('itemUserList', $data, TRUE);//----Data Table for table
     $this->output->set_content_type('application/json')->set_output(json_encode($json));
      }


}