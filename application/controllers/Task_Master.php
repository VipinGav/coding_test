<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Task_Master extends CI_Controller {
           public function __construct()
           {
            parent::__construct();
            // Your own constructor code
            //Model
            $this->load->model('Category_model'); 
            $this->load->model('Task_model');              
           }
            public function taskList()
             {
             	 $data['taskList']   ="active";
             	 $data['title']      ="Task List";
                 $this->load->view('header',$data);	
                 $this->load->view('leftNav');	
                 $this->load->view('taskMaster');
                 $this->load->view('footer');	   
             }
            public function itemList()
             {
              $data['allItems']       = $this->Task_model->taskList_join();  
              $data['sn']             = 1;
              $json['count']          = count($data['allItems']);
              $json['itemsListTable'] = $this->load->view('taskList', $data, TRUE);//-Data Table for table
             $this->output->set_content_type('application/json')->set_output(json_encode($json));
             }
            public function addTaskForm()
              {  
               $data['id']             = $this->input->get('id');
               $data['category']       = $this->Category_model->categoryList(); 
               $json['itemsListTable'] = $this->load->view('loadTaskForm',$data, TRUE);
               $this->output->set_content_type('application/json')->set_output(json_encode($json));
              }

  //----Save Task
          public function addTask()
             {
              $this->load->helper('form');  
              $this->load->library('form_validation');
              $this->form_validation->set_error_delimiters('', '');
              $this->form_validation->set_rules('task_name[]', 'Task Name', ['required', 'trim'],['required'=>"required"]);
              $this->form_validation->set_rules('category_id', 'Category ', ['required', 'trim'],['required'=>"required"]);
              $array         = $this->input->post('task_name');
              $category_id   = $this->input->post('category_id');
              $duplicateItem = $this->Task_model->getDuplicatItem($array);
              $existTable    = $this->Task_model->getDuplicatItem_Table($array,$category_id);

            if($this->form_validation->run() !== FALSE && empty($duplicateItem) && empty($existTable)){
              $this->db->trans_start();
                  $dataSet=array();
                  if(count($array)){ 
                        foreach($array as $key=>$val){
                           $dataSet[$key] = array('category_id'=>$category_id,'task_name'=>$val);
                        }
                     $this->Task_model->addTask($dataSet);
                  }

              $this->db->trans_complete();
              $json = $this->db->trans_status() !== FALSE ? 
                    ['status'=>1, 'msg'=>"Task Details Added Successfully"] 
                    : 
                    ['status'=>0, 'msg'=>"Oops! Unexpected server error! Please contact administrator for help. Sorry for the embarrassment"];
            }else if(!empty($duplicateItem)){
                $errList=null;
                foreach($duplicateItem as $dup){
                  $errList.=$dup."</br>";
                }
                $json['status'] =0;
                $json['msg'] = "Duplicate Item exist,</br>".$errList;    
            }else if(!empty($existTable)){
                $errsList=null;
                foreach($existTable as $dup){
                  $errsList.=$dup->task_name."</br>";
                }
                $json['status'] =0;
                $json['msg'] = "Below listed Item already exist in Database.</br>".$errsList;    
            }

            else{
            //return all error messages
            $json['errMsg'] = $this->form_validation->error_array();//get an array of all errors
            
            $json['msg'] = "One or more required fields are empty or not correctly filled / Entered Field Already Exist";
            $json['status'] = 0;
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($json));
             }
//---Get edit Task Form
            public function editTaskForm()
              {  
               $id   = $this->input->get('id');
                                         $this->db->where('task_id', $id);
               $data['details']        = $this->Task_model->taskList();
               $data['category']       = $this->Category_model->categoryList();
               $json['itemsListTable'] = $this->load->view('loadTaskForm',$data, TRUE);
               $this->output->set_content_type('application/json')->set_output(json_encode($json));
              }
 //----Edit Task
          public function editTask()
             {
              $this->load->helper('form');  
              $this->load->library('form_validation');
              $this->form_validation->set_error_delimiters('', '');
              $this->form_validation->set_rules('task_name[]', 'Task Name', ['required', 'trim'],['required'=>"required"]);
              $this->form_validation->set_rules('category_id', 'Category ', ['required', 'trim'],['required'=>"required"]);
              $array         = $this->input->post('task_name');
              $category_id   = $this->input->post('category_id');
              $id            = $this->input->post('task_id');

                               $this->db->where("task_id<>$id");
              $existTable    = $this->Task_model->getDuplicatItem_Table($array,$category_id);

            if($this->form_validation->run() !== FALSE && empty($existTable)){
              $this->db->trans_start();
                  $dataSet=array();
                  if(count($array)){ 
                        foreach($array as $key=>$val){
                           $dataSet[$key] = array('task_id'=>$id,'category_id'=>$category_id,'task_name'=>$val);
                        }
                     $this->Task_model->updateTask($dataSet);
                  }

              $this->db->trans_complete();
              $json = $this->db->trans_status() !== FALSE ? 
                    ['status'=>1, 'msg'=>"Task Details Updated Successfully"] 
                    : 
                    ['status'=>0, 'msg'=>"Oops! Unexpected server error! Please contact administrator for help. Sorry for the embarrassment"];
            }else if(!empty($existTable)){
                $errsList=null;
                foreach($existTable as $dup){
                  $errsList.=$dup->task_name."</br>";
                }
                $json['status'] =0;
                $json['msg'] = "Below listed Item already exist in Database.</br>".$errsList;    
            }

            else{
            //return all error messages
            $json['errMsg'] = $this->form_validation->error_array();//get an array of all errors
            
            $json['msg'] = "One or more required fields are empty or not correctly filled / Entered Field Already Exist";
            $json['status'] = 0;
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($json));
             }
//---------Delete Task
             public function deleteTask()
             {
              $id             = $this->input->get('id');
              $delete         = $this->Task_model->deleteTask($id);
              if($delete['status']==false){
                   $json['status'] = 0;
                   $json['msg']    = $delete['msg'];
               }else{
                   $json['status'] = 1;
                   $json['msg']    = $delete['msg'];
               }
              $this->output->set_content_type('application/json')->set_output(json_encode($json));   
             }
             

}