<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class IntroductoryMessage extends CI_Controller {

public function home_message()
     {
     
         $this->load->library('introductory_message');
         $mymessage        = New introductory_message();
         $data['message']  = $mymessage->introduce(' DL Ideas. ')."\n";
         $data['introMsg'] ="active";
         $this->load->view('header',$data);
         $this->load->view('leftNav');		
         $this->load->view('introductoryMessage');
         $this->load->view('footer');
         
     }

}