<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Assigned_Task_Master extends CI_Controller {
           public function __construct()
           {
            parent::__construct();
            // Your own constructor code
            //Model
            $this->load->model('Category_model'); 
            $this->load->model('Task_model');
            $this->load->model('Assigned_Task_model');              
           }
            public function assignedTaskList()
             {
             	 $data['assignedTaskList']   ="active";
             	 $data['title']              ="Assigned Task List";
                 $this->load->view('header',$data);	
                 $this->load->view('leftNav');	
                 $this->load->view('assignedTask');
                 $this->load->view('footer');	   
             }
            function usersArrays($array=null){
                 $result=[];
                if(!empty($array)){       
                 foreach ($array as $key=>$data)
                 {
                   $result[]=$data;
                 }
                 }
                return $result;
             }

            public function itemTab()
             {

              //set the sort order
              $orderBy = $this->input->get('orderBy', TRUE) ? $this->input->get('orderBy', TRUE) : "um.user_name";
              $orderFormat = $this->input->get('orderFormat', TRUE) ? $this->input->get('orderFormat', TRUE) : "ASC";
              
              //count the total number of items in db
              $this->db->from("dl_assigned_user_task");
              $this->db->group_by("user_id");
              $totalItems =$this->db->count_all_results();

              $this->load->library('pagination');
              $pageNumber = $this->uri->segment(3, 0);
              $limit = $this->input->get('limit', TRUE) ? $this->input->get('limit', TRUE) : 2;
              $start = $pageNumber == 0 ? 0 : ($pageNumber - 1) * $limit;
              
              $config = $this->genlib->setPaginationConfig($totalItems, "Assigned_Task_Master/itemTab", $limit, ['onclick'=>'return itemTab(this.href);']);
              
              $this->pagination->initialize($config);//initialize the library class
              
              //get all items from db
              $data['allItems']       = $this->Assigned_Task_model->assigned_taskList_group($orderBy, $orderFormat, $start, $limit);
              $data['range'] = $totalItems > 0 ? "Showing " . ($start+1) . "-" . ($start + count($data['allItems'])) . " of " . $totalItems : "";
              $data['links'] = $this->pagination->create_links();//page links
              $data['sn'] = $start+1;
              $json['count']          = $totalItems;
              $json['itemsListTable'] = $this->load->view('assignedTask_tab', $data, TRUE);//-Data Table for table
             $this->output->set_content_type('application/json')->set_output(json_encode($json));
             }
            public function getEach_user_task()
             {
              $id                     = $this->input->get('userId');
              $data['allItems']       = $this->Assigned_Task_model->assigned_taskList_category($id);  
              $data['sn']             = 1;
              $json['count']          = count($data['allItems']);
              $json['itemsListTable'] = $this->load->view('assignedTaskList', $data, TRUE);//-Data Table for table
             $this->output->set_content_type('application/json')->set_output(json_encode($json));
             }
            public function addAssignTaskForm()
              {  
               $data['id']             = $this->input->get('id');
               $data['category']       = $this->Category_model->categoryList();
               $data['task']           = $this->Task_model->taskList();
               $data['users']          = $this->Assigned_Task_model->usersList(); 
               $json['itemsListTable'] = $this->load->view('assignedTaskForm',$data, TRUE);
               $this->output->set_content_type('application/json')->set_output(json_encode($json));
              }
            public function getTask()
              {  
               $catgory                = $this->input->get('catgory');
                                         $this->db->where("category_id", $catgory);
               $data['task']           = $this->Task_model->taskList();
               $json['itemsListTable'] = $this->load->view('getTask',$data, TRUE);
               $this->output->set_content_type('application/json')->set_output(json_encode($json));
              }

  //----Save Task
          public function addAssignedTask()
             {
              $this->load->helper('form');  
              $this->load->library('form_validation');
              $this->form_validation->set_error_delimiters('', '');
              $this->form_validation->set_rules('category_id', 'Category ', ['required', 'trim'],['required'=>"required"]);
              $this->form_validation->set_rules('user_id', 'Users ', ['required', 'trim'],['required'=>"required"]);
              $this->form_validation->set_rules('task_id[]', 'Task Name', ['required', 'trim'],['required'=>"required"]);
              $array         = $this->input->post('task_id');
              $user_id       = $this->input->post('user_id');
              $category_id   = $this->input->post('category_id');
              $existTable    = $this->Assigned_Task_model->getDuplicatItem_Table($array,$category_id,$user_id);

            if($this->form_validation->run() !== FALSE && empty($existTable)){
              $this->db->trans_start();
                  
                  //$dataMaster =array('category_id'=>$category_id,'user_id'=>$user_id);
                  //$this->Assigned_Task_model->addAssignedTask($dataMaster);
                  //$assigned_task_id = $this->db->insert_id();

                  $dataSet=array();
                  if(count($array)){ 
                        foreach($array as $key=>$val){
                           $dataSet[$key] = array('task_id'=>$val,'category_id'=>$category_id,'user_id'=>$user_id);
                        }
                     $this->Assigned_Task_model->addAssignedTask($dataSet);
                  }

              $this->db->trans_complete();
              $json = $this->db->trans_status() !== FALSE ? 
                    ['status'=>1, 'msg'=>"Task Successfully assigned for Users"] 
                    : 
                    ['status'=>0, 'msg'=>"Oops! Unexpected server error! Please contact administrator for help. Sorry for the embarrassment"];
            }else if(!empty($existTable)){
                $errsList=null;
                $x=1;
                foreach($existTable as $dup){
                  $errsList.=$x.")".$dup->task_name."</br>";
                  $x++;
                }
                $json['status'] =0;
                $json['msg'] = "Below listed Item already assigned for this user.</br>".$errsList;    
            }

            else{
            //return all error messages
            $json['errMsg'] = $this->form_validation->error_array();//get an array of all errors
            
            $json['msg'] = "One or more required fields are empty or not correctly filled / Entered Field Already Exist";
            $json['status'] = 0;
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($json));
             }
//---Get edit Task Form

          

            public function editAssignTaskForm()
              {  

               
               $data['category_id']    = $this->input->get('id');  
               $data['user']           = $this->input->get('user');               
               $data['category']       = $this->Category_model->categoryList();
               $data['users']          = $this->Assigned_Task_model->usersList(); 
                                         $this->db->where("category_id='{$data['category_id']}'");
               $data['task']           = $this->Task_model->taskList();
               $assigned       = $this->Assigned_Task_model->assigned_taskList_only($data['user'],$data['category_id']); 
                       foreach ($assigned as $key => $value){
                            $singleArray[$key] = $value->task_id;
                       }
               $data['assigned']       = $singleArray;       
               $json['itemsListTable'] = $this->load->view('assignedTaskEditForm',$data, TRUE);
               $this->output->set_content_type('application/json')->set_output(json_encode($json));
              }
 //----Edit Task
          public function editAssignedTask()
             {
              $this->load->helper('form');  
              $this->load->library('form_validation');
              $this->form_validation->set_error_delimiters('', '');
              $this->form_validation->set_rules('category_id', 'Category ', ['required', 'trim'],['required'=>"required"]);
              $this->form_validation->set_rules('user_id', 'Users ', ['required', 'trim'],['required'=>"required"]);
              $this->form_validation->set_rules('task_id[]', 'Task Name', ['required', 'trim'],['required'=>"required"]);
              $array         = $this->input->post('task_id');
              $user_id       = $this->input->post('user_id');
              $category_id   = $this->input->post('category_id');
            
            if($this->form_validation->run() !== FALSE ){
              $this->db->trans_start();
                  $dataSet=array();
                  
                if(count($array)){ 
                        $this->Assigned_Task_model->deleteAssignedTask($category_id,$user_id);
                        foreach($array as $key=>$val){
                           $dataSet['user_id']     = $user_id;
                           $dataSet['category_id'] = $category_id;
                           $dataSet['task_id']     = $val;
                           $this->Assigned_Task_model->addAssignedTaskUpdate($dataSet);
                        }
                     
                     
                  }

              $this->db->trans_complete();
              $json = $this->db->trans_status() !== FALSE ? 
                    ['status'=>1, 'msg'=>"Assigned Task Details Updated Successfully"] 
                    : 
                    ['status'=>0, 'msg'=>"Oops! Unexpected server error! Please contact administrator for help. Sorry for the embarrassment"];
            }

            else{
            //return all error messages
            $json['errMsg'] = $this->form_validation->error_array();//get an array of all errors
            
            $json['msg'] = "One or more required fields are empty or not correctly filled / Entered Field Already Exist";
            $json['status'] = 0;
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($json));
             }
//---------Delete Task
             public function deleteAssignedTask()
             {
              $id             = $this->input->get('id');
              $delete         = $this->Assigned_Task_model->deleteAssignedTask_alone($id);
              $json['status'] = 1;
              $json['msg']    = "Assigned Task Deleted Successfully";
              $this->output->set_content_type('application/json')->set_output(json_encode($json));   
             }
             

}