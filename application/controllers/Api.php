<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api extends CI_Controller {
     public function index()
     {
      $data['usersList']   ="active";
      $data['title']       ="Completed Todo Users List";	
      $this->load->view('header',$data);		
      $this->load->view('leftNav');		
      $this->load->view('usersList');
      $this->load->view('footer');
     }


     public function userslist()
     {
      $todos = json_decode(file_get_contents('https://jsonplaceholder.typicode.com/todos'), true);
      $users = json_decode(file_get_contents('https://jsonplaceholder.typicode.com/users'), true);
      if(!empty($todos) && !empty($users)){
	         function filterArrayByKeyValue($array, $key, $keyValue) //=Get all completed todo users from array 
	           {
	           return array_filter($array, function($value) use ($key, $keyValue) {
	           return $value[$key] == $keyValue; });
	          }
                $array       = filterArrayByKeyValue($todos,'completed', 1);
                $rankCounts  = array_count_values(array_column($array, 'userId'));
			 //====combine both arrays id 
                function combineMultipleArrays($array1,$array2){
							$result=[];
							foreach ($array1 as $key1=>$data1){
							    foreach ($array2 as $key2=>$data2){
							        if($data1['id']==$data2['userId']){
							            $result[]=$data1+$data2;
							            unset($array1[$key1]);
							            unset($array2[$key2]);
							        }
							    }
							}
							if(!empty($array2)){
							    foreach ($array2 as $value){
							        $result[]=$value;
							    }
							}
							if(!empty($array1)){
							    foreach ($array1 as $value){
							        $result[]=$value;
							    }
							}

							return $result;
						}
 
                $result = combineMultipleArrays($users,$array);
                usort( //===Array sorting order by number of todo
					    $result,
					    function($a, $b) use ($rankCounts) {
		     return [$rankCounts[$b['userId']], $b['userId']] >= [$rankCounts[$a['userId']], $a['userId']];
					    }
					);
           $data['users']   = $result;
           $tempArr         = array_unique(array_column($result, 'name')); //===Get unique User id and sorting Ascending order
           $user_id         = array_map('unserialize', array_unique(array_map('serialize', $tempArr)));
           asort($user_id);
           $userID ="<option value=''>--User Name--</option>"; //===Select box for filtering   
                  foreach($user_id as $list){
                     $userID .="<option value='".$list."'>".$list."</option>";
                    }
          $json['user_id']   = $userID;
          $json['count']     = count($result);
          $json['itemsListTable'] = $this->load->view('usersData', $data, TRUE);//----Data Table for listing
           }else{
           
            $json['itemsListTable'] = null;

           }        

          $this->output->set_content_type('application/json')->set_output(json_encode($json));
     

     }


//**** Array Filtering using defferent values
		public function usersFiltering()
		     {

		$userId = $this->input->get('userId');
		$title  = $this->input->get('title');
		$method = $this->input->get('item');
		$todos  = json_decode(file_get_contents('https://jsonplaceholder.typicode.com/todos'), true);
		$users  = json_decode(file_get_contents('https://jsonplaceholder.typicode.com/users'), true);

    //---Multi array filter by multi key value
		      function searchMultifield($arr, $search_list) {
					// Create the result array
					  $result = array();
							    // Iterate over each array element
							foreach ($arr as $key => $value) {
							        // Iterate over each search condition
							      foreach ($search_list as $k => $v) {
							            // If the array element does not meet 
							            // the search condition then continue
							            // to the next element
							           if (!isset($value[$k]) || $value[$k] != $v)
							            {   
							                // Skip two loops
							                continue 2;
							            }
							        }
							        // Append array element's key to the
							        //result array
							        $result[] = $value;
							    }
							  
							    // Return result 
							    return $result;
							}


           //=Get all completed todo users from array 
             function filterArrayByKeyValue($array, $key, $keyValue) 
	           {
	            return array_filter($array, function($value) use ($key, $keyValue) {
	            return $value[$key] == $keyValue; });
	          }
	          $array         = filterArrayByKeyValue($todos,'completed', 1);
	          $rankCounts    = array_count_values(array_column($array, 'userId'));
			 //====combine both arrays id 
							$result=[];
							foreach ($users as $key1=>$data1){
							    foreach ($array as $key2=>$data2){
							        if($data1['id']==$data2['userId']){
							            $result[]=$data1+$data2;
							            unset($users[$key1]);
							            unset($array[$key2]);
							        }
							    }
							}
							if(!empty($array)){
							    foreach ($array as $value){
							        $result[]=$value;
							    }
							}
							if(!empty($users)){
							    foreach ($users as $value){
							        $result[]=$value;
							    }
							}
                usort( //===Array sorting order by number of todo
					    $result,
					    function($a, $b) use ($rankCounts) {
		     return [$rankCounts[$b['userId']], $b['userId']] >= [$rankCounts[$a['userId']], $a['userId']];
					    }
					);

    
				    if($method==1){

				    	

				        $data['users'] = filterArrayByKeyValue($result, 'name', $userId);
				    }
                    else if($method==2){
                        $filter      = strtolower($title);
                        $dataSet1    = filterArrayByKeyValue($result, 'title', $filter);
                        $dataSet2    = filterArrayByKeyValue($result, 'username', $title);
                        $dataSet3    = filterArrayByKeyValue($result, 'email', $title);
                        $dataSet4    = filterArrayByKeyValue($result, 'phone', $title);
                        $dataSet5    = filterArrayByKeyValue($result, 'website', $title);
                        $dataSet     = array_merge_recursive($dataSet1, $dataSet2, $dataSet3, $dataSet4, $dataSet5); 
                        $data['users'] = $dataSet;
                    }
                    else if($method==3){
                     
                      $filter              = strtolower($title);
					  $search_items1       = array('name'=>$userId,'title'=>$filter);
					  $search_items2       = array('name'=>$userId,'username'=>$title);
                      $search_items3       = array('name'=>$userId,'email'=>$title);
                      $search_items4       = array('name'=>$userId,'phone'=>$title);
                      $search_items5       = array('name'=>$userId,'website'=>$title);
                      $search_item1        = searchMultifield($result, $search_items1);
                      $search_item2        = searchMultifield($result, $search_items2);
                      $search_item3        = searchMultifield($result, $search_items3);
                      $search_item4        = searchMultifield($result, $search_items4);
                      $search_item5        = searchMultifield($result, $search_items5);
                      $search_item         = array_merge_recursive($search_item1,$search_item2,$search_item3,$search_item4,$search_item5);		
                      $data['users'] = $search_item;
                    }
                    else{
                      $data['users'] =null;	
                    }
 
               
			  $json['count']     = count($data['users']);         
		      $json['itemsListTable'] = $this->load->view('usersData', $data, TRUE);//----Data Table for listing
             $this->output->set_content_type('application/json')->set_output(json_encode($json));
		     }

        


}
