<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Restapi extends CI_Controller {
   public function index()
	{
	}
	public function userLogin()
	{
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'), true);
		  $email = $data['user_email'];
		  $password =  $data['user_password'];
		  $insertData['push_reg_id'] = $data['push_reg_id'];
		  $insertData['device_platform'] = $data['device_platform'];
		  $result = $this->Restapi_model->user_login($email, $password,$insertData);
		  
		 if($result != false)
		 {
				   $userid=$result[0]['user_id'];
				   $unreadNotifications=$this->Restapi_model->job_notification_count($userid);
				   $cartcount=0;
				   $unreadchat=0;
				   $this->load->library('cart');
				   $myserviceCount=$this->Restapi_model->my_services($userid);
				   $myjobCount=$this->Restapi_model->myJobCount($userid);
				   $myOrderCount=$this->Restapi_model->showMyOrderCount($userid);
				   $myEarnings=$this->Restapi_model->showMyEarnings($userid);  
				   $myRevenue=@$myEarnings[0]['total'];
					 if($myRevenue == "")
					 {
						  $myRevenue = 0;
					 }
					 $myAccountDetails=$this->Restapi_model->accountDetails($userid);
							
					 $catSubcatRes=$this->Restapi_model->catSubcatDet();
					 $catDetArray=$catSubcatRes['catDet'];
					 $subCatDetArray=$catSubcatRes['subcatDet'];
						
					 $catSubcatDetails=array();
							
					$i=0;
					foreach($catDetArray as $catDetails)
					{
					   $catSubcatDetails[$i]['cat_id']=$catDetails->service_cat_id;
					   $catSubcatDetails[$i]['cat_name']=$catDetails->service_cat_title;
					   $catSubcatDetails[$i]['sub_categories']=$subCatDetArray[$i];
					   $i=$i+1;
					}
		  }
		  
		// echo $result;
		  
	      if($result == false)
		  {
			  $response=array(
			   'status' =>  'false',
			   'message' => 'Username or password incorrect'
			   );
		  }
		  else
		  {
			  	   if($result[0]['about_user'] == "")
				   {
					  $result[0]['about_user'] = "I am always available on PS for your service. Please feel free to contact me...";
				   }
				    if($myAccountDetails[0]['about_user'] == "")
				   {
					  $myAccountDetails[0]['about_user'] = "I am always available on PS for your service. Please feel free to contact me...";
				   }
			  
			 	   if($myAccountDetails[0]['profile_pic'] == "")
					  $myAccountDetails[0]['profile_pic']="assets/images/no_image.png";
				   $response=array(
				    'status' =>  'success',
				    'user_id'     => $result[0]['user_id'], 
				    'user_name'   => $result[0]['name'],
				    'user_email'  => $result[0]['email'],
				    'user_phone'  => $result[0]['mobile'],
				    'my_self'     => $result[0]['about_user'],
				    'message' => 'Successfully logined',
					'counts_list' => array(
					   'unread_notifications' => $unreadNotifications, 
					   'cart_count'   => @$cartcount,
					   'unread_chat'  => @$unreadchat,
					   'my_services'  => $myserviceCount,
					   'my_jobs' =>  $myjobCount,
					   'my_orders' => $myOrderCount,
					   'my_revenue' => $myRevenue
					 ),
					   'account_details' => array(
								'name' => $myAccountDetails[0]['name'],
								'profile_pic'  => $myAccountDetails[0]['profile_pic'],
								'email' => $myAccountDetails[0]['email'],
								'phone' => $myAccountDetails[0]['mobile'],
								'followers' => $myAccountDetails[0]['total_followers'],
								'followings' => $myAccountDetails[0]['total_following'],
								'myself' => $myAccountDetails[0]['about_user']
					   ),
					   'service_categories' => $catSubcatDetails,
			   );
			   header('Content-Type: application/json');
		  }
		  echo json_encode($response);
 	}
	public function userRegistration()
	{
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'), true);
		  $insertData['name']=$data['user_name'];
		  $insertData['email'] = $data['user_email'];
		  $insertData['mobile'] = $data['user_phone'];
		  $insertData['password'] =  $data['user_password'];
		  $insertData['push_reg_id'] = $data['push_reg_id'];
		  $insertData['device_platform'] = $data['device_platform']; 
		  $insertData['last_logined_time'] = date("Y-m-d H:i:s");
		  $userid=$this->Restapi_model->addUserRegData($insertData);
		  
		 if($userid != "exist")
		 {
				  $unreadNotifications=$this->Restapi_model->job_notification_count($userid);
				  $cartcount=0;
				  $unreadchat=0;
				  $this->load->library('cart');
				   $myserviceCount=$this->Restapi_model->my_services($userid);
				   $myjobCount=$this->Restapi_model->myJobCount($userid);
				   $myOrderCount=$this->Restapi_model->showMyOrderCount($userid);
				   $myEarnings=$this->Restapi_model->showMyEarnings($userid);  
				   $myRevenue=@$myEarnings[0]['total'];
					 if($myRevenue == "")
					 {
						  $myRevenue = 0;
					 }
					 $myAccountDetails=$this->Restapi_model->accountDetails($userid);
							
					 $catSubcatRes=$this->Restapi_model->catSubcatDet();
					 $catDetArray=$catSubcatRes['catDet'];
					 $subCatDetArray=$catSubcatRes['subcatDet'];
						
					 $catSubcatDetails=array();
							
					$i=0;
					foreach($catDetArray as $catDetails)
					{
					   $catSubcatDetails[$i]['cat_id']=$catDetails->service_cat_id;
					   $catSubcatDetails[$i]['cat_name']=$catDetails->service_cat_title;
					   $catSubcatDetails[$i]['sub_categories']=$subCatDetArray[$i];
					   $i=$i+1;
					}
		  }
		  
		  
		  
		  if($userid == "exist")
		  {
			  $response=array(
			   'status' =>  'false',
			   'message' => 'Email already exist'
			   ); 
			  
		  }
		  else
		  {
			  if($myAccountDetails[0]['about_user'] == "")
			   {
				  $myAccountDetails[0]['about_user'] = "I am always available on PS for your service. Please feel free to contact me...";
			   }
			  if($myAccountDetails[0]['profile_pic'] == "")
			 	$myAccountDetails[0]['profile_pic']="assets/images/no_image.png";
			  $response=array(
			   'status' =>  'success',
			   'user_id'     => $userid, 
			   'user_name'   => $insertData['name'],
			   'user_email'  => $insertData['email'],
			   'user_phone'  => $insertData['mobile'],
			   'my_self'     => "",
			   'message' => 'Your registration is completed successfully !',
			   'counts_list' => array(
				   'unread_notifications' => $unreadNotifications, 
				   'cart_count'   => @$cartcount,
				   'unread_chat'  => @$unreadchat,
				   'my_services'  => $myserviceCount,
				   'my_jobs' =>  $myjobCount,
				   'my_orders' => $myOrderCount,
				   'my_revenue' => $myRevenue
		  		 ),
				   'account_details' => array(
							'name' => $myAccountDetails[0]['name'],
							'profile_pic'  => $myAccountDetails[0]['profile_pic'],
							'email' => $myAccountDetails[0]['email'],
							'phone' => $myAccountDetails[0]['mobile'],
							'followers' => $myAccountDetails[0]['total_followers'],
							'followings' => $myAccountDetails[0]['total_following'],
							'myself' => $myAccountDetails[0]['about_user']
				   ),
				   'service_categories' => $catSubcatDetails
			  ); 
		  }
		  header('Content-Type: application/json');
		  echo json_encode($response);
	}
	public function initData()
	{
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'), true);
		  $userid=$data["user_id"];
		  $unreadNotifications=$this->Restapi_model->job_notification_count($userid);
		  $cartcount=0;
		  $unreadchat=0;
		  $this->load->library('cart');
		  $cartcount=$this->Restapi_model->cartCount($userid);
		  // $unreadchat=0;
		   $myserviceCount=$this->Restapi_model->my_services($userid);
		   $myjobCount=$this->Restapi_model->myJobCount($userid);
		   $myOrderCount=$this->Restapi_model->showMyOrderCount($userid);
		   $myEarnings=$this->Restapi_model->showMyEarnings($userid);  
		   $myRevenue=@$myEarnings[0]['total'];
			 if($myRevenue == "")
			 {
				  $myRevenue = 0;
			 }
			 $myAccountDetails=$this->Restapi_model->accountDetails($userid);
				
			
			 $catSubcatRes=$this->Restapi_model->catSubcatDet();
			 $catDetArray=$catSubcatRes['catDet'];
			 $subCatDetArray=$catSubcatRes['subcatDet'];
				
			 $catSubcatDetails=array();
					
			$i=0;
			foreach($catDetArray as $catDetails)
			{
			   $catSubcatDetails[$i]['cat_id']=$catDetails->service_cat_id;
			   $catSubcatDetails[$i]['cat_name']=$catDetails->service_cat_title;
			   $catSubcatDetails[$i]['sub_categories']=$subCatDetArray[$i];
			   $i=$i+1;
			}
			if($myAccountDetails[0]['about_user'] == "")
			 {
				  $myAccountDetails[0]['about_user'] = "I am always available on PS for your service. Please feel free to contact me...";
			 }
			if($myAccountDetails[0]['profile_pic'] == "")
				$myAccountDetails[0]['profile_pic']="assets/images/no_image.png";
			    $response=array(
		   'status' =>  'success',
		   'counts_list' => array(
				   'unread_notifications' => $unreadNotifications, 
				   'cart_count'   => $cartcount,
				   'unread_chat'  => @$unreadchat,
				   'my_services'  => $myserviceCount,
				   'my_jobs' =>  $myjobCount,
				   'my_orders' => $myOrderCount,
				   'my_revenue' => $myRevenue
		   ),
		   'account_details' => array(
		    		'name' => $myAccountDetails[0]['name'],
					'profile_pic'  => $myAccountDetails[0]['profile_pic'],
					'email' => $myAccountDetails[0]['email'],
					'phone' => $myAccountDetails[0]['mobile'],
					'followers' => $myAccountDetails[0]['total_followers'],
					'followings' => $myAccountDetails[0]['total_following'],
					'myself' => $myAccountDetails[0]['about_user']
		   ),
		   'service_categories' => $catSubcatDetails,
		   );
		   header('Content-Type: application/json');
		   echo json_encode($response);
	}
	public function updateProfile()
	{
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'), true);
		  if($updateData['about_user'] == "")
   	      {
			 $updateData['about_user'] = "I am always available on PS for your service. Please feel free to contact me...";
		  }
		  $updateData['name']=$data['user_name'];
		  $updateData['email'] = $data['user_email'];
		  $updateData['mobile'] = $data['user_phone'];
		  $updateData['about_user']= $data['my_self'];
		  $userid=$this->Restapi_model->updateProfileData($updateData);
		  
		  
		  if($userid != "false")
		  {
			   $response=array(
			   'status' =>  'success',
			   'user_id'     => $userid, 
			   'user_name'   => $updateData['name'],
			   'user_email'  => $updateData['email'],
			   'user_phone'  => $updateData['mobile'],
			   'my_self'     => $updateData['about_user'],
			   'message' => 'Updated your profile successfully !'
			  ); 
		  }
		  else
		  {
			  $response=array(
			   'status' =>  'false',
			   'message' => 'Email not exist'
			   ); 
		  }
		  header('Content-Type: application/json');
		  echo json_encode($response);
	}
	public function trendingServices()
	{
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'), true);
		  $userid=$data['user_id'];
		  $discoverServiceList=$this->Restapi_model->discoverserviceList();  
		
		  if(count($discoverServiceList->result_array()) > 0)
		  {
			  $serviceList=array();
			  $i=0;
			  foreach($discoverServiceList->result_array() as $discServDetails)
			  {
				  $serviceList[$i]["service_id"]=$discServDetails["service_id"];
				  $serviceList[$i]["service_title"]=$discServDetails["service_title"];
				  $serviceList[$i]["service_pic"]=$discServDetails["service_image1"];
				  $serviceList[$i]["user_id"]=$discServDetails["user_id"];
				  $serviceList[$i]["user_name"]= $discServDetails["name"];
				  if($discServDetails["profile_pic"] == "")
					 $discServDetails["profile_pic"]="assets/images/no_image.png";
				  $serviceList[$i]["profile_pic"]=$discServDetails["profile_pic"];
				  $serviceList[$i]["service_date"]=date("M d, Y",strtotime($discServDetails["service_date"]));
				  $serviceList[$i]["basic_price"]=$discServDetails["basic_price"];
				  $serviceList[$i]["likes"]=$discServDetails["total_likes"];
				  $serviceList[$i]["comments"]=$discServDetails["total_comments"];
				  $serviceList[$i]["rating"]=$discServDetails["rating"];
				/*  $serviceList[$i]["is_following"]=$this->Restapi_model->isfollowing($discServDetails["servuserid"],$discServDetails["user_id"]);
				  $serviceList[$i]["is_liked"]=$this->Restapi_model->isliked($discServDetails["service_id"],$discServDetails["user_id"]);*/
				  $serviceList[$i]["is_following"]=$this->Restapi_model->isfollowing($discServDetails["servuserid"],$userid);
				  $serviceList[$i]["is_liked"]=$this->Restapi_model->isliked($discServDetails["service_id"],$userid);
				  if($discServDetails["is_suggested_service"] == 0)
					 $serviceList[$i]["is_suggested_service"]=false;
				  else
					 $serviceList[$i]["is_suggested_service"]=true;
				  
				  $i=$i+1;
			  }
			   $response=array(
			   'status' =>  'success',
			   'service_list' => $serviceList
			  ); 
		  }
		  else
		  {
			   $response=array(
			   'status' =>  'false',
			   'message' => 'No service Exist'
			   ); 
		  }
		  header('Content-Type: application/json');
		  echo json_encode($response);
	}
	public function serviceDetails()
	{
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'), true);
		  $userid=$data['user_id'];
		  $serviceid=$data['service_id'];
		  $serviceDetailList=$this->Restapi_model->serviceDetail($serviceid);  
		
		  if(count($serviceDetailList->result_array()) > 0)
		  {
				  $serviceList=array();
				  $ServDetails=$serviceDetailList->result_array();
				  $serviceList["service_id"]=$ServDetails[0]["service_id"];
				  $serviceList["service_title"]=$ServDetails[0]["service_title"];
				  $serviceList["service_description"]=$ServDetails[0]["service_description"];
				  $serviceList["service_pic"]=$ServDetails[0]["service_image1"];
				 
				  $serviceImages=array();
				  if($ServDetails[0]["service_image1"] != "")
				  {
					 $serviceImages[0]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceImages[0]['image_uploaded']=false;
				  }
				  $serviceImages[0]["img_url"]=$ServDetails[0]["service_image1"];


				  if($ServDetails[0]["service_image2"] != "")
				  {
					 $serviceImages[1]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceImages[1]['image_uploaded']=false;
				  }
				  $serviceImages[1]["img_url"]=$ServDetails[0]["service_image2"];
				  
				  
				  if($ServDetails[0]["service_image3"] != "")
				  {
					 $serviceImages[2]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceImages[2]['image_uploaded']=false;
				  }
				  $serviceImages[2]["img_url"]=$ServDetails[0]["service_image3"];
				  
				  
				  if($ServDetails[0]["service_image4"] != "")
				  {
					 $serviceImages[3]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceImages[3]['image_uploaded']=false;
				  }
				  $serviceImages[3]["img_url"]=$ServDetails[0]["service_image4"];
					  
					  
				  $serviceThumbImages=array();
				  if($ServDetails[0]["service_image1_thumb"] != "")
				  {
					 $serviceThumbImages[0]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceThumbImages[0]['image_uploaded']=false;
				  }
				  $serviceThumbImages[0]["img_url"]=$ServDetails[0]["service_image1_thumb"];
				  if($ServDetails[0]["service_image2_thumb"] != "")
				  {
					 $serviceThumbImages[1]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceThumbImages[1]['image_uploaded']=false;
				  }
				  $serviceThumbImages[1]["img_url"]=$ServDetails[0]["service_image2_thumb"];
				  if($ServDetails[0]["service_image3_thumb"] != "")
				  {
					 $serviceThumbImages[2]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceThumbImages[2]['image_uploaded']=false;
				  }
				  $serviceThumbImages[2]["img_url"]=$ServDetails[0]["service_image3_thumb"];
				  if($ServDetails[0]["service_image4_thumb"] != "")
				  {
					 $serviceThumbImages[3]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceThumbImages[3]['image_uploaded']=false;
				  }
				  $serviceThumbImages[3]["img_url"]=$ServDetails[0]["service_image4_thumb"];
				  
		  				  
				  $serviceList["user_id"]=$ServDetails[0]["user_id"];
				  $serviceList["user_name"]=$ServDetails[0]["name"];
				   if($ServDetails[0]["user_pic"] == "")
					  $ServDetails[0]["user_pic"]="assets/images/no_image.png";
				  $serviceList["user_pic"]=$ServDetails[0]["user_pic"];
				  $serviceList["basic_price"]=$ServDetails[0]["basic_price"];
				  $serviceList["likes"]=$ServDetails[0]["total_likes"];
				  $serviceList["comments"]=$ServDetails[0]["total_comments"];
				  $serviceList["rating"]=$ServDetails[0]["rating"];
				 /* $serviceList["is_following"]=$this->Restapi_model->isfollowing($ServDetails[0]["servuserid"],$ServDetails[0]["user_id"]);
				  $serviceList["is_liked"]=$this->Restapi_model->isliked($ServDetails[0]["service_id"],$ServDetails[0]["user_id"]);*/
				   $serviceList["is_following"]=$this->Restapi_model->isfollowing($ServDetails[0]["servuserid"],$userid);
				  $serviceList["is_liked"]=$this->Restapi_model->isliked($ServDetails[0]["service_id"],$userid);
				  if($ServDetails[0]["is_suggested_service"] == 0)
					  $serviceList["is_suggested_service"]=false;
				  else
					  $serviceList["is_suggested_service"]=true;
				 
				 
				  $extServDetails=$this->Restapi_model->extra_ServiceDetails($ServDetails[0]["service_id"]);
				
				 
				  $otherserviceList=array();
				  $otherServDetails=$this->Restapi_model->otherServices($serviceid,$serviceList["user_id"]);
				//  exit;
				  $i=0;
				  foreach($otherServDetails->result() as $otherServDet)
				  {
				      $otherserviceList[$i]["servce_id"]=$otherServDet->service_id;
					  $otherserviceList[$i]["servce_title"]=$otherServDet->service_title;
					  $otherserviceList[$i]["servce_pic"]=$otherServDet->service_image1;
					  $otherserviceList[$i]["basic_price"]=$otherServDet->basic_price;
					  $otherserviceList[$i]["rating"]=$otherServDet->rating;
				      $i=$i+1;
				  }
				 
				   $response=array(
				   'status' =>  'success',
				   'service_list' => $serviceList,
				   'service_images' => $serviceImages,
				   'service_thumb_images' => $serviceThumbImages,
				   'extras_list' => $extServDetails,
				   'more_services' => $otherserviceList
				  ); 
		  }
		  else
		  {
				   $response=array(
				   'status' =>  'false',
				   'message' => 'No service detail to show'
				   ); 
		  }
		  header('Content-Type: application/json');
		  echo json_encode($response);
	}
	public function myServices()
	{
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'), true);
		  $userid=$data['user_id'];
		  $myServActiveList=$this->Restapi_model->my_services_act($userid); 
		  $actServiceList=array();
		  $InactServiceList=array();
		  
		  if(count($myServActiveList) > 0)
		  {
			//  $actServiceList=array();
			  $i=0;
			  foreach($myServActiveList as $myServDetails)
			  {
				  $actServiceList[$i]["service_id"]=$myServDetails["service_id"];
				  $actServiceList[$i]["service_title"]=$myServDetails["service_title"];
				  $actServiceList[$i]["service_pic"]=$myServDetails["service_image1"];
				  $actServiceList[$i]["service_date"]=date("M d,Y",strtotime($myServDetails["post_date"]));
				  $actServiceList[$i]["user_id"]=$myServDetails["user_id"];
				  $actServiceList[$i]["user_name"]= $myServDetails["name"];
				  if($myServDetails["user_pic"] == "")
					  $myServDetails["user_pic"]="assets/images/no_image.png";
				  $actServiceList[$i]["user_pic"]=$myServDetails["user_pic"];
				  $actServiceList[$i]["basic_price"]=$myServDetails["basic_price"];
				  $actServiceList[$i]["likes"]=$myServDetails["total_likes"];
				  $actServiceList[$i]["comments"]=$myServDetails["total_comments"];
			      $actServiceList[$i]["views"]=0;
				  $actServiceList[$i]["rating"]=$myServDetails["rating"];
				/*  $actServiceList[$i]["is_following"]=$this->Restapi_model->isfollowing($myServDetails["servuserid"],$myServDetails["user_id"]);
				  $actServiceList[$i]["is_liked"]=$this->Restapi_model->isliked($myServDetails["service_id"],$myServDetails["user_id"]);*/
				  $actServiceList[$i]["is_following"]=$this->Restapi_model->isfollowing($myServDetails["servuserid"],$userid);
				  $actServiceList[$i]["is_liked"]=$this->Restapi_model->isliked($myServDetails["service_id"],$userid);
				  $i=$i+1;
			  }
		  }
		  
		  $myServInActiveList=$this->Restapi_model->my_services_inc($userid); 
		  if(count($myServInActiveList) > 0)
		  {
			 // $InactServiceList=array();
			  $i=0;
			  foreach($myServInActiveList as $myServDetails)
			  {
				  $InactServiceList[$i]["service_id"]=$myServDetails["service_id"];
				  $InactServiceList[$i]["service_title"]=$myServDetails["service_title"];
				  $InactServiceList[$i]["service_pic"]=$myServDetails["service_image1"];
				  $InactServiceList[$i]["service_date"]=date("M d,Y",strtotime($myServDetails["post_date"]));
				  $InactServiceList[$i]["user_id"]=$myServDetails["user_id"];
				  $InactServiceList[$i]["user_name"]= $myServDetails["name"];
				  if($myServDetails["user_pic"] == "")
					  $myServDetails["user_pic"]="assets/images/no_image.png";
				  $InactServiceList[$i]["user_pic"]=$myServDetails["user_pic"];
				  $InactServiceList[$i]["basic_price"]=$myServDetails["basic_price"];
				  $InactServiceList[$i]["likes"]=$myServDetails["total_likes"];
				  $InactServiceList[$i]["comments"]=$myServDetails["total_comments"];
			      $InactServiceList[$i]["views"]=0;
				  $InactServiceList[$i]["rating"]=$myServDetails["rating"];
				/*  $InactServiceList[$i]["is_following"]=$this->Restapi_model->isfollowing($myServDetails["servuserid"],$myServDetails["user_id"]);
				  $InactServiceList[$i]["is_liked"]=$this->Restapi_model->isliked($myServDetails["service_id"],$myServDetails["user_id"]);*/
				  $InactServiceList[$i]["is_following"]=$this->Restapi_model->isfollowing($myServDetails["servuserid"],$userid);
				  $InactServiceList[$i]["is_liked"]=$this->Restapi_model->isliked($myServDetails["service_id"],$userid);

				  $i=$i+1;
			  }
		  }
		  
		
		  
		/*  if(count($myServActiveList) == 0 && count($myServInActiveList) == 0)
		  {
			   $response=array(
			   'status' =>  'false',
			   'message' => "No services to show"
			  ); 
		  }
		  else
		  {*/
			 /* if(count($actServiceList) == 0)
		        $actServiceList=array();
			  if(count($InactServiceList) == 0)
		        $InactServiceList=array();*/
				
				
		  	  $response=array(
			   'status' =>  'success',
			   'active_services_list' => $actServiceList,
			   'inactive_services_list' => $InactServiceList
			  ); 
		 /* }*/
		  
		  header('Content-Type: application/json');
		  echo json_encode($response);
	}
	public function myJobs()
	{
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'), true);
		  $userid=$data['user_id'];
		  
		  // Pending Job Details - Only Job Requested coming
			$pendjobDetInfoDet=$this->Restapi_model->PendingMyjobServDet($userid);
			$pendjobDetailInfo=array();
			$pendjobExtraIds=array();
			if(count($pendjobDetInfoDet->result()) > 0)
			{
				$i=0;
				foreach($pendjobDetInfoDet->result_array() as $jobDet)
				{
					$extraServDet=array();
					if($jobDet["id"] == "")
					    $jobDet["id"]="";
				
					$jobDet['service_date']=date("M d, Y",strtotime($jobDet['service_date']));
					$jobDet['requested_date']=date("M d, Y",strtotime($jobDet['requested_date']));
					$pendjobDetailInfo[$i]=$jobDet;
					$jobServiceIds=unserialize($jobDet["id"]);
					if(count($jobServiceIds) > 0)
					{
						for($j=1;$j <= count($jobServiceIds); $j++)
						{
							$extServId=$jobServiceIds[$j];
							if($extServId[0] != "")
							{
							    $extRes=$this->Restapi_model->extraServiceDetails($extServId[0]);
								if(count($extRes) > 0)
							      $extraServDet[]=$extRes;
							}
						}
					}
					$pendjobDetailInfo[$i]['extras_list']=$extraServDet;
					$i=$i+1;
				}
			}
			else
			{
				$pendjobDetailInfo=[];
			}
		
		
		
		    // Active Job Details - Job started
			$actjobDetInfoDet=$this->Restapi_model->ActiveMyjobServDet($userid);
			$actjobDetailInfo=array();
			$actjobExtraIds=array();
			if(count($actjobDetInfoDet->result_array()) > 0)
			{
				$i=0;
				foreach($actjobDetInfoDet->result_array() as $jobDet)
				{
					$extraServDet=array();
					if($jobDet["id"] == "")
					    $jobDet["id"]="";
					$jobDet['service_date']=date("M d, Y",strtotime($jobDet['service_date']));
					$jobDet['requested_date']=date("M d, Y",strtotime($jobDet['requested_date']));
					$actjobDetailInfo[$i]=$jobDet;
					$jobServiceIds=unserialize($jobDet["id"]);
					if(count($jobServiceIds) > 0)
					{
						for($j=1;$j <= count($jobServiceIds); $j++)
						{
							$extServId=$jobServiceIds[$j];
							if($extServId[0] != "")
							{
							    $extRes=$this->Restapi_model->extraServiceDetails($extServId[0]);
								if(count($extRes) > 0)
							      $extraServDet[]=$extRes;
							}
						}
					}
					$actjobDetailInfo[$i]['extras_list']=$extraServDet;
					$i=$i+1;
				}
			}
			else
			{
				$actjobDetailInfo=[];
			}
		
		    // Completed Job Details - Only Job Completed jobs coming
			$compjobDetInfoDet=$this->Restapi_model->CompletedMyjobServDet($userid);
			$compjobDetailInfo=array();
			$compjobExtraIds=array();
			
			
			
			if(count($compjobDetInfoDet->result_array()) > 0)
			{
				$i=0;
				foreach($compjobDetInfoDet->result_array() as $jobDet)
				{
					$extraServDet=array();
				    if($jobDet["id"] == "")
					    $jobDet["id"]="";
					$jobDet['service_date']=date("M d, Y",strtotime($jobDet['service_date']));
					$jobDet['requested_date']=date("M d, Y",strtotime($jobDet['requested_date']));
					$compjobDetailInfo[]=$jobDet;
					$jobServiceIds=unserialize($jobDet["id"]);
				//	$extraServDet=array();
					for($j=1;$j <= count($jobServiceIds); $j++)
					{
						$extServId=$jobServiceIds[$j];
						if($extServId[0] != "")
						{
						  	$extRes=$this->Restapi_model->extraServiceDetails($extServId[0]);
							if(count($extRes) > 0)
							   $extraServDet[]=$extRes;
						}
					}
					
					$compjobDetailInfo[$i]['extras_list']=$extraServDet;
					$i=$i+1;
				}
			}
		
		  	 $response=array(
				   'status' =>  'success',
				   'job_requests_list' => $pendjobDetailInfo,
				   'active_jobs_list' => $actjobDetailInfo,
				   'completed_jobs_list' => $compjobDetailInfo
		     ); 
			 header('Content-Type: application/json');
		 	 echo json_encode($response);
		
	}
	public function profileDetails()
	{
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'),true);
		  $userid=$data['user_id'];
		  $sender_id=$data['loginuser_id'];
		  $receiver_id=$userid;
		  
		  $userDetails  = $this->Restapi_model->get_profile($userid);
		  $myServicesList=array();
		  $myServices  = $this->Restapi_model->myServices($userid);
		  if(count($myServices) > 0)
		  {
			  $i=0;
			  foreach($myServices as $myServDetails)
			  {
				  $myServicesList[$i]["service_id"]=$myServDetails["service_id"];
				  $myServicesList[$i]["service_title"]=$myServDetails["service_title"];
				  $myServicesList[$i]["service_pic"]=$myServDetails["service_image1"];
				  $myServicesList[$i]["basic_price"]=$myServDetails["basic_price"];
				  $myServicesList[$i]["rating"]=$myServDetails["rating"];
				  $i=$i+1;
			  }
		  }
		  
		 
		  $channelId=$this->Restapi_model->getchannelId($sender_id,$receiver_id);
		  if($channelId != 0)
		  {
			 $channelStat=true;
		  }
		  else
		  {
			  $channelId=0;  
			  $channelStat=false;
		  }
		  
		 
		
		  if($userDetails[0]['user_pic'] == "")
		  {
			$userDetails[0]['user_pic'] = "assets/images/no_image.png";
		  }
		 if($userDetails[0]['about'] == "")
		  {
			$userDetails[0]['about'] = "I am always available on PS for your service. Please feel free to contact me...";
		  }
		 
		  
		  if(count($userDetails) > 0)
		  {
		 		 $response=array(
				   'status' =>  'success',
				   'is_following' => $this->Restapi_model->isfollowing($userid,$sender_id), //is_followed($userid),
				   'user_details' => $userDetails[0],
				   'more_services' => $myServicesList,
				   'channel_id' => $channelId,
				   'channel_stat' => $channelStat
				  ); 
		  }
		  else
		  {
			 	 $response=array(
				   'status' =>  'false',
				   'message' => "User not exist"
				  ); 
		  }
		  header('Content-Type: application/json');
		  echo json_encode($response);
	}
	public function userFeeds()
	{
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'),true);
		  $userid=$data['user_id'];
		  $suggestedProfile=array();
		  $suggestedProfile  = $this->Restapi_model->suggestedprofile($userid);
		  $suggestedProfileList=array();
		  $i=0;
		  foreach($suggestedProfile as $sugProfile)
		  {	 
				  $suggestedProfileList[$i]['user_id']=$sugProfile["user_id"];
				  $suggestedProfileList[$i]['user_name']=$sugProfile["user_name"];
				  if($sugProfile["user_pic"] == "")
					$sugProfile["user_pic"]="assets/images/no_image.png";
				  $suggestedProfileList[$i]['user_pic']=$sugProfile["user_pic"];
				  $i=$i+1;
		  }
		  
		  $feedsList=array();
		  $feedsList  = $this->Restapi_model->feedsList();
		  $feedsDetailsList=array();
		  $i=0;
		  foreach($feedsList as $feedsDetails)
		  {
			     if($feedsDetails["is_suggested_service"] == 0)
				     $feedsDetailsList[$i]["is_suggested_service"]=false;
			     else
				     $feedsDetailsList[$i]["is_suggested_service"]=true;
					  
					 
					 $feedsDetailsList[$i]["service_id"]=$feedsDetails["service_id"];
					 $feedsDetailsList[$i]["service_title"]=$feedsDetails["service_title"];
					 $feedsDetailsList[$i]["service_pic"]=$feedsDetails["service_pic"];
					 $feedsDetailsList[$i]["user_id"]=$feedsDetails["user_id"];
					 if($feedsDetails["user_pic"] == "")
					   $feedsDetails["user_pic"]="assets/images/no_image.png";
					 $feedsDetailsList[$i]["user_pic"]=$feedsDetails["user_pic"];
					 $feedsDetailsList[$i]["basic_price"]=$feedsDetails["basic_price"];
					 $feedsDetailsList[$i]["likes"]=$feedsDetails["likes"];
					 $feedsDetailsList[$i]["comments"]=$feedsDetails["comments"];
					 $feedsDetailsList[$i]["rating"]=$feedsDetails["rating"];
					 $feedsDetailsList[$i]["user_name"]=$feedsDetails["name"];
					 $feedsDetailsList[$i]["service_date"]=date("M d, Y",strtotime($feedsDetails["service_date"]));
					 $feedsDetailsList[$i]["is_following"]=$this->Restapi_model->isfollowing($feedsDetails["servuserid"],$userid);
					 $feedsDetailsList[$i]["is_liked"]=$this->Restapi_model->isliked($feedsDetails["service_id"],$userid);
				     $i=$i+1;
		  }
	  
		  if((count($suggestedProfile) > 0) ||  (count($feedsDetailsList) > 0))
		  {
			     $response=array(
				   'status' =>  'success',
				   'suggested_profiles' =>  $suggestedProfileList,
				   'feeds_list' => $feedsDetailsList
				  );   
		  }
		  else
		  {
				  $response=array(
					   'status' =>  'false',
					   'message' => "No data to show"
				  ); 
		  }
		  header("Content-Type: application/json");
		  echo json_encode($response);
	}
	public function suggestedProfiles()
	{
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'),true);
		  $userid=$data['user_id'];
		  $suggestedProfile=array();
		  $suggestedProfile  = $this->Restapi_model->suggestedprofile($userid);
		  $suggestedProfileList=array();
		 /* if((count($suggestedProfile) > 0))
		  {*/
			      $i=0;
			  	  foreach($suggestedProfile as $sugProfile)
				  {	 
				  	  $suggestedProfileList[$i]['user_id']=$sugProfile["user_id"];
					  $suggestedProfileList[$i]['user_name']=$sugProfile["user_name"];
					  if($sugProfile["user_pic"] == "")
					  	$sugProfile["user_pic"]="assets/images/no_image.png";
					  $suggestedProfileList[$i]['user_pic']=$sugProfile["user_pic"];
				      $suggestedProfileList[$i]['service_count']=$this->Restapi_model->my_services($sugProfile["user_id"]);
					  $i=$i+1;
				  } 
			     $response=array(
					  'status' =>  'success',
					  'suggested_profiles' => $suggestedProfileList,
				  );   
		 /* }
		  else
		  {
				  $response=array(
					   'status' =>  'false',
					   'message' => "No data to show"
				  ); 
		  }*/
		  header("Content-Type: application/json");
		  echo json_encode($response);
	}
	public function likesList()
	{
		  $likesList=array();
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'),true);
		//  $userid=$data['user_id'];
		  $serviceid=$data['service_id'];
		
		  $likesList=$this->Restapi_model->likesListDet($serviceid);
		  $likesListDetails=array();
		  
		  $i=0;
		  
		  foreach($likesList as $likesListDet)
		  {	 
			  $likesListDetails[$i]['user_id']=$likesListDet["user_id"];
			  $likesListDetails[$i]['user_name']=$likesListDet["user_name"];
			  if($likesListDet["user_pic"] == "")
				$likesListDet["user_pic"]="assets/images/no_image.png";
			  $likesListDetails[$i]['user_pic']=$likesListDet["user_pic"];
			  $likesListDetails[$i]['is_following']=$this->Restapi_model->is_followed($likesListDet["user_id"]);
			  $likesListDetails[$i]['date']=date("M d,Y",strtotime($likesListDet["date"]));
			  $i=$i+1;
		  } 
		 $response=array(
			  'status' =>  'success',
			  'likes_list' => $likesListDetails
		  ); 
		  header("Content-Type: application/json");
		  echo json_encode($response);  
	}
	
	
	public function commentList()
	{
		  $commentList=array();
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'),true);
		//  $userid=$data['user_id'];
		  $serviceid=$data['service_id'];
		
		  $commentList=$this->Restapi_model->commentListDet($serviceid);
		  $commentListDetails=array();
		  
		  $i=0;
		  
		  foreach($commentList as $commentListDet)
		  {	 
			  $commentListDetails[$i]['user_id']=$commentListDet["user_id"];
			  $commentListDetails[$i]['user_name']=$commentListDet["user_name"];
			  if($commentListDet["user_pic"] == "")
				$commentListDet["user_pic"]="assets/images/no_image.png";
			  $commentListDetails[$i]['user_pic']=$commentListDet["user_pic"];
			  $commentListDetails[$i]['comment']=$commentListDet["comment"];
			  $commentListDetails[$i]['is_following']=$this->Restapi_model->is_followed($commentListDet["user_id"]);
			  $commentListDetails[$i]['date']=date("M d,Y",strtotime($commentListDet["date"]));
			  $i=$i+1;
		  } 
		 $response=array(
			  'status' =>  'success',
			  'comment_list' => $commentListDetails
		  ); 
		  header("Content-Type: application/json");
		  echo json_encode($response);  
	}
	public function userFollowUnfollow()
	{
		   $this->load->model('Restapi_model');
		   $mydata = json_decode(file_get_contents('php://input'),true);
		   $data['following_id']  =  $mydata["user_id"];  // logined user_id
		   $data['follower_id']   =  $mydata["follow_user_id"];  // service posted persons id
		   
		   $following_id=$mydata["user_id"] ;
		   $follower_id=$mydata["follow_user_id"];
		   
		   $following =$this->Restapi_model->getFollowCount($following_id,$follower_id);
	 	
		   if(count($following) > 0)   // already followed , so unfollow
		   { 
				 $follow_id = $following[0]['follow_id'];
				 $this->Restapi_model->rmvFollower($follow_id);
				 $result1=$this->Restapi_model->getFollowerDetails($follower_id);
			     $total_followers = $result1[0]['total_followers'];
			     $datacnt1['total_followers'] = $total_followers-1;
			     $this->Restapi_model->updateFollowersCount($follower_id,$datacnt1);
			     $result2 = $this->Restapi_model->getFollowingDetails($following_id);
			     $total_following = $result2[0]['total_following'];
			     $datacnt['total_following'] = $total_following-1;
			     $this->Restapi_model->updateFollowingCount($following_id,$datacnt);  
		   }
		   else   // follow
		   {
			   $this->Restapi_model->saveFollowDetails($data);
			   $result1=$this->Restapi_model->getFollowerDetails($follower_id);
			   $total_followers = $result1[0]['total_followers'];
			   $datacnt1['total_followers'] = $total_followers+1;
			   $this->Restapi_model->updateFollowersCount($follower_id,$datacnt1);
			 
			   $result2 = $this->Restapi_model->getFollowingDetails($following_id);
			   $total_following = $result2[0]['total_following'];
			   $datacnt2['total_following'] = $total_following+1;
			   $this->Restapi_model->updateFollowingCount($following_id,$datacnt2);  
		   }
		 
		   
		  $response=array(
			  'status' =>  'success'
		   );
		   header("Content-Type: application/json");
		   echo json_encode($response);  
	 }
	 
	 
	 public function getServiceList()
	 {
		$this->load->model("Restapi_model");
		$data = json_decode(file_get_contents('php://input'),true);
		$userid=$data['user_id'];  // not necessary
		$categoryId=$data['cat_id'];
		$SubCategoryId=$data['sub_cat_id'];
		$discoverServiceList=$this->Restapi_model->searchDiscServBycatSubcat($categoryId,$SubCategoryId);
		$serviceList=array();
		if(count($discoverServiceList->result_array()) > 0)
		{
			  $i=0;
			  foreach($discoverServiceList->result_array() as $discServDetails)
			  {
				  $serviceList[$i]["service_id"]=$discServDetails["service_id"];
				  $serviceList[$i]["service_title"]=$discServDetails["service_title"];
				  $serviceList[$i]["service_pic"]=$discServDetails["service_image1"];
				  $serviceList[$i]["user_id"]=$discServDetails["user_id"];
				  $serviceList[$i]["user_name"]= $discServDetails["name"];
				  if($discServDetails["profile_pic"] == "")
					 $discServDetails["profile_pic"]="assets/images/no_image.png";
				  $serviceList[$i]["profile_pic"]=$discServDetails["profile_pic"];
				  $serviceList[$i]["service_date"]=date("M d, Y",strtotime($discServDetails["service_date"]));
				  $serviceList[$i]["basic_price"]=$discServDetails["basic_price"];
				  $serviceList[$i]["likes"]=$discServDetails["total_likes"];
				  $serviceList[$i]["comments"]=$discServDetails["total_comments"];
				  $serviceList[$i]["rating"]=$discServDetails["rating"];
				/*  $serviceList[$i]["is_following"]=$this->Restapi_model->isfollowing($discServDetails["servuserid"],$discServDetails["user_id"]);
				  $serviceList[$i]["is_liked"]=$this->Restapi_model->isliked($discServDetails["service_id"],$discServDetails["user_id"]);*/
				  $serviceList[$i]["is_following"]=$this->Restapi_model->isfollowing($discServDetails["servuserid"],$userid);
				  $serviceList[$i]["is_liked"]=$this->Restapi_model->isliked($discServDetails["service_id"],$userid);
				  if($discServDetails["is_suggested_service"] == 0)
					 $serviceList[$i]["is_suggested_service"]=false;
				  else
					 $serviceList[$i]["is_suggested_service"]=true;
				  
				  $i=$i+1;
			  }
		  }
		   $response=array(
			   'status' =>  'success',
			   'service_list' => $serviceList
			  ); 
		 /* else
		  {
			   $response=array(
			   'status' =>  'false',
			   'message' => 'No service Exist'
			   ); 
		  }*/
		  header('Content-Type: application/json');
		  echo json_encode($response);
		
	}
	public function followersList()
	{
		$this->load->model("Restapi_model");
		$data = json_decode(file_get_contents('php://input'),true);
		$followerid=$data["user_id"];
		$followersList=array();
		$followersDet=$this->Restapi_model->getFollowersList($followerid);
		$i=0;
		if(count($followersDet) > 0)
		{
				foreach($followersDet as $followRow)
				{
					   $followersList[$i]['user_id']=$followRow['user_id'];
					   $followersList[$i]['user_name']=$followRow['user_name'];
					   if($followRow['user_pic'] == "")
						  $followRow['user_pic']="assets/images/no_image.png";
				  	   $followersList[$i]['user_pic']=$followRow['user_pic']; 
					   $followersList[$i]['is_following']=$this->Restapi_model->equallyFollowed($followRow['user_id'],$followerid);
					   $i=$i+1;
				}
		}
		$response=array(
		   'status' =>  'success',
		   'followers_list' => $followersList
		); 
		header('Content-Type: application/json');
		echo json_encode($response);
	}
	
	
	public function followingList()
	{
		$this->load->model("Restapi_model");
		$data = json_decode(file_get_contents('php://input'),true);
		$followingid=$data["user_id"];
		$followingList=array();
		$followingDet=$this->Restapi_model->getFollowingList($followingid);
		$i=0;
		if(count($followingDet) > 0)
		{
			foreach($followingDet as $followRow)
			{
				   $followingList[$i]['user_id']=$followRow['user_id'];
				   $followingList[$i]['user_name']=$followRow['user_name'];
				   if($followRow['user_pic'] == "")
					  $followRow['user_pic']="assets/images/no_image.png";
				   $followingList[$i]['user_pic']=$followRow['user_pic']; 
				   $followingList[$i]['is_following']=$this->Restapi_model->equallyFollowed($followerid,$followRow['user_id']);
				   $i=$i+1;
			}
		}
		$response=array(
		   'status' =>  'success',
		   'following_list' => $followingList
		); 
		header('Content-Type: application/json');
		echo json_encode($response);
	}
	public function addToCart()
	{
			$this->load->model("Restapi_model");
			$this->load->library("cart");
			$data = json_decode(file_get_contents('php://input'),true);
			//print_r($data);
			
			
			$userid=$data["user_id"];
			$serviceid=$data["service_id"];
			$extraserviceids=$data["extras_list"];
		
			
			
			$serviceDetList=$this->Restapi_model->showServiceDetails($serviceid);
			
			if(count($extraserviceids) > 0)
			{
				$extServDet=$this->Restapi_model->extServiceDetails($serviceid,$extraserviceids);
				$extraServiceCount=count($extServDet->result_array()); 
			}
				
			
			if(count($serviceDetList) >0)
			{
					$servUserDet=$this->Restapi_model->serviceUserDetails($serviceDetList[0]['user_id']);
					$servUserDetails=$servUserDet->result_array();
					
					
					
					if(count($extraserviceids) > 0)
					{
						$i=0;
						$subtotal=0;
						if($extraServiceCount > 0 )
						{
							$extservid=array();
							$extservdesc=array();
							$extservprice=array();
							
							foreach(@$extServDet->result() as $extServ)
							{
									$i=$i+1;
									$extservid[$i]=$extServ->extra_service_id;
									$extservdesc[$i]=$extServ->extra_serv_description;
									$extservprice[$i]=$extServ->extra_serv_price;
									$subtotal=$subtotal+$extServ->extra_serv_price;
							}
						}
							
							
							//@$subtotal=@$subtotal+ @$servUserDetails[0]['basic_price'];
							if(($extraServiceCount != 0) && (count($extservid) == 0))
								 $extservstatus="yes";
							else
								 $extservstatus="";
				
							$data = array(
									'id'      => @$servUserDetails[0]["user_id"],
									'qty'     => 1,
									'price'   => @$servUserDetails[0]['basic_price'],
									'name'    => @$servUserDetails[0]['name'],
									'post_date' => @$serviceDetList[0]['post_date'], 
									'profile_pic' => @$servUserDetails[0]["profile_pic"],
									'userid'  => $userid, // @$servUserDetails[0]["user_id"],
									'servicetitle' => @$serviceDetList[0]['service_title'],
									'serviceid' => $serviceid,
								//	'extservid'   => serialize($extservid),
							//		'extservdesc' => serialize($extservdesc),
								//	'extservprice' => serialize($extservprice),
									'service_image1_thumb' => @$serviceDetList[0]['service_image1_thumb'],
								//	'coupon'    => 'XMAS-50OFF',
									'curservicetotal'	=> $subtotal,
									'extservstatus'  => $extservstatus,
									'orderid'	=> ''
							);
								
						if($extraServiceCount > 0 )
						{		
								$data["extservid"]=serialize($extservid);
								$data["extservdesc"] = serialize($extservdesc);
								$data["extservprice"] = serialize($extservprice);
						}
							
					}
					else
					{
						
						            $extServDet=$this->Restapi_model->extraServicExist($serviceid);
									$extraServiceCount=count($extServDet->result_array());
									$extservstatus="";
							
									if($extraServiceCount != 0)
									  $extservstatus="yes";
							  
									$subtotal=$servUserDetails[0]['basic_price'];
									$data = array(
									'id'      => @$servUserDetails[0]["user_id"],
									'qty'     => 1,
									'price'   => @$servUserDetails[0]['basic_price'],
									'name'    => @$servUserDetails[0]['name'],
									'post_date' => @$serviceDetList[0]['post_date'], 
									'profile_pic' => @$servUserDetails[0]["profile_pic"],
									'userid'  => $userid, // @$servUserDetails[0]["user_id"],
									'servicetitle' => @$serviceDetList[0]['service_title'],
									'serviceid' => $serviceid,
									//'extservid'   => $extservid,
									//'extservdesc' => $extservdesc,
								//	'extservprice' => $extservprice,
									'service_image1_thumb' => @$serviceDetList[0]['service_image1_thumb'],
								//	'coupon'    => 'XMAS-50OFF',
									'curservicetotal'	=> $subtotal,
									'extservstatus'  => $extservstatus,
									'orderid'	=> ''
							);
					}
					
					$this->Restapi_model->saveCart($userid,$serviceid,$data);
			}
			$response=array(
			   'status' =>  'success'
			); 
			header('Content-Type: application/json');
		    echo json_encode($response);
	}
	public function cartList()
	{
		   $this->load->model("Restapi_model");
		   $data=json_decode(file_get_contents('php://input'),true);
		   $userid=$data["user_id"];
		   $mycartData=$this->Restapi_model->showCartData($userid);
		 //  print_r($mycartData);
		   
		   $cartList=array();
		   $i=0;
		   $grandtotal=0;
			foreach($mycartData as $cartData)
			{
					$cartList[$i]["service_id"]=$cartData["serviceid"];
					$cartList[$i]["service_title"]=$cartData["servicetitle"];
					$cartList[$i]["service_date"]=date("M d,Y",strtotime($cartData["post_date"]));
					if($cartData["service_image1_thumb"] == "")
					  $cartData["service_image1_thumb"] = "";
					$cartList[$i]["service_pic"]=$cartData["service_image1_thumb"];
					$cartList[$i]["user_id"]=$cartData["id"];
					$cartList[$i]["user_name"]=$cartData["name"];
					if($cartData["profile_pic"] == "")
					  $cartData["profile_pic"]="assets/images/no_image.png";
					$cartList[$i]["user_pic"]=$cartData["profile_pic"];
					$cartList[$i]["basic_price"]=$cartData["price"];
			
				    $subtotal=0;
				    $extraserviceids=unserialize($cartData["extservid"]);
					$extServDet=$this->Restapi_model->extServiceDetails($cartData["serviceid"],$extraserviceids);
				    $extrasList=array();
					$j=0;
					if(count(@$extServDet->result()) > 0)
					{
						foreach(@$extServDet->result() as $extServ)
						{
								$extrasList[$j]['id']=$extServ->extra_service_id;
								$extrasList[$j]['title']=$extServ->extra_serv_description;
								$extrasList[$j]['price']=$extServ->extra_serv_price;
								$subtotal=$subtotal+$extServ->extra_serv_price;
								$j=$j+1;
						}
					}
					$cartList[$i]["extras_ordered"]=$subtotal;
					$cartList[$i]["ordered_total_price"]=$cartData["price"] + $subtotal;
					$grandtotal=$grandtotal + $cartList[$i]["ordered_total_price"];
					$cartList[$i]['extrasList']=$extrasList;
					$i=$i+1;
			}
			
			$totalservices=$i;
			$totalprice=$grandtotal;
			
			$response=array(
			   'status' =>  'success',
			   'total_services' =>  $totalservices,
			   'total_price'  => $grandtotal,
			   'cart_list' => $cartList
			); 
			header('Content-Type: application/json');
		    echo json_encode($response);
	}
	public function removeFromCart()
	{
		 $this->load->model("Restapi_model");
		 $data=json_decode(file_get_contents('php://input'),true);
		 $userid=$data["user_id"];
		 $serviceid=$data["service_id"];
		 $this->Restapi_model->deleteCartItem($userid,$serviceid);
		 
		 $mycartData=$this->Restapi_model->showCartData($userid);
		 $cartList=array();
		 $i=0;
		 $grandtotal=0;
		 foreach($mycartData as $cartData)
	  	 {
				    $subtotal=0;
				    $extraserviceids=unserialize($cartData["extservid"]);
					$extServDet=$this->Restapi_model->extServiceDetails($cartData["serviceid"],$extraserviceids);
				    $extrasList=array();
					$j=0;
					foreach(@$extServDet->result() as $extServ)
					{
							$subtotal=$subtotal+$extServ->extra_serv_price;
					}
					$cartList[$i]["ordered_total_price"]=$cartData["price"] + $subtotal;
					$grandtotal=$grandtotal + $cartList[$i]["ordered_total_price"];
					$i=$i+1;
		  }
			
		  $totalservices=$i;
		 // $totalprice=$grandtotal;
		 
		 $response=array(
		   'status' =>  'success',
		   'total_services' => $totalservices,
		   'total_price' => $grandtotal
		 ); 
		 header('Content-Type: application/json');
		 echo json_encode($response);
	}
	public function removeExtras()
	{
		 $this->load->model("Restapi_model");
		 $data=json_decode(file_get_contents('php://input'),true);
		 $userid=$data["user_id"];
		 $serviceid=$data["service_id"];
		 $extrasid=$data["extras_id"];
		 $this->Restapi_model->removeExtraServId($userid,$serviceid,$extrasid);
		 $response=array(
		   'status' =>  'success',
		 ); 
		 header('Content-Type: application/json');
		 echo json_encode($response);
	}
	Public function placeOrder()
	{
		
			 $this->load->model("Restapi_model");
			 $data=json_decode(file_get_contents('php://input'),true);
			 $userid=$data["user_id"];
			 $useridDet=array();
			 $cartContents=$this->Restapi_model->showCartData($userid);
			 $grandTotal=0;
			 $orderid=0;
			 $noofServices=count($cartContents);
			 $serviceOwnersIds=array();
			
			/* foreach($cartContents as $cartcontent)
			 {*/		
				$myOrderData['user_id']= $userid;   
				$myOrderData['order_date']=date("Y-m-d H:i:s");
				$myOrderData['no_of_services']=$noofServices;
				$orderid=$this->Restapi_model->saveOrderInfo($myOrderData);  // commented
				// break;
			 /*}*/
			
			$i=0;
			foreach($cartContents as $cartcontent)
			{
				    $serviceOwnersIds[$i]=$cartcontent['id'];
					$myOrderDetData['order_id']=$orderid;
					$existStat=$this->Restapi_model->serviceidExistOrder($cartcontent['serviceid'],$orderid); // commented
					
					if($existStat != "yes")
					{
							$myOrderDetData['service_id']=$cartcontent['serviceid'];
							if(@$cartcontent['extservid'])
							{
							$myOrderDetData['extra_id']=$cartcontent['extservid'];
							}
							$myOrderDetData['price']=$cartcontent['price'];
							if(@$cartcontent['extservprice'])
							{
							   $myOrderDetData['extra_price']=$cartcontent['extservprice'];
							}
							$myOrderDetData['sub_total']=$cartcontent['curservicetotal']; 
							$myOrderDetData['order_status']='Pending';
							$grandTotal=$grandTotal+$myOrderDetData['sub_total'];
							$this->Restapi_model->saveOrderDetailsInfo($myOrderDetData);  // commented
					}
				$i=$i+1;		
			}
			$myOrder['grand_total']=$grandTotal;
			$this->Restapi_model->updateGrandTotal($myOrder,$orderid); // commented
			$this->Restapi_model->deleteCartData($userid); // commented
		
			
			// starts push notification 
			$orderMsg="Your have placed an order";
			$useridDet[0]=$userid;
			$this->pushNotifications($useridDet,$orderMsg);
			
			$jobNotificMsg="send you a job request";
			$this->pushNotifications($serviceOwnersIds,$jobNotificMsg);
			// ends push notification
			
			
			
			$response=array(
		      'status' =>  'success',
		    ); 
		    header('Content-Type: application/json');
		    echo json_encode($response);
			
		
	}
	
	
	public function pushNotifications($serviceOwnersIds,$custom_message)
	{
		   $this->load->model("Restapi_model");
		   foreach($serviceOwnersIds as $serviceOwner)
		   {
					 $userDet=$this->Restapi_model->accountDetails($serviceOwner);
					 
					 //set api key 
					 $api_key = "AIzaSyBtY1-FwZg9PCqufcwfiHgv8k7V52107DA"; 
					 
					 //Getting registration token we have to make it as array 
					 $reg_token = $userDet[0]["push_reg_id"];
					 
					 //Getting the message 
					 if($custom_message != "Your have placed an order")
					 {
					    $message = $userDet['name'] . $custom_message;
					 }
					 else
					 {
						 $message = $custom_message;
					 }

					 
					 //Creating a message array 
					 $msg = array
					 (
					 'message' => $message,
					 'title' => 'Personal Service',
					 'subtitle' => 'Personal Service Notifications',
					 'tickerText' => '',
					 'vibrate' => 1,
					 'sound' => 1,
					 'largeIcon' => 'large_icon',
					 'smallIcon' => 'small_icon'
					 );
					 
					 
					 //Creating a new array fileds and adding the msg array and registration token array here 
					 $fields = array
					 (
					 'registration_ids' => array($reg_token),
					 'data' => $msg
					 );
					 
					 //Adding the api key in one more array header 
					 $headers = array
					 (
					 'Authorization: key=' . $api_key,
					 'Content-Type: application/json'
					 ); 
					 
					 //Using curl to perform http request 
					 $ch = curl_init();
					 curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					 curl_setopt( $ch,CURLOPT_POST, true );
					 curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
					 curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
					 curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
					 curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					 
					 //Getting the result 
					 $result = curl_exec($ch );
					 curl_close( $ch );
					 
					 //Decoding json from result 
					 $res = json_decode($result);
					 
					 //Getting value from success 
					 $flag = $res->success;
					 
					 //if success is 1 means message is sent   	
		  }
	}
	
	
	public function myOrders()
	{
		  $this->load->model("Restapi_model");
		  $data=json_decode(file_get_contents('php://input'),true);
		  $userid=$data["user_id"];
		  $UserOrder=$this->Restapi_model->showUserOrder($userid);
		 
		  $i=0;
		  $orderlist=array();
		  $grandtotal=0;
		 
		   foreach($UserOrder->result() as $orders)
		   {
			  	  $OrderDetails=array();
		   		  $showOrderDet=array();
				  $OrderDetails['order_no']=$orders->order_no;
				  $OrderDetails['total_services']=$orders->total_services;
				  $OrderDetails['total_price']=$orders->total_price;
				  $OrderDetails['ordered_date']=date("M d,Y",strtotime($orders->ordered_date));
				  $orderid=$orders->order_no;
				  $orderDetailsObj=$this->Restapi_model->showOrderDetails($orderid);
				  $orderServDetails=$orderDetailsObj->result_array();
				//  print_r($orderDetails);
				  
				  $j=0;
				  foreach($orderServDetails as $ordDtl)
				  {
						   // print_r($ordDtl);
						   $showOrderDet[$j]['service_id']=$ordDtl['service_id'];
						   $ratingDetails=array();
						   $ratingDetails=$this->Restapi_model->ratingDet($orders->order_no,$ordDtl['service_id'],$userid);
						   
						   if(count($ratingDetails) > 0)
						   {
							   $showOrderDet[$j]['is_rated']=true;
							   $showOrderDet[$j]['my_rating']=$ratingDetails[0]['rating'];
							   $showOrderDet[$j]['my_review']=$ratingDetails[0]['review'];
						   }
						   else
						   {
							   $showOrderDet[$j]['is_rated']=false;
							   $showOrderDet[$j]['my_rating']="";
							   $showOrderDet[$j]['my_review']="";
						   }
						   
						   $showOrderDet[$j]['service_title']=$ordDtl['service_title'];
						   $showOrderDet[$j]['service_image']=$ordDtl['service_image1'];
						   $showOrderDet[$j]['user_id']=$ordDtl['user_id'];
						   $showOrderDet[$j]['user_name']=$ordDtl['name'];
						   if($ordDtl['profile_pic'] == "")
							  $ordDtl['profile_pic']="assets/images/no_image.png";
						   $showOrderDet[$j]['user_pic']=$ordDtl['profile_pic'];   // no image check
						   $showOrderDet[$j]['basic_price']=$ordDtl['price'];
							$subtotal=0;
							$extraserviceids=unserialize($ordDtl["extra_id"]);
							$extServDet=$this->Restapi_model->extServiceDetails($ordDtl["service_id"],$extraserviceids);
							$extrasList=array();
							$h=0;
							foreach(@$extServDet->result() as $extServ)
							{
									$extrasList[$h]['id']=$extServ->extra_service_id;
									$extrasList[$h]['title']=$extServ->extra_serv_description;
									$extrasList[$h]['price']=$extServ->extra_serv_price;
									$subtotal=$subtotal+$extServ->extra_serv_price;
									$h=$h+1;
							}
							$showOrderDet[$j]['extras_ordered']=$subtotal;
							$showOrderDet[$j]['order_status']=$ordDtl['order_status'];
							$showOrderDet[$j]["ordered_total_price"]=$ordDtl["price"] + $subtotal;
							$grandtotal=$grandtotal + $showOrderDet[$j]["ordered_total_price"];
							$showOrderDet[$j]['contact_phone']=$ordDtl['mobile'];
							$showOrderDet[$j]['contact_email']=$ordDtl['email'];
							$showOrderDet[$j]['extrasList']=$extrasList;
							$j=$j+1;
				    }
					//$containlist=$showOrderBasic;
			     	$OrderDetails['order_items']=$showOrderDet;
				    $orderlist[$i]=$OrderDetails;
					$i=$i+1;
		  }
		
		  
		  $response=array(
		      'status' =>  'success',
			  'order_list' => $orderlist
		   ); 
		   header('Content-Type: application/json');
		   echo json_encode($response);
		   
	}
	public function acceptJobRequest()
	{
		 $this->load->model("Restapi_model");
		  $data=json_decode(file_get_contents('php://input'),true);
		  $userid=$data["user_id"];
		  $jobId=$data["job_id"];
		  $myData["order_status"]="Started";
		  $myData["order_accepted_time"]=date("Y-m-d H:i:s");
		  $this->Restapi_model->updateJobStatus($jobId,$myData);
		
		
		    // Pending Job Details - Only Job Requested coming
			$pendjobDetInfoDet=$this->Restapi_model->PendingMyjobServDet($userid);
			$pendjobDetailInfo=array();
			$pendjobExtraIds=array();
			if(count($pendjobDetInfoDet->result()) > 0)
			{
				$i=0;
				foreach($pendjobDetInfoDet->result_array() as $jobDet)
				{
					$extraServDet=array();
					if($jobDet["id"] == "")
					    $jobDet["id"]="";
				
					$jobDet['service_date']=date("M d, Y",strtotime($jobDet['service_date']));
					$jobDet['requested_date']=date("M d, Y",strtotime($jobDet['requested_date']));
					$pendjobDetailInfo[$i]=$jobDet;
					$jobServiceIds=unserialize($jobDet["id"]);
					if(count($jobServiceIds) > 0)
					{
						for($j=1;$j <= count($jobServiceIds); $j++)
						{
							$extServId=$jobServiceIds[$j];
							if($extServId[0] != "")
							{
							    $extRes=$this->Restapi_model->extraServiceDetails($extServId[0]);
								if(count($extRes) > 0)
							      $extraServDet[]=$extRes;
							}
						}
					}
					$pendjobDetailInfo[$i]['extras_list']=$extraServDet;
					$i=$i+1;
				}
			}
			else
			{
				$pendjobDetailInfo=[];
			}
		
		
		
		    // Active Job Details - Job started
			$actjobDetInfoDet=$this->Restapi_model->ActiveMyjobServDet($userid);
			$actjobDetailInfo=array();
			$actjobExtraIds=array();
			if(count($actjobDetInfoDet->result_array()) > 0)
			{
				$i=0;
				foreach($actjobDetInfoDet->result_array() as $jobDet)
				{
					$extraServDet=array();
					if($jobDet["id"] == "")
					    $jobDet["id"]="";
					$jobDet['service_date']=date("M d, Y",strtotime($jobDet['service_date']));
					$jobDet['requested_date']=date("M d, Y",strtotime($jobDet['requested_date']));
					$actjobDetailInfo[$i]=$jobDet;
					$jobServiceIds=unserialize($jobDet["id"]);
					if(count($jobServiceIds) > 0)
					{
						for($j=1;$j <= count($jobServiceIds); $j++)
						{
							$extServId=$jobServiceIds[$j];
							if($extServId[0] != "")
							{
							    $extRes=$this->Restapi_model->extraServiceDetails($extServId[0]);
								if(count($extRes) > 0)
							      $extraServDet[]=$extRes;
							}
						}
					}
					$actjobDetailInfo[$i]['extras_list']=$extraServDet;
					$i=$i+1;
				}
			}
			else
			{
				$actjobDetailInfo=[];
			}
			
		
		    // Completed Job Details - Only Job Completed jobs coming
			$compjobDetInfoDet=$this->Restapi_model->CompletedMyjobServDet($userid);
			$compjobDetailInfo=array();
			$compjobExtraIds=array();
			
			
			
			if(count($compjobDetInfoDet->result_array()) > 0)
			{
				$i=0;
				foreach($compjobDetInfoDet->result_array() as $jobDet)
				{
					$extraServDet=array();
				    if($jobDet["id"] == "")
					    $jobDet["id"]="";
					$jobDet['service_date']=date("M d, Y",strtotime($jobDet['service_date']));
					$jobDet['requested_date']=date("M d, Y",strtotime($jobDet['requested_date']));
					$compjobDetailInfo[]=$jobDet;
					$jobServiceIds=unserialize($jobDet["id"]);
				//	$extraServDet=array();
					for($j=1;$j <= count($jobServiceIds); $j++)
					{
						$extServId=$jobServiceIds[$j];
						if($extServId[0] != "")
						{
						  	$extRes=$this->Restapi_model->extraServiceDetails($extServId[0]);
							if(count($extRes) > 0)
							   $extraServDet[]=$extRes;
						}
					}
					
					$compjobDetailInfo[$i]['extras_list']=$extraServDet;
					$i=$i+1;
				}
			}
		
		  	 $response=array(
				   'status' =>  'success',
				   'job_requests_list' => $pendjobDetailInfo,
				   'active_jobs_list' => $actjobDetailInfo,
				   'completed_jobs_list' => $compjobDetailInfo
		     ); 
			 header('Content-Type: application/json');
		 	 echo json_encode($response);
	}
	public function rejectJobRequest()
	{
		 $this->load->model("Restapi_model");
		  $data=json_decode(file_get_contents('php://input'),true);
		  $userid=$data["user_id"];
		  $jobId=$data["job_id"];
		  $myData["order_status"]="Rejected";
		  $myData["order_rejected_time"]=date("Y-m-d H:i:s");
		  $this->Restapi_model->updateJobStatus($jobId,$myData);
		
		
		    // Pending Job Details - Only Job Requested coming
			$pendjobDetInfoDet=$this->Restapi_model->PendingMyjobServDet($userid);
			$pendjobDetailInfo=array();
			$pendjobExtraIds=array();
			if(count($pendjobDetInfoDet->result()) > 0)
			{
				$i=0;
				foreach($pendjobDetInfoDet->result_array() as $jobDet)
				{
					$extraServDet=array();
					if($jobDet["id"] == "")
					    $jobDet["id"]="";
				
					$jobDet['service_date']=date("M d, Y",strtotime($jobDet['service_date']));
					$jobDet['requested_date']=date("M d, Y",strtotime($jobDet['requested_date']));
					$pendjobDetailInfo[$i]=$jobDet;
					$jobServiceIds=unserialize($jobDet["id"]);
					if(count($jobServiceIds) > 0)
					{
						for($j=1;$j <= count($jobServiceIds); $j++)
						{
							$extServId=$jobServiceIds[$j];
							if($extServId[0] != "")
							{
							    $extRes=$this->Restapi_model->extraServiceDetails($extServId[0]);
								if(count($extRes) > 0)
							      $extraServDet[]=$extRes;
							}
						}
					}
					$pendjobDetailInfo[$i]['extras_list']=$extraServDet;
					$i=$i+1;
				}
			}
			else
			{
				$pendjobDetailInfo=[];
			}
		
		
		
		    // Active Job Details - Job started
			$actjobDetInfoDet=$this->Restapi_model->ActiveMyjobServDet($userid);
			$actjobDetailInfo=array();
			$actjobExtraIds=array();
			if(count($actjobDetInfoDet->result_array()) > 0)
			{
				$i=0;
				foreach($actjobDetInfoDet->result_array() as $jobDet)
				{
					$extraServDet=array();
					if($jobDet["id"] == "")
					    $jobDet["id"]="";
					$jobDet['service_date']=date("M d, Y",strtotime($jobDet['service_date']));
					$jobDet['requested_date']=date("M d, Y",strtotime($jobDet['requested_date']));
					$actjobDetailInfo[$i]=$jobDet;
					$jobServiceIds=unserialize($jobDet["id"]);
					if(count($jobServiceIds) > 0)
					{
						for($j=1;$j <= count($jobServiceIds); $j++)
						{
							$extServId=$jobServiceIds[$j];
							if($extServId[0] != "")
							{
							    $extRes=$this->Restapi_model->extraServiceDetails($extServId[0]);
								if(count($extRes) > 0)
							      $extraServDet[]=$extRes;
							}
						}
					}
					$actjobDetailInfo[$i]['extras_list']=$extraServDet;
					$i=$i+1;
				}
			}
			else
			{
				$actjobDetailInfo=[];
			}
			
		
		    // Completed Job Details - Only Job Completed jobs coming
			$compjobDetInfoDet=$this->Restapi_model->CompletedMyjobServDet($userid);
			$compjobDetailInfo=array();
			$compjobExtraIds=array();
			
			
			
			if(count($compjobDetInfoDet->result_array()) > 0)
			{
				$i=0;
				foreach($compjobDetInfoDet->result_array() as $jobDet)
				{
					$extraServDet=array();
				    if($jobDet["id"] == "")
					    $jobDet["id"]="";
					$jobDet['service_date']=date("M d, Y",strtotime($jobDet['service_date']));
					$jobDet['requested_date']=date("M d, Y",strtotime($jobDet['requested_date']));
					$compjobDetailInfo[]=$jobDet;
					$jobServiceIds=unserialize($jobDet["id"]);
				//	$extraServDet=array();
					for($j=1;$j <= count($jobServiceIds); $j++)
					{
						$extServId=$jobServiceIds[$j];
						if($extServId[0] != "")
						{
						  	$extRes=$this->Restapi_model->extraServiceDetails($extServId[0]);
							if(count($extRes) > 0)
							   $extraServDet[]=$extRes;
						}
					}
					
					$compjobDetailInfo[$i]['extras_list']=$extraServDet;
					$i=$i+1;
				}
			}
		
		  	 $response=array(
				   'status' =>  'success',
				   'job_requests_list' => $pendjobDetailInfo,
				   'active_jobs_list' => $actjobDetailInfo,
				   'completed_jobs_list' => $compjobDetailInfo
		     ); 
			 header('Content-Type: application/json');
		 	 echo json_encode($response);
	}
	public function jobCompletedRequest()
	{
		 $this->load->model("Restapi_model");
		  $data=json_decode(file_get_contents('php://input'),true);
		  $userid=$data["user_id"];
		  $jobId=$data["job_id"];
		  $myData["order_status"]="Completed";
		  $myData["order_completed_time	"]=date("Y-m-d H:i:s");
		  $this->Restapi_model->updateJobStatus($jobId,$myData);
		
		
		    // Pending Job Details - Only Job Requested coming
			$pendjobDetInfoDet=$this->Restapi_model->PendingMyjobServDet($userid);
			$pendjobDetailInfo=array();
			$pendjobExtraIds=array();
			if(count($pendjobDetInfoDet->result()) > 0)
			{
				$i=0;
				foreach($pendjobDetInfoDet->result_array() as $jobDet)
				{
					$extraServDet=array();
					if($jobDet["id"] == "")
					    $jobDet["id"]="";
				
					$jobDet['service_date']=date("M d, Y",strtotime($jobDet['service_date']));
					$jobDet['requested_date']=date("M d, Y",strtotime($jobDet['requested_date']));
					$pendjobDetailInfo[$i]=$jobDet;
					$jobServiceIds=unserialize($jobDet["id"]);
					if(count($jobServiceIds) > 0)
					{
						for($j=1;$j <= count($jobServiceIds); $j++)
						{
							$extServId=$jobServiceIds[$j];
							if($extServId[0] != "")
							{
							    $extRes=$this->Restapi_model->extraServiceDetails($extServId[0]);
								if(count($extRes) > 0)
							      $extraServDet[]=$extRes;
							}
						}
					}
					$pendjobDetailInfo[$i]['extras_list']=$extraServDet;
					$i=$i+1;
				}
			}
			else
			{
				$pendjobDetailInfo=[];
			}
		
		
		
		    // Active Job Details - Job started
			$actjobDetInfoDet=$this->Restapi_model->ActiveMyjobServDet($userid);
			$actjobDetailInfo=array();
			$actjobExtraIds=array();
			if(count($actjobDetInfoDet->result_array()) > 0)
			{
				$i=0;
				foreach($actjobDetInfoDet->result_array() as $jobDet)
				{
					$extraServDet=array();
					if($jobDet["id"] == "")
					    $jobDet["id"]="";
					$jobDet['service_date']=date("M d, Y",strtotime($jobDet['service_date']));
					$jobDet['requested_date']=date("M d, Y",strtotime($jobDet['requested_date']));
					$actjobDetailInfo[$i]=$jobDet;
					$jobServiceIds=unserialize($jobDet["id"]);
					if(count($jobServiceIds) > 0)
					{
						for($j=1;$j <= count($jobServiceIds); $j++)
						{
							$extServId=$jobServiceIds[$j];
							if($extServId[0] != "")
							{
							    $extRes=$this->Restapi_model->extraServiceDetails($extServId[0]);
								if(count($extRes) > 0)
							      $extraServDet[]=$extRes;
							}
						}
					}
					$actjobDetailInfo[$i]['extras_list']=$extraServDet;
					$i=$i+1;
				}
			}
			else
			{
				$actjobDetailInfo=[];
			}
			
		
		    // Completed Job Details - Only Job Completed jobs coming
			$compjobDetInfoDet=$this->Restapi_model->CompletedMyjobServDet($userid);
			$compjobDetailInfo=array();
			$compjobExtraIds=array();
			
			
			
			if(count($compjobDetInfoDet->result_array()) > 0)
			{
				$i=0;
				foreach($compjobDetInfoDet->result_array() as $jobDet)
				{
					$extraServDet=array();
				    if($jobDet["id"] == "")
					    $jobDet["id"]="";
					$jobDet['service_date']=date("M d, Y",strtotime($jobDet['service_date']));
					$jobDet['requested_date']=date("M d, Y",strtotime($jobDet['requested_date']));
					$compjobDetailInfo[]=$jobDet;
					$jobServiceIds=unserialize($jobDet["id"]);
				//	$extraServDet=array();
					for($j=1;$j <= count($jobServiceIds); $j++)
					{
						$extServId=$jobServiceIds[$j];
						if($extServId[0] != "")
						{
						  	$extRes=$this->Restapi_model->extraServiceDetails($extServId[0]);
							if(count($extRes) > 0)
							   $extraServDet[]=$extRes;
						}
					}
					
					$compjobDetailInfo[$i]['extras_list']=$extraServDet;
					$i=$i+1;
				}
			}
		
		  	 $response=array(
				   'status' =>  'success',
				   'job_requests_list' => $pendjobDetailInfo,
				   'active_jobs_list' => $actjobDetailInfo,
				   'completed_jobs_list' => $compjobDetailInfo
		     ); 
			 header('Content-Type: application/json');
		 	 echo json_encode($response);
	}
	public function notifications()
	{
		  $this->load->model("Restapi_model");
		  $data=json_decode(file_get_contents('php://input'),true);
		  $userid=$data["user_id"];
		  $notifications_list=array();
		  $i=0;
		  $jobrequestnotifications=$this->Restapi_model->job_request_notifications($userid);
		  foreach($jobrequestnotifications as $jobreqnotifications)
           {
		      // $notifications_list[$i]['notification_id']=$jobreqnotifications->id;
			   $notifications_list[$i]['notification_text']=$jobreqnotifications->name . " send you a job request";
			   $notifications_list[$i]['notification_type']=0; //"Job Request";
			   if($jobreqnotifications->profile_pic == "")
				  $jobreqnotifications->profile_pic="assets/images/no_image.png";
			   $notifications_list[$i]['user_pic']=$jobreqnotifications->profile_pic;
			   $notifications_list[$i]['date']=date("l M d, Y",strtotime($jobreqnotifications->order_date));
			   $i=$i+1;
		   }
		  $jobacceptnotifications=$this->Restapi_model->job_accept_notifications($userid);
		  foreach($jobacceptnotifications as $jobacceptnotific)
          {
			//   $notifications_list[$i]['notification_id']=$jobacceptnotific->id;
			   $notifications_list[$i]['notification_text']=$jobacceptnotific->name . " accepted your order";
			   $notifications_list[$i]['notification_type']=1; // "Job Accept";
			   if($jobacceptnotific->profile_pic == "")
				  $jobacceptnotific->profile_pic="assets/images/no_image.png";
			   $notifications_list[$i]['user_pic']=$jobacceptnotific->profile_pic;
			   $notifications_list[$i]['date']=date("l M d, Y",strtotime($jobacceptnotific->order_date));
			   $i=$i+1; 
		  }
		  $jobrejectnotifications=$this->Restapi_model->job_reject_notifications($userid);
		  foreach($jobrejectnotifications as $jobrejectnotific)
          {
			//   $notifications_list[$i]['notification_id']=$jobrejectnotific->id;
			   $notifications_list[$i]['notification_text']=$jobrejectnotific->name . " rejected your order";
			   $notifications_list[$i]['notification_type']=2; // "Job Reject";
			   if($jobrejectnotific->profile_pic == "")
				  $jobrejectnotific->profile_pic="assets/images/no_image.png";
			   $notifications_list[$i]['user_pic']=$jobrejectnotific->profile_pic;
			   $notifications_list[$i]['date']=date("l M d, Y",strtotime($jobrejectnotific->order_date));
			   $i=$i+1; 
		  }
		  $jobcompletednotifications=$this->Restapi_model->job_completed_notifications($userid);
		  foreach($jobcompletednotifications as $jobcomplnotific)
          {
			 //  $notifications_list[$i]['notification_id']=$jobcomplnotific->id;
			   $notifications_list[$i]['notification_text']=$jobcomplnotific->name . " completed your order";
			   $notifications_list[$i]['notification_type']=3; //"Job Completed";
			   if($jobcomplnotific->profile_pic == "")
				  $jobcomplnotific->profile_pic="assets/images/no_image.png";
			   $notifications_list[$i]['user_pic']=$jobcomplnotific->profile_pic;
			   $notifications_list[$i]['date']=date("l M d, Y",strtotime($jobcomplnotific->order_date));
			   $i=$i+1; 
		  }
		
		  $response=array(
			   'status' =>  'success',
			   'notifications_list' => $notifications_list
		  ); 
		  header('Content-Type: application/json');
		  echo json_encode($response);
	}
	public function myRevenue()
	{
	     $this->load->model("Restapi_model");
		 $data=json_decode(file_get_contents('php://input'),true);
		 $userid=$data["user_id"];
		 $showEarnings=$this->Restapi_model->showMyEarnings($userid);
		 $myEarnings=$showEarnings[0]['total'];
		 if($myEarnings == null)
		   $myEarnings = 0;
		 $myOrderCount=$this->Restapi_model->showMyOrderCount($userid);
		 $availableBalance=0;
		 $orderList=$this->Restapi_model->OrderList($userid);
		/* echo "<pre>";
		 print_r($orderList);
		 echo "</pre>";
		 exit;*/
		 
		 $response=array(
			  'status' =>  'success',
			  'myearnings' => $myEarnings,
			  'totalorders' => $myOrderCount,
			  'availablebalance' => $availableBalance,
			  'order_list' => $orderList
		  ); 
		  header('Content-Type: application/json');
		  echo json_encode($response);
	}
	
	public function likeUnlike()
	{
		 $this->load->model("Restapi_model");
		 $data=json_decode(file_get_contents('php://input'),true);
		 $userid=$data["user_id"];
		 $serviceid=$data["service_id"];
		// $action=$data["action"];
		 
		 $status=$this->Restapi_model->like_status($userid,$serviceid);
		 if(count($status) > 0) // already liked
		  {  
			 $this->Restapi_model->like_delete($userid,$serviceid);
		}
		else  
		{ 
		    $data = array(
			   'service_id' => $serviceid,
			   'user_id' => $userid  
			 );
			 $this->Restapi_model->like($data);
		}	
		 $response=array(
			  'status' =>  'success'
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	} 
	public function comment()
	{
			 $this->load->model("Restapi_model");
			 $data=json_decode(file_get_contents('php://input'),true);
			 
			 $userid=$data["user_id"];
			 $serviceid=$data["service_id"];
			 $comment=$data["comment"];
			
			 $myComment = array(
				  'comments' => $comment,
				  'user_id' => $userid,
				  'service_id' => $serviceid
				 );
			 $this->Restapi_model->addComment($myComment,$serviceid);
			  $response=array(
			  'status' =>  'success'
			);
			header('Content-Type: application/json');
			echo json_encode($response);
	}
	public function postservice()
	{
		  $this->load->model("Restapi_model");
		  $data=json_decode(file_get_contents('php://input'),true);
		  $mydata['user_id']=$data["user_id"];
		  $mydata['service_title']=$data["service_title"];
		  $mydata['service_cat_id']=$data["category"];
		  $mydata['service_sub_cat_id']=$data["sub_category"];
		  $mydata['service_description']=$data["service_description"];
		  $mydata['location_anywhere']=$data["location_anywhere"];
		  $mydata['service_location']=$data["service_location"];
		  $mydata['service_location_latitude']=$data["service_location_latitude"];
		  $mydata['service_location_longitude']=$data["service_location_longitude"];
		  $mydata['service_basicinfo_stat']="yes";
		  $mydata["basic_price"]=$data["basic_price"];
		  $extras_list=array();
		  if($data["extras_list"] != "")
		  {
		    $extras_list=$data["extras_list"];
		  }
		  $mydata['service_pricing_stat']="yes";
		  
		  $mydata['service_image1']=$data["service_image1"];
		  $mydata['service_image1_thumb']=$data["service_image1_thumb"];
		  $mydata['service_image2']=$data["service_image2"];
		  $mydata['service_image2_thumb']=$data["service_image2_thumb"];
		  $mydata['service_image3']=$data["service_image3"];
		  $mydata['service_image3_thumb']=$data["service_image3_thumb"];
		  $mydata['service_image4']=$data["service_image4"];
		  $mydata['service_image4_thumb']=$data["service_image4_thumb"];
		  $mydata['service_gallery_stat']="yes";
		  
		  $mydata['service_publish_stat']=$data["service_publish_stat"];;
		  
		  $serviceid=$this->Restapi_model->saveServicePost($mydata);
		  for($i=0;$i<sizeof($extras_list);$i++)
		  {
			$dataSet[$i] = array ('service_id' =>  $serviceid, 'extra_serv_description' => $extras_list[$i]['desc'], 'extra_serv_price' => $extras_list[$i]['price'],'status'=>'on');
		  }
		  $this->Restapi_model->savePricingDetailsInfo($dataSet,$serviceid);
		
		  $response=array(
			  'status' =>  'success',
			  'service_id' => $serviceid
		  );
		  header('Content-Type: application/json');
		  echo json_encode($response);
	}
	public function editService()
	{
		  $this->load->model('Restapi_model');
		  $data = json_decode(file_get_contents('php://input'), true);
		  $userid=$data['user_id'];
		  $serviceid=$data['service_id'];
		  $serviceDetailList=$this->Restapi_model->serviceDetails($serviceid);  
		  /*echo "<pre>";
		  print_r($serviceDetailList->result_array());
		  echo "</pre>"; 
		  exit;*/
		
		  if(count($serviceDetailList->result_array()) > 0)
		  {
			       $ServDetails=$serviceDetailList->result_array();
				  /* $serviceImages=array($ServDetails[0]["service_image1"],$ServDetails[0]["service_image2"],$ServDetails[0]["service_image3"],$ServDetails[0]["service_image4"]);
				   $serviceThumbImages=array($ServDetails[0]["service_image1_thumb"],$ServDetails[0]["service_image2_thumb"],$ServDetails[0]["service_image3_thumb"],$ServDetails[0]["service_image4_thumb"]);*/
				 $serviceImages=array();
				  if($ServDetails[0]["service_image1"] != "")
				  {
					 $serviceImages[0]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceImages[0]['image_uploaded']=false;
				  }
				  $serviceImages[0]["img_url"]=$ServDetails[0]["service_image1"];


				  if($ServDetails[0]["service_image2"] != "")
				  {
					 $serviceImages[1]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceImages[1]['image_uploaded']=false;
				  }
				  $serviceImages[1]["img_url"]=$ServDetails[0]["service_image2"];
				  
				  
				  if($ServDetails[0]["service_image3"] != "")
				  {
					 $serviceImages[2]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceImages[2]['image_uploaded']=false;
				  }
				  $serviceImages[2]["img_url"]=$ServDetails[0]["service_image3"];
				  
				  
				  if($ServDetails[0]["service_image4"] != "")
				  {
					 $serviceImages[3]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceImages[3]['image_uploaded']=false;
				  }
				  $serviceImages[3]["img_url"]=$ServDetails[0]["service_image4"];
					  
					  
				  $serviceThumbImages=array();
				  if($ServDetails[0]["service_image1_thumb"] != "")
				  {
					 $serviceThumbImages[0]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceThumbImages[0]['image_uploaded']=false;
				  }
				  $serviceThumbImages[0]["img_url"]=$ServDetails[0]["service_image1_thumb"];
				  if($ServDetails[0]["service_image2_thumb"] != "")
				  {
					 $serviceThumbImages[1]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceThumbImages[1]['image_uploaded']=false;
				  }
				  $serviceThumbImages[1]["img_url"]=$ServDetails[0]["service_image2_thumb"];
				  if($ServDetails[0]["service_image3_thumb"] != "")
				  {
					 $serviceThumbImages[2]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceThumbImages[2]['image_uploaded']=false;
				  }
				  $serviceThumbImages[2]["img_url"]=$ServDetails[0]["service_image3_thumb"];
				  if($ServDetails[0]["service_image4_thumb"] != "")
				  {
					 $serviceThumbImages[3]['image_uploaded']=true;
				  }
				  else
				  {
					 $serviceThumbImages[3]['image_uploaded']=false;
				  }
				  $serviceThumbImages[3]["img_url"]=$ServDetails[0]["service_image4_thumb"];
				 
				   $response=array(
				      'status' =>  'success',
				      "user_id"=>$ServDetails[0]["user_id"],
					  "service_id" => $serviceid,
					  "service_title"=>$ServDetails[0]["service_title"],
					  "category"=>$ServDetails[0]["category"],
					  "category_name" => $ServDetails[0]["categoryname"],
					  "sub_category"=>$ServDetails[0]["sub_category"],
					  "subcategory_name"  => $ServDetails[0]["subcategoryname"],
					  "service_description"=>$ServDetails[0]["service_description"],
					  "location_anywhere"=>$ServDetails[0]["location_anywhere"],
					  "service_location"=>$ServDetails[0]["service_location"],
					  "service_location_latitude"=>$ServDetails[0]["service_location_latitude"],
					  "service_location_longitude"=>$ServDetails[0]["service_location_longitude"],
					  "service_description"=>$ServDetails[0]["service_description"],
					  "basic_price"=>$ServDetails[0]["basic_price"],
					  "extras_list"=>$extServDetails=$this->Restapi_model->ext_ServDtls($serviceid),
					  "service_images" => $serviceImages,
					  "service_thumb_images" => $serviceThumbImages,
					  "service_publish_stat"=>$ServDetails[0]["service_publish_stat"]
				  ); 
		  }
		  else
		  {
				   $response=array(
				   'status' =>  'false',
				   'message' => 'No service detail to show'
				   ); 
		  }
		  header('Content-Type: application/json');
		  echo json_encode($response);
	}
	public function updatePostService()
	{
		  $this->load->model("Restapi_model");
		  $data=json_decode(file_get_contents('php://input'),true);
		  $serviceid=$data["service_id"];
		  
		  if($data["user_id"] != "")
		  {
		    $mydata['user_id']=$data["user_id"];
		  }
		  if($data["service_title"] != "")
		  {
		     $mydata['service_title']=$data["service_title"];
		  }
		  if($data["category"] != "")
		  {
		     $mydata['service_cat_id']=$data["category"];
		  }
		  if($data["sub_category"] != "")
		  {
		     $mydata['service_sub_cat_id']=$data["sub_category"];
		  }
		  if($data["service_description"] != "")
		  {
		     $mydata['service_description']=$data["service_description"];
		  }
		  if($data["location_anywhere"] != "")
		  {
		     $mydata['location_anywhere']=$data["location_anywhere"];
		  }
		  if($data["service_location"] != "")
		  {
		     $mydata['service_location']=$data["service_location"];
		  }
		  if($data["service_location_latitude"] != "")
		  {
		      $mydata['service_location_latitude']=$data["service_location_latitude"];
		  }
		  if($data["service_location_longitude"] != "")
		  {
		     $mydata['service_location_longitude']=$data["service_location_longitude"];
		  }
		  if($data["service_basicinfo_stat"] != "")
		  {
		    $mydata['service_basicinfo_stat']="yes";
		  }
		  if($data["basic_price"] != "")
		  {
		    $mydata["basic_price"]=$data["basic_price"];
		  }
		  $extras_list=array();
		  if($data["extras_list"] != "")
		  {
		    $extras_list=$data["extras_list"];
		  }
		  $mydata['service_pricing_stat']="yes";
		  
		  
		  if($data["service_image1"] != "")
		  {
		 	 $mydata['service_image1']=$data["service_image1"];
		  }
		  if($data["service_image1_thumb"] != "")
		  {
		     $mydata['service_image1_thumb']=$data["service_image1_thumb"];
		  }
		  if($data["service_image2"] != "")
		  {
		     $mydata['service_image2']=$data["service_image2"];
		  }
		  if($data["service_image2_thumb"] != "")
		  {
		     $mydata['service_image2_thumb']=$data["service_image2_thumb"];
		  }
		  if($data["service_image3"] != "")
		  {
		     $mydata['service_image3']=$data["service_image3"];
		  }
		  if($data["service_image3_thumb"] != "")
		  {
		     $mydata['service_image3_thumb']=$data["service_image3_thumb"];
		  }
		  if($data["service_image4"] != "")
		  {
		     $mydata['service_image4']=$data["service_image4"];
		  }
		  if($data["service_image4_thumb"] != "")
		  {
		     $mydata['service_image4_thumb']=$data["service_image4_thumb"];
		  }
		  $mydata['service_gallery_stat']="yes";
		  if($data["service_publish_stat"] != "")
		  {
		     $mydata['service_publish_stat']=$data["service_publish_stat"];// "no";
		  }

		  $this->Restapi_model->updateServicePost($mydata,$serviceid);
		  for($i=0;$i<sizeof($extras_list);$i++)
		  {
			$dataSet[$i] = array ('service_id' =>  $serviceid, 'extra_serv_description' => $extras_list[$i]['desc'], 'extra_serv_price' => $extras_list[$i]['price'],'status'=>'on');
		  }
		  $this->Restapi_model->savePricingDetailsInfo($dataSet,$serviceid);
		  $response=array(
			  'status' =>  'success'
		  );
		  header('Content-Type: application/json');
		  echo json_encode($response);
	}
	public function updatePublishStat()
	{
		  $this->load->model("Restapi_model");
		  $data=json_decode(file_get_contents('php://input'),true);
		  $serviceid=$data["service_id"];
		  $servicepublishstat=$data["service_publish_stat"];
		  $mydata["service_publish_stat"]=$servicepublishstat;
		  $this->Restapi_model->updatePublishStatus($mydata,$serviceid);
		  $response=array(
			  'status' =>  'success'
		  );
		  header('Content-Type: application/json');
		  echo json_encode($response);
 	}
public function initChat()
{
	$this->load->model('Restapi_model');
	$data=json_decode(file_get_contents('php://input'),true);
	$sender_id=$data['sender_id'];
	$receiver_id=$data['receiver_id'];
	if($sender_id != $receiver_id)
	{
		$userDet=$this->Restapi_model->getUserdata($receiver_id);
		if($userDet[0]['user_pic']=="")
		  $userDet[0]['user_pic']="assets/images/no_image.png";
		$channelId=$this->Restapi_model->getchannelId($sender_id,$receiver_id);
		if($channelId == "")
		{
			$channelId=0;
		}
		
		$userDetails=$this->Restapi_model->getUserdata($sender_id);
		$username=$userDetails[0]['user_name'];
		$userpic=$userDetails[0]['user_pic'];
		if($userpic == "")
		   $userpic = "assets/images/no_image.png";
		   
		$otherUserDet=$this->Restapi_model->getUserdata($receiver_id);
		$otherusername=$otherUserDet[0]['user_name'];
		$otheruserpic=$otherUserDet[0]['user_pic'];
		if($otheruserpic == "")
		   $otheruserpic = "assets/images/no_image.png";
		   
       ?>  
	  <?php
		$response=array(
		  'status' =>  'success',
		  'channelId' => $channelId,
		  'user_name' => $userDet[0]['user_name'],
		  'user_pic' => $userDet[0]['user_pic']
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}
}
public function getChatList()
{
	$this->load->model('Restapi_model');
	$data=json_decode(file_get_contents('php://input'),true);
	$user_id=$data['user_id'];
	$userSelList=$this->Restapi_model->getSelUserList($user_id);
	$i=0;
	foreach($userSelList as $user)
	{
		$userList[$i]=$this->Restapi_model->getSelUsersDet($user['user_id'],$user['channel_id'],$user['chat_stat'],$user['lastmsg_recuser'],$user['last_message']);
		$i=$i+1;
	}
	 $response=array(
	  'status' =>  'success',
	  'chat_list'=>$userList
	 );
	header('Content-Type: application/json');
	echo json_encode($response);
}
public function postReview()
{
	$this->load->model('Restapi_model');
	$data=json_decode(file_get_contents('php://input'),true);
	$insertData=array();
	$insertData['order_id']=$data['order_id'];  //  not in list
	$insertData['service_id']=$data['service_id'];
	$insertData['user_id']=$data['user_id'];
	$insertData['rating']=$data['rating'];
	$insertData['review']=$data['review'];
	$this->Restapi_model->saveReview($insertData);
	$response=array(
	  'status' =>  'success'
	);
	header('Content-Type: application/json');
	echo json_encode($response);
}
public function getReviewsList()
{
	$this->load->model('Restapi_model');
	$data=json_decode(file_get_contents('php://input'),true);
	$service_id=$data['service_id'];
	$reviewList=$this->Restapi_model->reviewDetails($service_id);
	$i=0;
	$revlistarr=array();
	foreach ($reviewList->result() as $list) 
	{
		$revlistarr[$i]['user_id']=$list->user_id;
		$revlistarr[$i]['user_name']=$list->name;
		$revlistarr[$i]['user_pic']=$list->profile_pic;
		$revlistarr[$i]['rating']=$list->rating;
		$revlistarr[$i]['description']=$list->review;
		$i=$i+1;
	}
	$response=array(
	  'status' =>  'success',
	  'reviews_list' => $revlistarr
	);
	header('Content-Type: application/json');
	echo json_encode($response);
}
public function discoverService()
{
			$this->load->model('Restapi_model');
			$data=json_decode(file_get_contents('php://input'),true);	 
			$keyword=$data['keyword'];
			if(!empty($data['keyword'])) 
			{
					$keyword = $data['keyword'];
					$this->db->where("service_publish_stat='yes'");
					$this->db->like('service_title', $keyword);
					$query1 = $this->db->get('service');
					$schresult = $query1->result_array();
				
					
					
					$this->db->where("status='on'");
					$this->db->like('service_cat_title', $keyword);
					$query2 = $this->db->get('service_category_master');
					$catresult = $query2->result_array();
					
					$this->db->where("status='on'");
					$this->db->like('service_sub_cat_title', $keyword);
					$query3 = $this->db->get('service_sub_category_master');
					$subresult = $query3->result_array();
					
					$serviceids=array();
					$servicetitles=array();
					$i=0;
					/*foreach ($catresult as $list)
					{
						$serviceids[]=$list['service_cat_id'];
						$servicetitles[]=$list['service_cat_title'];
					}
					foreach ($subresult as $list)
					{
						$serviceids[]=$list['service_sub_cat_id'];
						$servicetitles[]=$list['service_sub_cat_title'];
					}*/
					$servicedetails=array();
					foreach ($schresult as $list)
					{
						$servicedetails[$i]['service_id']=$list['service_id'];
						$servicedetails[$i]['service_title']=$list['service_title'];
						$i=$i+1;
					}
			 }
			 $response=array(
				  'status' =>  'success',
				  'servicedetails'=> $servicedetails
				);
				header('Content-Type: application/json');
				echo json_encode($response);
	}
}
?>